﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (SpriteAnimationManager))]
public class Painting : MonoBehaviour {

	public bool CanChangePainting = false; //{get; private set;}
	private SpriteAnimationManager SAM;

	// Use this for initialization
	void Start () 
	{
		SAM = GetComponent<SpriteAnimationManager>();

		if(CanChangePainting)
		{
			int r = Random.Range(0,SAM.animations.Length);
			SAM.SwitchAnimation(r);
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
