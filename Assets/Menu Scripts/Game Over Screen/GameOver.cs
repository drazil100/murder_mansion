﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    private string[] buttons= { "Restart", "Title Screen" };
    private int focusedIndex=0;
    public Text restartButton, titleButton;

	// Use this for initialization
	void Start ()
    {
        if(GameObject.Find("GameManagement")!=null)
        {
            //Destroy(GameObject.Find("GameManagement"));
            //Destroy(GameObject.Find("Canvas"));
            //GameManagement.SusMeter = null;
            GameManagement.Manager.characters.Clear();
        }

        restartButton = restartButton.GetComponent<Text>();
        titleButton = titleButton.GetComponent<Text>();

        SetFocused(buttons[0]);   
	}
	
	// Update is called once per frame
	void Update ()
    {
        GetInput();
	}

    public void GetInput()
    {
        if (Input.GetButtonUp("Horizontal"))
        {
            if (Input.GetAxis("Horizontal") == Mathf.Abs(Input.GetAxis("Horizontal")))
            {
                if (focusedIndex == buttons.Length - 1)
                {
                    UnsetFocused(buttons[focusedIndex]);
                    focusedIndex -= 1;
                    SetFocused(buttons[focusedIndex]);
                }
                else
                {
                    UnsetFocused(buttons[focusedIndex]);
                    focusedIndex += 1;
                    SetFocused(buttons[focusedIndex]);
                }
            }
            else
            {
                if (focusedIndex == 0)
                {
                    UnsetFocused(buttons[focusedIndex]);
                    focusedIndex += 1;
                    SetFocused(buttons[focusedIndex]);
                }
                else
                {
                    UnsetFocused(buttons[focusedIndex]);
                    focusedIndex -= 1;
                    SetFocused(buttons[focusedIndex]);
                }
            }
        }

        else if (Input.GetButtonUp("Interact") || Input.GetButtonUp("Submit"))
        {
            if(buttons[focusedIndex]=="Restart")
            {
                SceneManager.LoadScene(1);
            }
            else
            {
                SceneManager.LoadScene(0);
            }
        }
    }

    public void SetFocused(string controlName)
    {
        if(controlName=="Restart")
        {
            restartButton.color = Color.yellow;
        }
        else
        {
            titleButton.color = Color.yellow;
        }
    }

    public void UnsetFocused(string controlName)
    {
        if (controlName == "Restart")
        {
            restartButton.color = Color.white;
        }
        else
        {
            titleButton.color = Color.white;
        }
    }
}
