﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour {

    bool cursorLocked;
    int selectedIndex = 0;
    string[] options = { "Play", "Controls",  "Quit" };
    string focusedControl;
    public Text playButton, highscoresButton, controlsButton, optionsButton, quitButton;

    GameObject controlsCanvas, startMenuCanvas;

	// Use this for initialization
	void Start ()
    {
        //cursorLocked = true;
        //Cursor.lockState = CursorLockMode.Locked;
        //Cursor.visible = false;

        playButton = playButton.GetComponent<Text>();
        //highscoresButton = highscoresButton.GetComponent<Text>();
        controlsButton = controlsButton.GetComponent<Text>();
        //optionsButton = optionsButton.GetComponent<Text>();
        quitButton = quitButton.GetComponent<Text>();

        //playButton.GetComponent<ButtonFocused>().Focused();
        SetFocusedControl("Play");

        controlsCanvas = GameObject.Find("Controls Canvas");
        startMenuCanvas = GameObject.Find("Start Menu Canvas");
        startMenuCanvas.SetActive(true);

        controlsCanvas.SetActive(false);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (startMenuCanvas.activeInHierarchy)
        {
            CheckInput();
        }
        else if(controlsCanvas.activeInHierarchy)
        {
            CheckInputControls();
        }
        
        //CheckCursorState();
	}

    void CheckInput()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            cursorLocked = !cursorLocked;
        }

		if(Input.GetButtonUp("Vertical"))  // used to check if the W key was pressed
        {
            if (Input.GetAxis("Vertical")==Mathf.Abs(Input.GetAxis("Vertical")))
            {
                if (selectedIndex == 0)
                {
                    UnsetFocusedControl(options[selectedIndex]);
                    selectedIndex = options.Length - 1;
                    SetFocusedControl(options[selectedIndex]);
                }
                else
                {
                    UnsetFocusedControl(options[selectedIndex]);
                    selectedIndex -= 1;
                    SetFocusedControl(options[selectedIndex]);
                }
            }
            else
            {
                if (selectedIndex == options.Length - 1)
                {
                    UnsetFocusedControl(options[selectedIndex]);
                    selectedIndex = 0;
                    SetFocusedControl(options[selectedIndex]);
                }
                else
                {
                    UnsetFocusedControl(options[selectedIndex]);
                    selectedIndex += 1;
                    SetFocusedControl(options[selectedIndex]);
                }
            }
            Input.ResetInputAxes();
        }

        if(Input.GetButtonUp("Interact")||Input.GetButtonUp("Submit"))
        {
            if(focusedControl=="Play")
            {
				//Debug.Log ("Called");
				GameManagement.Manager.CreateCanvasAndLoadScreen ();
                SceneManager.LoadScene("Foyer");

            }
            /*if (focusedControl == "Highscores")
            {
                
            }*/
            if (focusedControl == "Controls")
            {
                controlsCanvas.SetActive(true);

                startMenuCanvas.SetActive(false);
                focusedControl = "Controls Canvas";
            }
            /*if (focusedControl == "Options")
            {

            }*/
            if (focusedControl == "Quit")
            {
                Application.Quit(); //Doesn't work in editor only in build
            }
        }
    }

    void CheckCursorState()
    {
        if(cursorLocked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    void SetFocusedControl(string controlName)
    {
        if(controlName=="Play")
        {
            focusedControl=controlName;
            playButton.color = new Color(255, 255, 0);
        }
        /*if(controlName=="Highscores")
        {
            focusedControl=controlName;
            highscoresButton.color = new Color(255, 255, 0);
        }*/
        if(controlName=="Controls")
        {
            focusedControl = controlName;
            controlsButton.color= new Color(255, 255, 0);
        }
        /*if(controlName=="Options")
        {
            focusedControl = controlName;
            optionsButton.color= new Color(255, 255, 0);
        }*/
        if(controlName=="Quit")
        {
            focusedControl = controlName;
            quitButton.color= new Color(255, 255, 0);
        }
    }

    void UnsetFocusedControl(string controlName)
    {
        if(controlName=="Play")
        {
            playButton.color=new Color(255,255,255);
        }
        /*if(controlName=="Highscores")
        {
            highscoresButton.color = new Color(255, 255, 255);
        }*/
        if(controlName=="Controls")
        {
            controlsButton.color = new Color(255, 255, 255);
        }
        /*if(controlName=="Options")
        {
            optionsButton.color = new Color(255, 255, 255);
        }*/
        if(controlName=="Quit")
        {
            quitButton.color = new Color(255, 255, 255);
        }
    }

    void CheckInputControls()
    {
        if(Input.GetButtonUp("Interact") || Input.GetButtonUp("Submit"))
        {
            controlsCanvas.SetActive(false);
            startMenuCanvas.SetActive(true);
            focusedControl = "Controls";
        }
    }
}
