﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//[RequireComponent(typeof(SuspicionMeter))]
public class GameManagement : MonoBehaviour {

    private static GameManagement manager;
    public static Transform player;
    public List<CharacterProfile> characters = new List<CharacterProfile>();
	public List<CharacterProfile> living = new List<CharacterProfile>();
    public Dictionary<string, bool> hazardsUsed = new Dictionary<string, bool>();
    public static Dictionary<CharacterType, Dictionary<string, List<string>>> messageDatabase;//Keep
	public List<CharacterProfile> deadCharacters = new List<CharacterProfile>();
    public Inventory playersInventory;
	public Dictionary<string, string> InputKeys = new Dictionary<string, string>();

    public List<DoorProfile> doorProfiles = new List<DoorProfile>();//Keep
    public Dictionary<string, List<string>> connectedRooms = new Dictionary<string, List<string>>();//Keep
    private Dictionary<string, List<Message>> messages = new Dictionary<string, List<Message>>();
    [HideInInspector]
    public string[] rooms;//Keep
    public Dictionary<string, WallPair> sceneBounds = new Dictionary<string, WallPair>();//Keep
    [HideInInspector]
    public int sceneIndex = 1;

    public Transform playerPrefab;
    public Transform inspectorPrefab;
    public Transform defaultGuestPrefab;
    public Transform[] guestPrefabs;
	public Texture2D[] namePlates;
	public Texture2D[] deadPlates;

	public string[] characterNames = {"Jack","Lucas","Maria","Jeff","Lavel","Gloria","Francis","Eleanor"};

	public Transform openedDoorSound;
	public Transform closedDoorSound;

    [Space(10)]
    public int numberOfGuests = 20;
    public static int guestNumber = -1;
    public Texture2D bubble;
    private static Texture2D bubble_saved;

    public float timer = 300;//Time in seconds
    protected float startTime, elapsedTime;
    protected Text timerText = null;
    protected float TimerPosX, TimerPosY;

    private GameObject chatBoxObject;
	private Text chatText;
	private int sender = 0;

	private bool isChatboxVisible;
	private int chatterIndex;
	private bool chatterIsRightOfPlayer;
	public Texture2D[] portraitIndex = new Texture2D[16];

    [HideInInspector]
    public GameObject canvas;
    [HideInInspector]
    public GameObject loadImage, loadText;
	[HideInInspector]
	public int menuIndex = 0;
	[HideInInspector]
	public bool paused = false;
	[HideInInspector]
	public bool hideHelpBoxes = false;
	[HideInInspector]
	public bool gameOver = false;

	private Texture2D fadeOut;
	private float fadeOutTimer = 0;

	//private float imageOffset = 0;

    //[HideInInspector]
    //public SuspicionMeter SusMeter;

	//public Sprite chathead;

    public bool runTimer = true;
    private static bool runTimer_saved;

	//private bool playIntro = true;

    public static GameManagement Manager
    {
        get { return manager; }
    }

    public static List<CharacterProfile> GetCharacters(bool getDeadCharacters = false)
    {
		if (getDeadCharacters)
        	return Manager.characters;

		return Manager.living;
    }

	public static void RemoveOnDeath(CharacterProfile p)
	{
		Manager.living.Remove (p);
	}

	private void SetInputKeys()
	{
		InputKeys.Add ("Interact", "E");
		InputKeys.Add ("UseWeapon", "Space");
		InputKeys.Add ("ExpandView", "Tab");
		InputKeys.Add ("Cancel", "Escape");
	}

	public GameManagement()
	{
		SetInputKeys ();
	}

	void Awake()
	{
			
	}

    void Start()
    {
		if (!PlayerPrefs.HasKey ("help"))
		{
			PlayerPrefs.SetInt ("help", 0);
		}

		hideHelpBoxes = (PlayerPrefs.GetInt ("help") == 1) ? true : false;
		//Time.timeScale = 0.2f;
		Profiler.maxNumberOfSamplesPerFrame = -1;
		//Debug.Log ("Start() called");
		fadeOut = new Texture2D (1, 1);
        ////Debug.Log (Screen.height);
        TimerPosX = (Screen.width / 10) * -3.5f;
        TimerPosY = (Screen.height / 10) * 4;

        if (guestNumber == -1)
        {
            guestNumber = numberOfGuests;
            bubble_saved = bubble;
            runTimer_saved = runTimer;
        }
        else
        {
            bubble = bubble_saved;
            runTimer = runTimer_saved;
        }
        if (manager != null)
        {
            Destroy(gameObject);
        }
        else
        {
            manager = this;
            DontDestroyOnLoad(gameObject);
            if (messageDatabase == null)
            {
				//Debug.Log ("Message Database building");
                BuildMessageDatabase();
            }
            //NewGame();
        }
        //SusMeter = GetComponent<SuspicionMeter>();
    }

    void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == sceneIndex)
        {
            GetSceneBounds();
            InitializeDoorData();
        }
        else
        {
            if (SceneManager.GetActiveScene().name == "Foyer" && characters.ToArray().Length == 0)
            {
                NewGame();
            }
            foreach (CharacterProfile profile in characters)
            {
                profile.Update();
            }
            if (canvas != null && runTimer)
            {
                RunTimer();
            }
        }

		ShowAnswerLists ();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            string rName = SceneManager.GetActiveScene().name;
            if (messages.ContainsKey(rName) && messages[rName].Count > 0)
            {
                //messages [rName] [0].lifeSpan = 0;
            }
        }

        TimerPosX = (Screen.width / 10) * -3.5f;
        TimerPosY = (Screen.height / 10) * 4;

    }

    void FixedUpdate()
    {
		if (GetCharacters ().Count == 1 && GetCharacters (true).Count > 1)
		{

			menuIndex = 0;
			if (fadeOutTimer < 0.92)
			{
				fadeOutTimer = CharacterCore.Approach (fadeOutTimer, 1, 0.2f);
			}
			else
			{
				Win ();
			}
		}
		else if (gameOver && !GameManagement.CoversationHappening (SceneManager.GetActiveScene ().name))
		{
			menuIndex = 0;
			if (fadeOutTimer < 0.92)
			{
				fadeOutTimer = CharacterCore.Approach (fadeOutTimer, 1, 0.2f);
			}
			else
			{
				GameOver ();
			}
		}
		else
		{
			foreach (CharacterProfile profile in characters)
			{
				profile.FixedUpdate();
			}

			DoMessageManagement();
		}
        ////Debug.Log("sus: " + SuspicionMeter.SusMeterPercent);
    }

    private void DoMessageManagement()
    {
        foreach (KeyValuePair<string, List<Message>> room in messages)
        {
            if (room.Value.Count > 0)
            {
                if (!room.Value[0].IsFinished())
                {
					room.Value[0].ShowMessage(room.Key);
                }
                else
                {
                    room.Value[0].ReleaseFromConversation();
                    HideChatBox();
                    room.Value.RemoveAt(0);
                    if (room.Value.Count > 0)
                    {
						room.Value[0].ShowMessage(room.Key);
                    }
                }
            }
        }
    }

	private void ShowAnswerLists()
	{
		foreach (KeyValuePair<string, List<Message>> room in messages)
		{
			if (room.Value.Count > 0)
			{
				if (!room.Value[0].IsFinished())
				{
					room.Value[0].ShowAnswerList();
				}
			}
		}
	}

    public static void ClearMessages(string room = "")
    {
        bool clearAll = false;
        if (room == "") clearAll = true;
        foreach (KeyValuePair<string, List<Message>> r in Manager.messages)
        {
            if (r.Key == room || clearAll)
            {
                foreach (Message m in r.Value)
                {
                    m.ReleaseFromConversation();
                }
                r.Value.Clear();
            }
        }
		Manager.HideChatBox ();
    }

    void OnLevelWasLoaded(int level)
    {
		isChatboxVisible = false;
        string name = SceneManager.GetActiveScene().name;

        if (messages.ContainsKey(name) && messages[name].Count > 0)
        {
            messages[name][0].Time = 0;
        }
		fadeOutTimer = 0;
    }

	public static void ClearObjects()
	{
		Transform[] allObjects = GameObject.FindObjectsOfType (typeof(Transform)) as Transform[];

		foreach (Transform t in allObjects)
		{
			if (t != Manager.GetComponent<Transform>())
				GameObject.Destroy (t.gameObject);
		}
	}

    private void GetSceneBounds()
    {
		//Debug.Log ("Initializing scene bounds");
        GameObject[] collidables = GameObject.FindGameObjectsWithTag("Collidable");
        GameObject rightObj = collidables[0], leftObj = collidables[0];

        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Collidable"))
        {
            if (obj.transform.position.x > rightObj.transform.position.x)
            {
                rightObj = obj;
            }
            else if (obj.transform.position.x < leftObj.transform.position.x)
            {
                leftObj = obj;
            }
        }

        sceneBounds.Add(SceneManager.GetActiveScene().name, new WallPair(leftObj.GetComponent<Collider2D>().bounds, rightObj.GetComponent<Collider2D>().bounds));
    }

    private void InitializeHazardData()
    {
        //todo later
    }

    private void InitializeDoorData()
    {
		//Debug.Log ("Initializing door data");
        if (canvas == null)
        {
            CreateCanvasAndLoadScreen();
        }
        GetDoorData();
        if (sceneIndex + 1 < SceneManager.sceneCountInBuildSettings - 2)
        {
            ++sceneIndex;
            SceneManager.LoadScene(sceneIndex);
        }
        else
        {
            sceneIndex += 5;
            FinishDoorProfiles();
            SceneManager.LoadScene("Foyer");
        }
    }

    private void FinishDoorProfiles()
    {
		//Debug.Log ("Finish door profiles");
        foreach (DoorProfile dp in doorProfiles)
        {
            foreach (DoorProfile dp2 in doorProfiles)
            {
                if (dp.OtherRoom == dp2.Room && dp2.OtherRoom == dp.Room && dp.Number == dp2.Number)
                {
                    dp.other = dp2;
                }
            }
        }

        rooms = new List<string>(connectedRooms.Keys).ToArray();
    }

    public void NewGame()
    {
        if (gameOver != true)
        {
            //Debug.Log("New Game called");
            /*foreach(KeyValuePair<string,WallPair> kv in sceneBounds)
            {
                //Debug.Log(kv.Key+": "+kv.Value.leftWall.size+" "+kv.Value.rightWall.size);
            }*/
            playersInventory = GetComponent<Inventory>();
            playersInventory.showGUI = true;
            SetTimer();
            //Debug.Log("Clearing characters");
            characters.Clear();
            //Debug.Log("Adding characters");
			characters.Add(new CharacterProfile(characterNames[0], CharacterType.Player));
			characters.Add(new CharacterProfile(characterNames[1], CharacterType.Inspector));  // Moved for reasons
            for (int i = 0; i < guestNumber; i++)
            {
				CharacterProfile prof = new CharacterProfile(characterNames[i+2], CharacterType.Guest);
                characters.Add(prof);
                if (i < guestPrefabs.Length)
                {
                    prof.guestPrefab = guestPrefabs[i];
                }
                else
                {
                    prof.guestPrefab = defaultGuestPrefab;
                }
            }
            CharacterProfile.colorIndex = 0;
            //SusMeter.NewGame();

            //Debug.Log("Initializing characters");
            foreach (CharacterProfile profile in characters)
            {
                profile.Update();
            }

			foreach (CharacterProfile profile in characters)
			{
				profile.Context.player = characters [0].Context;
				profile.Context.inspector = characters [1].Context;
			}

            //Debug.Log("Initializing memory");
            foreach (CharacterProfile profile in characters)
            {
                profile.AddCall(new InitializeMemory());
            }
			living = new List<CharacterProfile> (characters);

			foreach (CharacterProfile p in characters)
			{
				p.Context.gameStarted = true;
			}

			GetComponent<Speaker>().PlaySoundContinuous();

			Message m = new Message (characters [0].Context, "(I need to get rid of everyone before they find anything.)");
			m.AddResponse (new Message (characters [0].Context, "(Maybe I can use some of the objects around the house to take care them.)"));
			AddMessage(m);
			/*
			Message m = new Message (characters [1].Context, "I'm Inspector Lucas. I'm here to investigate the death of William Hines.");
			m.AddResponse (new Message (characters [3].Context, "William has been murdered?!"));
			m.AddResponse (new Message (characters [7].Context, "But I saw him yesterday!"));
			m.AddResponse (new Message (characters [1].Context, "It's true. He was killed yesterday."));
			m.AddResponse (new Message (characters [0].Context, "(How did he find me so quickly?)"));
			m.AddResponse (new Message (characters [6].Context, "Do you know who did it?"));
			m.AddResponse (new Message (characters [1].Context, "I don't, but I have reason to believe the killer is here."));
			m.AddResponse (new Message (characters [2].Context, "Oh my god!"));
			m.AddResponse (new Message (characters [5].Context, "Are you saying the killer is one of us?!"));
			m.AddResponse (new Message (characters [1].Context, "I don't know for sure...."));
			m.AddResponse (new Message (characters [0].Context, "(Ok... He doesn't know I did it yet.)"));
			m.AddResponse (new Message (characters [0].Context, "(Maybe I can still get away with this.)"));
			m.AddResponse (new Message (characters [4].Context, "What do we do?"));
			m.AddResponse (new Message (characters [1].Context, "Everyone should remain calm."));
			m.AddResponse (new Message (characters [1].Context, "I'm going to look around. If you find anything let me know."));
			m.AddResponse (new Message (characters [0].Context, "(I need to get rid of him before he finds anything.)"));
			m.AddResponse (new Message (characters [0].Context, "(Maybe I can use some of the objects around the house to take care of him and the others.)"));
			m.AddActionToLastMessage (delegate(CharacterData context)
				{
					foreach (CharacterProfile p in characters)
					{
						p.Context.gameStarted = true;
					}
				});

			if (playIntro)
			{
				

				if (!PlayerPrefs.HasKey ("HasPlayed"))
				{
					AddMessage (m);
					PlayerPrefs.SetInt ("HasPlayed", 1);
				}
				else
				{
					AnswerList a = new AnswerList ();
					a.Add ("Yes", m);
					a.Add ("No", null, delegate
						{
							m.ReleaseFromConversation ();
							foreach (CharacterProfile p in characters)
							{
								p.Context.gameStarted = true;
							}
						});
					AddMessage (new Message (characters [0].Context, "[Would you like to play the intro?]", a));
				}
				playIntro = false;
			}
			else
			{
				AnswerList a = new AnswerList ();
				a.Add ("Yes", m);
				a.Add ("No", null, delegate
					{
						m.ReleaseFromConversation ();
						foreach (CharacterProfile p in characters)
						{
							p.Context.gameStarted = true;
						}
					});
				AddMessage (new Message (characters [0].Context, "[Would you like to play the intro again?]", a));
			}
			*/
        }
    }

    public void GetDoorData()
    {
		GameObject[] objects = GameObject.FindGameObjectsWithTag ("InteractableObject");
        if (objects.Length != 0)
        {
            foreach (GameObject obj in objects)
            {
				if (obj.GetComponent<Door>() != null && obj.GetComponent<Door>().includedInDoorProfiles)
                {
                    doorProfiles.Add(new DoorProfile(SceneManager.GetActiveScene().name, obj.GetComponent<Door>().doorName, obj.GetComponent<Door>().doorNumber, obj.transform.position));
                    if (!connectedRooms.ContainsKey(SceneManager.GetActiveScene().name))
                    {
                        connectedRooms.Add(SceneManager.GetActiveScene().name, new List<string>());
                    }
                    bool hasRoom = false;
                    foreach (string room in connectedRooms[SceneManager.GetActiveScene().name])
                    {
                        if (room == SceneManager.GetActiveScene().name)
                        {
                            hasRoom = true;
                        }
                    }
                    if (!hasRoom)
                    {
                        connectedRooms[SceneManager.GetActiveScene().name].Add(obj.GetComponent<Door>().doorName);
                    }
                }
            }
        }
    }



    public void SetTimer()
    {
		//Debug.Log ("Set Timer called");
        CreateCanvasAndTimer();
        //SusMeter.DrawMeter();
        DontDestroyOnLoad(canvas);
        //DontDestroyOnLoad(SusMeter);

        startTime = Time.time;
        //timerText = GameObject.Find("Timer").GetComponent<Text>();
        //timerText.text = ((int)(timer - (Time.time - startTime)) / 60).ToString() + ":" + ((timer - (Time.time - startTime)) % 60).ToString("f0");
    }

    public void RunTimer()
    {
        elapsedTime = Time.time - startTime;
        if ((timer - elapsedTime) > 0)
        {
            //timerText.text = ((int)(timer - elapsedTime) / 60).ToString() + ":" + (((timer - elapsedTime) % 60) < 9.5 ? "0" + ((timer - elapsedTime) % 60).ToString("f0") : ((timer - elapsedTime) % 60).ToString("f0"));
        }
        else
        {
            //timerText.text = "0:00";
            SceneManager.LoadScene("Escaped");
        }
    }

    public void CreateCanvasAndTimer()
    {
        canvas = new GameObject("Canvas");
        canvas.AddComponent<Canvas>();
        canvas.AddComponent<CanvasScaler>();
        canvas.AddComponent<GraphicRaycaster>();
        canvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
        canvas.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        canvas.GetComponent<CanvasScaler>().screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
        canvas.layer = 5;

        /*
        GameObject theTimer = new GameObject("Timer");
        theTimer.transform.SetParent(canvas.transform);
        theTimer.AddComponent<Text>();
		theTimer.GetComponent<Text>().transform.localPosition = new Vector3(TimerPosX,TimerPosY);
        Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
        theTimer.GetComponent<Text>().font = ArialFont;
        theTimer.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Truncate;
        theTimer.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
        theTimer.GetComponent<Text>().fontSize = 50;
        theTimer.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
        //theTimer.GetComponent<Text>().alignByGeometry = true;
        theTimer.GetComponent<Text>().color = Color.black;
        */
    }

    public void CreateCanvasAndLoadScreen()
    {
        canvas = new GameObject("Canvas");
        canvas.AddComponent<Canvas>();
        canvas.AddComponent<CanvasScaler>();
        canvas.AddComponent<GraphicRaycaster>();
        canvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
        canvas.layer = 5;

        loadImage = new GameObject("LoadScreen");
        loadImage.transform.SetParent(canvas.transform);
        loadImage.AddComponent<Image>();
		loadImage.GetComponent<Image> ().color = new Color (0.157f, 0.157f, 0.157f, 1);
        loadImage.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
        loadImage.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
        loadImage.GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);
        loadImage.GetComponent<RectTransform>().offsetMin = new Vector2(0, -1);

        loadText = new GameObject("Loading...");
        loadText.AddComponent<Text>();
        loadText.transform.SetParent(canvas.transform);
        loadText.GetComponent<Text>().text = "Loading...";
        Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
        loadText.GetComponent<Text>().font = ArialFont;
        loadText.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Overflow;
        loadText.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Overflow;
        loadText.GetComponent<Text>().fontSize = 50;
        loadText.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
        loadText.GetComponent<Text>().alignByGeometry = true;
		loadText.GetComponent<Text>().color = Color.white;
        loadText.GetComponent<Text>().transform.localPosition = new Vector3(.5f, .5f);

    }

    public CharacterProfile GetMyProfile(Transform character)
    {
        foreach (CharacterProfile profile in characters) {
            if (profile.BelongsToMe(character)) {
                return profile;
            }
        }
        return null;
    }

    // CharacterProfile cp,
	public void ShowChatBox(string theText, Color color, int senderIndex, int dir = 1)
    {
        Texture2D sp = bubble;

		chatterIndex = senderIndex;
		if(dir < 0)
		{
			chatterIsRightOfPlayer = true;
		}
		else if (dir > 0)
		{
			chatterIsRightOfPlayer = false;
		}

        //float scaleFactor;
        //float scaledSpriteW; //, scaledSpriteH;

        ////Debug.Log (theText);

		if (sender != senderIndex)
		{
			HideChatBox ();
		}



		if (chatBoxObject == null)
		{
			sender = senderIndex;
			chatBoxObject = new GameObject ("ChatCanvas");

			GameObject ChatCanvas = chatBoxObject;

			ChatCanvas.AddComponent<Canvas> ();
			ChatCanvas.AddComponent<CanvasScaler> ();
			ChatCanvas.AddComponent<GraphicRaycaster> ();
			ChatCanvas.GetComponent<Canvas> ().renderMode = RenderMode.ScreenSpaceOverlay;

			GameObject ChatBox = new GameObject ("ChatBox");

			ChatBox.AddComponent<RawImage> ();
			ChatBox.transform.SetParent (ChatCanvas.transform);
			ChatBox.transform.localPosition = new Vector3 (0, (-(Screen.height / 2) + (Screen.height / 12)), 0);
			ChatBox.GetComponent<RectTransform> ().sizeDelta = new Vector2 (Screen.width, Screen.height / 6);
			ChatBox.GetComponent<RawImage> ().color = color;
			ChatBox.GetComponent<RawImage> ().texture = sp;

			isChatboxVisible = true;

			/*
        GameObject ChatHead = new GameObject("ChatHead");

        ChatHead.transform.SetParent(ChatBox.transform);
        ChatHead.AddComponent<Image>();
		ChatHead.GetComponent<Image>().sprite = chathead; //was sender
		ChatHead.GetComponent<RectTransform>().sizeDelta = new Vector3(160, 200, 0);

		if (chathead.rect.width > chathead.rect.height && chathead.rect.width > ChatBox.GetComponent<RectTransform>().sizeDelta.y)
        {
			scaleFactor = (ChatBox.GetComponent<RectTransform>().sizeDelta.y) / chathead.rect.width;
            scaledSpriteW = sender.rect.width * scaleFactor;
            //scaledSpriteH = sender.rect.height * scaleFactor;
        }
		else if (chathead.rect.width < chathead.rect.height && chathead.rect.height > ChatBox.GetComponent<RectTransform>().sizeDelta.y)
        {
			scaleFactor = (ChatBox.GetComponent<RectTransform>().sizeDelta.y) / chathead.rect.height;
			scaledSpriteW = chathead.rect.width * scaleFactor;
            //scaledSpriteH = sender.rect.height * scaleFactor;
        }
        else
        {
            scaleFactor = 1;
			scaledSpriteW = chathead.rect.width;
            //scaledSpriteH = sender.rect.height;
        }


        ChatHead.transform.localScale = new Vector2(scaleFactor, scaleFactor);
        ChatHead.transform.localPosition = new Vector3((-dir * (Screen.width / 2)) + (50 * dir), -5, 0);

		*/

			float w = 160, h = 200, scaledW = w, scalefactor = 1;

			if (h != Screen.height / 6)
			{
				scalefactor = (Screen.height / 4) / h;
				//scaledH = h*scalefactor;
				scaledW = w * scalefactor;
			}
			scaledW += 40;

			float offset = (dir == 1) ? (scaledW/2) : -(scaledW/2);

			GameObject ChatText = new GameObject ("ChatText");


			ChatText.AddComponent<Text> ();
			ChatText.transform.SetParent (ChatBox.transform);
			ChatText.AddComponent<CanvasScaler> ();
			ChatText.GetComponent<CanvasScaler> ().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;

			ChatText.transform.localPosition = new Vector3 (offset, 0, 0); //dir * 40) + scaledSpriteW
			ChatText.GetComponent<RectTransform> ().sizeDelta = new Vector2 (Screen.width - scaledW - 60, Screen.height / 6);

			ChatText.GetComponent<Text> ().text = theText;
			Font ArialFont = (Font)Resources.GetBuiltinResource (typeof(Font), "Arial.ttf");
			ChatText.GetComponent<Text> ().font = ArialFont;
			ChatText.AddComponent<Outline> ();
			ChatText.GetComponent<Outline> ().effectDistance = new Vector2 (1, 1);
			ChatText.GetComponent<Text> ().verticalOverflow = VerticalWrapMode.Truncate;
			ChatText.GetComponent<Text> ().horizontalOverflow = HorizontalWrapMode.Wrap;
			ChatText.GetComponent<Text> ().fontSize = 32;
			ChatText.GetComponent<Text> ().alignment = TextAnchor.MiddleLeft;
			chatText = ChatText.GetComponent<Text> ();
		}
		else
		{
			chatText.text = theText;
		}

    }

    public void HideChatBox()
    {
        if (chatBoxObject != null)
        {
			isChatboxVisible = false;
            Destroy(chatBoxObject);

        }

    }

    public static void AddMessage(Message message)
    {
        if (!Manager.messages.ContainsKey(message.Room))
            Manager.messages.Add(message.Room, new List<Message>());

        Manager.messages[message.Room].Add(message);
		message.BeginConversation ();
    }

    public static bool CoversationHappening(string room)
    {
        if (!Manager.messages.ContainsKey(room) || Manager.messages[room].Count == 0)
        {
            return false;
        }

        return true;
    }

    public static string GetMessage(CharacterType type, string messageType)
    {
        if (messageDatabase != null && messageDatabase.ContainsKey(type) && messageDatabase[type].ContainsKey(messageType) && messageDatabase[type][messageType] != null && messageDatabase[type][messageType].Count > 0)
        {
            return messageDatabase[type][messageType][Random.Range(0, messageDatabase[type][messageType].Count)];
        }

        return string.Format("Error: Unable to find a message for CharacterType: {0} and messageType: {1}", type, messageType);
    }

    private void BuildMessageDatabase()
    {
        messageDatabase = new Dictionary<CharacterType, Dictionary<string, List<string>>>();

        messageDatabase.Add(CharacterType.Inspector, new Dictionary<string, List<string>>());
        messageDatabase.Add(CharacterType.Guest, new Dictionary<string, List<string>>());

        messageDatabase[CharacterType.Guest].Add("follow_request", new List<string>());
        messageDatabase[CharacterType.Guest].Add("follow_response_yes", new List<string>());
        messageDatabase[CharacterType.Guest].Add("follow_response_no", new List<string>());
        messageDatabase[CharacterType.Guest].Add("wander", new List<string>());
        messageDatabase[CharacterType.Guest].Add("wander_response", new List<string>());
        messageDatabase[CharacterType.Guest].Add("unfollow", new List<string>());
        messageDatabase[CharacterType.Guest].Add("unfollow_response", new List<string>());
        messageDatabase[CharacterType.Guest].Add("witness_murder", new List<string>());
        messageDatabase[CharacterType.Guest].Add("lost_sight", new List<string>());
		messageDatabase[CharacterType.Guest].Add("found_victim", new List<string>());
        messageDatabase[CharacterType.Guest].Add("interact", new List<string>());

        messageDatabase[CharacterType.Inspector].Add("follow_request", new List<string>());
        messageDatabase[CharacterType.Inspector].Add("follow_response_yes", new List<string>());
        messageDatabase[CharacterType.Inspector].Add("wander", new List<string>());
        messageDatabase[CharacterType.Inspector].Add("wander_response", new List<string>());
        messageDatabase[CharacterType.Inspector].Add("unfollow", new List<string>());
        messageDatabase[CharacterType.Inspector].Add("unfollow_response", new List<string>());
        messageDatabase[CharacterType.Inspector].Add("witness_murder", new List<string>());
        messageDatabase[CharacterType.Inspector].Add("lost_sight", new List<string>());
		messageDatabase[CharacterType.Inspector].Add("found_victim", new List<string>());
        messageDatabase[CharacterType.Inspector].Add("interact", new List<string>());



        //  Guest
        messageDatabase[CharacterType.Guest]["follow_request"].Add("Let's stick together.");
        messageDatabase[CharacterType.Guest]["follow_request"].Add("I would like to go with you.");
        messageDatabase[CharacterType.Guest]["follow_request"].Add("May I tag along?");

        messageDatabase[CharacterType.Guest]["follow_response_yes"].Add("Alright.");
        messageDatabase[CharacterType.Guest]["follow_response_yes"].Add("Sure.");
        messageDatabase[CharacterType.Guest]["follow_response_yes"].Add("Okay.");
        messageDatabase[CharacterType.Guest]["follow_response_yes"].Add("Let's go together.");

        messageDatabase[CharacterType.Guest]["follow_response_no"].Add("I don't really trust you.");
        messageDatabase[CharacterType.Guest]["follow_response_no"].Add("No.");
        messageDatabase[CharacterType.Guest]["follow_response_no"].Add("No Thanks.");
        messageDatabase[CharacterType.Guest]["follow_response_no"].Add("Not interested.");

        messageDatabase[CharacterType.Guest]["wander"].Add("I'll look over here.");
        messageDatabase[CharacterType.Guest]["wander"].Add("I'm going to wander over here.");
        messageDatabase[CharacterType.Guest]["wander"].Add("Let's see what's over here.");

        messageDatabase[CharacterType.Guest]["wander_response"].Add("Keep your eyes open for any evidence.");
        messageDatabase[CharacterType.Guest]["wander_response"].Add("Let me know if you find anything.");

        messageDatabase[CharacterType.Guest]["unfollow"].Add("I'm going to go check out a different room.");
        messageDatabase[CharacterType.Guest]["unfollow"].Add("Let's split up.");

        messageDatabase[CharacterType.Guest]["unfollow_response"].Add("Be careful.");
        messageDatabase[CharacterType.Guest]["unfollow_response"].Add("Watch your back.");
        messageDatabase[CharacterType.Guest]["unfollow_response"].Add("Let me know if you find anything.");
        messageDatabase[CharacterType.Guest]["unfollow_response"].Add("Don't get yourself killed.");
        messageDatabase[CharacterType.Guest]["unfollow_response"].Add("Good luck.");

        messageDatabase[CharacterType.Guest]["witness_murder"].Add("Oh my god!");
        messageDatabase[CharacterType.Guest]["witness_murder"].Add("Oh good Heavens!");

        messageDatabase[CharacterType.Guest]["lost_sight"].Add("Where are you?");
        messageDatabase[CharacterType.Guest]["lost_sight"].Add("Hello?");

		messageDatabase[CharacterType.Guest]["found_victim"].Add("Come quick!");
		messageDatabase[CharacterType.Guest]["found_victim"].Add("Help!");
		messageDatabase[CharacterType.Guest]["found_victim"].Add("Oh god!");
		messageDatabase[CharacterType.Guest]["found_victim"].Add("Inspector!");
		messageDatabase[CharacterType.Guest]["found_victim"].Add("Somebody help!");

        messageDatabase[CharacterType.Guest]["interact"].Add("Can I help you?");



        //  Inspector
        messageDatabase[CharacterType.Inspector]["follow_request"].Add("I would like to go with you for a while.");
        messageDatabase[CharacterType.Inspector]["follow_request"].Add("I'm going with you");
        //messageDatabase [CharacterType.Inspector] ["follow_request"].Add ("Let's stick together");

        messageDatabase[CharacterType.Inspector]["follow_response_yes"].Add("Sure.");
        messageDatabase[CharacterType.Inspector]["follow_response_yes"].Add("Stay close to me.");
        messageDatabase[CharacterType.Inspector]["follow_response_yes"].Add("Okay.");
        messageDatabase[CharacterType.Inspector]["follow_response_yes"].Add("Alright.");

        messageDatabase[CharacterType.Inspector]["wander"].Add("I'll look over here.");
        messageDatabase[CharacterType.Inspector]["wander"].Add("I'm going to wander over here.");
        messageDatabase[CharacterType.Inspector]["wander"].Add("There has to be a clue here somewhere");

        messageDatabase[CharacterType.Inspector]["wander_response"].Add("Be careful.");
        messageDatabase[CharacterType.Inspector]["wander_response"].Add("Watch your back.");
        messageDatabase[CharacterType.Inspector]["wander_response"].Add("Let me know if you find anything.");
        messageDatabase[CharacterType.Inspector]["wander_response"].Add("Don't get yourself killed.");

        messageDatabase[CharacterType.Inspector]["unfollow"].Add("I'm going to go check out a different room.");
        messageDatabase[CharacterType.Inspector]["unfollow"].Add("I'll see you again later.");

        messageDatabase[CharacterType.Inspector]["unfollow_response"].Add("Be careful.");
        messageDatabase[CharacterType.Inspector]["unfollow_response"].Add("Watch your back.");
        messageDatabase[CharacterType.Inspector]["unfollow_response"].Add("Let me know if you find anything.");
        messageDatabase[CharacterType.Inspector]["unfollow_response"].Add("Don't get yourself killed.");
        messageDatabase[CharacterType.Inspector]["unfollow_response"].Add("Good luck.");

        messageDatabase[CharacterType.Inspector]["witness_murder"].Add("So it WAS you!");

        messageDatabase[CharacterType.Inspector]["lost_sight"].Add("Where did you go?");
        messageDatabase[CharacterType.Inspector]["lost_sight"].Add("Hello?");
        messageDatabase[CharacterType.Inspector]["lost_sight"].Add("Hmmmm.");

		messageDatabase[CharacterType.Inspector]["found_victim"].Add("Come quick!");
		messageDatabase[CharacterType.Inspector]["found_victim"].Add("Oh god!");

        messageDatabase[CharacterType.Inspector]["interact"].Add("Can I help you?");


    }

    public void Win()
    {
        GMCleanUp();

        SceneManager.LoadScene("Escaped");
    }

    public void GameOver()
    {
        GMCleanUp();

        SceneManager.LoadScene("GameOver");
    }

    private void GMCleanUp()
    {

		GetComponent<Speaker>().StopPlaying();

		gameOver = false;
        player = null;
		living.Clear ();
        hazardsUsed.Clear();
		deadCharacters.Clear ();
        playersInventory.hasItem = false;
        playersInventory.showGUI = false;
		CharacterProfile.order = 0;


		foreach (KeyValuePair<string, List<Message>> room in messages)
		{
			room.Value.Clear ();
		}
        Destroy(GameObject.Find("Canvas"));
    }

	void OnGUI()
	{
		fadeOut.SetPixel(0, 0, new Color(0, 0, 0, fadeOutTimer));
		fadeOut.Apply ();

		foreach (KeyValuePair<string, List<Message>> room in messages)
		{
			if (room.Value.Count > 0)
			{
				if (!room.Value[0].IsFinished())
				{
					room.Value[0].DrawAnswerList();
				}
			}
		}



		if (isChatboxVisible)
		{
			float screenBuffer = 20;

			GUIStyle background = new GUIStyle ();
			background.stretchWidth = true;
			background.stretchHeight = true;

			if (!chatterIsRightOfPlayer)
			{
				background.normal.background = portraitIndex [(chatterIndex * 2)];
			}
			else
			{
				background.normal.background = portraitIndex [(chatterIndex * 2) + 1];
			}

			float side = 0;

			float w = 160, h = 200, scaledW = w, scaledH = h, scalefactor = 1;

			if (h != Screen.height / 4)
			{
				scalefactor = (Screen.height / 4) / h;
				scaledH = h * scalefactor;
				scaledW = w * scalefactor;
			}

			if (!chatterIsRightOfPlayer)
			{
				side = 0 + screenBuffer;
			}
			else if (chatterIsRightOfPlayer)
			{
				side = (Screen.width - scaledW) - screenBuffer;
			}

			GUI.Label (new Rect (side, Screen.height - scaledH, scaledW, scaledH), "", background);
		}
		else
		{
			float w2 = 160, h2 = 40, scaledW2 = w2, scaledH2 = h2, scalefactor2 = 1;
			if (w2 != Screen.width / 7)
			{
				scalefactor2 = (Screen.width / 7) / w2;
				scaledH2 = h2 * scalefactor2;
				scaledW2 = w2 * scalefactor2;
			}

			GUILayout.BeginArea (new Rect (0, Screen.height - scaledH2, Screen.width, scaledH2));
			GUILayout.BeginHorizontal ();
			foreach (CharacterProfile p in characters)
			{
				GUIStyle background2 = new GUIStyle ();
				background2.stretchWidth = true;
				background2.stretchHeight = true;
				if (p.Type == CharacterType.Player) continue;
				if (p.Context != null && p.Context.isAlive)
				{
					background2.normal.background = namePlates [p.Context.index - 1];
				}
				else if (p.Context != null)
				{
					background2.normal.background = deadPlates [p.Context.index - 1];
				}
				GUILayout.Label ("", background2, GUILayout.MinWidth(scaledW2), GUILayout.MinHeight(scaledH2));
			}
			GUILayout.EndHorizontal ();
			GUILayout.EndArea ();
		}

		if (GetComponent<Menu> () != null)
		{
			GetComponent<Menu> ().DoGUI ();
		}

		GUIStyle s = new GUIStyle ();
		s.stretchHeight = true;
		s.stretchWidth = true;
		s.normal.background = fadeOut;

		GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "", s);
	}
}