﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

[System.Serializable]
public class SpAnimation {
	public string name = "";
	public Texture2D sheet;
	public int rows;
	public int columns;
	public int frames;
	public float frameMS = 16.0f;
	public Vector2 pivot = new Vector2(0.5f, 0.5f);
	[HideInInspector]
	public float duration;
	private Sprite[] sprites;
	private static Dictionary<Texture2D, Sprite[]> spriteDatabase = new Dictionary<Texture2D, Sprite[]>();

	public void Initialize(int pixelsPerUnit)
	{
		duration = frameMS * frames;
		if (spriteDatabase.ContainsKey (sheet))
		{
			sprites = spriteDatabase [sheet];
			return;
		}
		//frames = rows * columns;
		sprites = new Sprite[frames];
		int index = 0;
		for(int y = 0; y < rows; y++) {
			for(int x = 0; x < columns && index < frames; x++) {
				Rect frameRect = GetFrameRect(index);
				sprites[index] = Sprite.Create(
					sheet,
					frameRect,
					pivot,
					pixelsPerUnit);
				index++;
			}
		}

		spriteDatabase.Add (sheet, sprites);
	}

	public Rect GetFrameRect (int frame)
	{
		int frameHeight = sheet.height / rows;
		int frameWidth = sheet.width / columns;

		int x = (frame % columns) * frameWidth;
		int y = sheet.height - (frame / columns + 1) * frameHeight;

		return new Rect (x, y, frameWidth, frameHeight);
	}

	public Sprite GetSprite(float step)
	{
		try
		{
			//  try catch only used on this because [ExecuteInEditMode] threw some scary out of index
			//  errors when variables were changed in the inspector. Now it just returns null when
			//  this happens but this is not an issue in edit mode
			return sprites[Mathf.Clamp(Mathf.FloorToInt(step / frameMS), 0, frames - 1)];
		}
		catch (Exception e)
		{
			//  Lazily supress warning about unused variable by using it in useless way
			Exception e2 = e;
			e = e2;

			Debug.Log ("Variables changed in the inspector or assets were reloaded. Out of index exception ignored. \nHit play to reset sprite in scene view.");
			return null;
		}
	}
}

//  This is so that it 
//[ExecuteInEditMode]
[RequireComponent (typeof (SpriteRenderer))]
public class SpriteAnimationManager : MonoBehaviour {

	public int pixelsPerUnit = 300;
	public SpAnimation[] animations;
	private float step = 0;
	private int currentAnimation = 0;
	private bool isPlaying = false;
	private SpAnimation oneShot = null;
	private Sprite current;

	void Start() {
		foreach(SpAnimation anim in animations) {
			anim.Initialize(pixelsPerUnit);
		}
		current = GetComponent<SpriteRenderer> ().sprite;
		step = 0;
	}

	void Update() {
		if (!isPlaying && current != GetComponent<SpriteRenderer> ().sprite) 
			return;
		if (oneShot != null) {
			GetComponent<SpriteRenderer>().sprite = oneShot.GetSprite(step);
			step += Time.deltaTime * 1000;
			if(step >= oneShot.duration) {
				oneShot = null;
				step = 0;
			} else {
				return;
			}
		}

		SpAnimation animation = animations[currentAnimation];
		GetComponent<SpriteRenderer>().sprite = animation.GetSprite(step);
		if (isPlaying) {
			step += Time.deltaTime * 1000;
			if (step >= animation.duration) {
				step -= animation.duration;
			}
		}
		current = GetComponent<SpriteRenderer> ().sprite;
	}

	public void Play() {
		isPlaying = true;
	}

	public void Stop() {
		isPlaying = false;
		if (oneShot == null)
			step = 0;
	}

	public void SetFrame(int frame)	{
		step = Mathf.Clamp(frame, 0, animations [currentAnimation].frames);
	}

	public void PlayOnce(int animation) {
		oneShot = animations[animation];
		step = 0;
	}

	public void PlayOnce(string name) {
		for (int i = 0; i < animations.Length; i++) {
			if (name == animations [i].name) {
				PlayOnce(i);
				return;
			}
		}
	}

	public bool OneShotPlaying() {
		return oneShot != null;
	}

	public void SwitchAnimation(int animation, bool play = false) 
	{
		if (currentAnimation != animation || play != isPlaying)
		{
			//Debug.Log (animation);
			currentAnimation = animation;
			if (oneShot == null)
				step = 0;
		}
		if (play)
		{
			Play ();
		}
		else
		{
			Stop ();
		}
	}

	public void SwitchAnimation(string name, bool play = false) {

		for (int i = 0; i < animations.Length; i++) {
			if (name == animations [i].name) {
				SwitchAnimation(i, play);
				return;
			}
		}
	}



	public void RecolorAll(Color darkest, Color brightest)
	{
		//Debug.Log ("yes it is");
		foreach (SpAnimation animation in animations)
		{
			animation.sheet = Recolor (animation.sheet, darkest, brightest);
			animation.Initialize (pixelsPerUnit);
		}

	}



	public static Texture2D Recolor(Texture2D sprite, Color darkest, Color brightest) {
		int width = sprite.width;
		int height = sprite.height;
		Texture2D result = new Texture2D(width, height);

		float minValue = 1.0f;
		float maxValue = 0.0f;

		float darkH, darkS, darkV, brightH, brightS, brightV;
		Color.RGBToHSV(darkest, out darkH, out darkS, out darkV);
		Color.RGBToHSV(brightest, out brightH, out brightS, out brightV);

		float h, s, v;
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				Color pixel =  sprite.GetPixel(x, y);
				if(pixel.a < 0.1) continue;

				Color.RGBToHSV(pixel, out h, out s, out v);
				if(v < minValue) {
					minValue = v;
				}
				if(v > maxValue) {
					maxValue = v;
				}
			}
		}

		//Debug.Log (string.Format ("min: {0}, max: {1}", minValue, maxValue));

		float valueRange = (maxValue - minValue);
		float outH, outS, outV, lerpT;
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				Color pixel =  sprite.GetPixel(x, y);
				if(pixel.a == 0) {
					result.SetPixel(x, y, pixel);
					continue;
				}

				Color.RGBToHSV(pixel, out h, out s, out v);
				lerpT = (v - minValue) / valueRange;
				outH = Mathf.Lerp(darkH, brightH, lerpT);
				outS = Mathf.Lerp(darkS, brightS, lerpT);
				outV = Mathf.Lerp(darkV, brightV, lerpT);
				Color recolored = Color.HSVToRGB(outH, outS, outV);
				recolored.a = pixel.a;

				result.SetPixel(x, y, recolored);

				//Debug.Log (string.Format("original: {0}, new: {1}", pixel, recolored));
			}
		}


		result.Apply();
		return result;
	}
}
