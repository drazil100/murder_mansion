﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;

public enum CharacterType
{
	Player,
	Guest,
	Inspector
}

public class CharacterProfile {

	private string mCharacterName;
	private CharacterType mType;
	//private Vector3 pos;
	public Transform owner;
	private Branch<CharacterData> tree;
	private CharacterData context;
	public Texture2D bubble;
	public bool safeToSpeak = false;

    //Variables Joey added
    public string enteredDoor=null;
    public int enteredDoorNumber;
    public string roomLeft;
    //public string currentScene;
    public bool facing;
	public Color lightColor = Color.white;
	public Color darkColor = Color.black;
	public Color bubbleColor = Color.gray;
	public Color answerColor = Color.gray;
	public SpAnimation[] animations;
	float colorVarience = -1;
	public static float colorIndex = 0;
	public static int order = 0;
	public Transform guestPrefab;

	public static CharacterProfile[] Characters {
		get { return GameManagement.GetCharacters(true).ToArray(); }
	}

    public CharacterType Type
	{
		get { return mType; }
	}

	public string Name
	{
		get { return mCharacterName; }
	}

	public CharacterData Context { get { return context; } }

	public CharacterProfile(string characterName, CharacterType type = CharacterType.Guest)
	{
		mCharacterName = characterName;
		mType = type;
		bubble = GameManagement.Manager.bubble;

		if (type == CharacterType.Guest)
		{
			colorVarience = 1.0f / GameManagement.Manager.numberOfGuests;
			float newIndex = Mathf.Clamp (colorIndex + (0.25f *colorVarience), 0.0f, 0.95f);
			lightColor = UnityEngine.Random.ColorHSV (colorIndex, newIndex, 1, 1, 1, 1);
			colorIndex = Mathf.Clamp (colorIndex + colorVarience, 0.0f, 0.95f);
			float h, s, v;
			Color.RGBToHSV (lightColor, out h, out s, out v);
			lightColor = Color.HSVToRGB (h, 0.6f, v);
			darkColor = Color.HSVToRGB (h, s, 0.00025f);
			bubbleColor = Color.HSVToRGB (h, 0.4f, 0.5f);
			answerColor = Color.HSVToRGB (h, 0.5f, 0.275f);
			//bubble = SpriteAnimationManager.Recolor (bubble, darkColor, value);
		}
		else if (type == CharacterType.Inspector)
		{
			darkColor = new Color(0.00025f, 0.00025f, 0.00025f, 1);
			lightColor = new Color(0.75f, 0.75f, 0.5f, 1);
			bubbleColor = lightColor;
			float h, s, v;
			Color.RGBToHSV (lightColor, out h, out s, out v);
			answerColor = Color.HSVToRGB (h, 0.3f, 0.275f);
			bubbleColor = Color.HSVToRGB (h, 0.25f, 0.5f);
			//bubble = SpriteAnimationManager.Recolor (bubble, darkColor, lightColor);
		}
		else
		{
			darkColor = new Color(0.00025f, 0.00025f, 0.00025f, 1);
			lightColor = new Color(0.25f, 0.25f, 0.25f, 1);
			bubbleColor = lightColor;
			float h, s, v;
			Color.RGBToHSV (lightColor, out h, out s, out v);
			answerColor = Color.HSVToRGB (h, 0.5f, 0.275f);
			//bubble = SpriteAnimationManager.Recolor (bubble, darkColor, lightColor);
		}

	}

	public void Update()
	{
		//if (mType == CharacterType.Player) GetCurrentScene();


		if (owner == null)
		{
			CharacterCore c;
			bool getNewPosition = false;
			switch (mType)
			{
				case CharacterType.Player:
					owner = (Transform)GameObject.Instantiate (GameManagement.Manager.playerPrefab, new Vector3(0, 2.5f, 0), Quaternion.identity);
					c = owner.GetComponent<CharacterCore> ();
					if (context == null)
					{
						context = new CharacterData (this);
						context.index = order++;
					}
					c.context = context;
					context.Room = SceneManager.GetActiveScene().name;
					context.character = c;
					context.bounds = owner.GetComponent<BoxCollider2D> ().bounds;
					SetCharacterPosition ();
					break;
				case CharacterType.Guest:
					if ((context != null && (context.Room != SceneManager.GetActiveScene ().name)))
						break;
					if (context == null)
					{
						getNewPosition = true;
						context = new CharacterData (this);
						context.Room = SceneManager.GetActiveScene ().name;
						context.SetTarget(context.Target, context.Room);
						context.index = order++;
						//Debug.Log (context.index);
					}
					owner = (Transform)GameObject.Instantiate (guestPrefab, new Vector3(context.position.x, 2.5f, 0), Quaternion.identity);
					if (!context.isAlive) owner.Rotate (0, 0, 90);
					c = owner.GetComponent<CharacterCore> ();
					if (c.context == null)
					{
						c.context = context;
					}
					context.character = c;
					context.bounds = owner.GetComponent<BoxCollider2D> ().bounds;
					if (getNewPosition)
					{
						context.SetRandomTargetInRoom ();
						context.position.x = context.Target;
						owner.position = context.Position;

					}
					SetCharacterPosition ();
					break;
				case CharacterType.Inspector:
					if ((context != null && (context.Room != SceneManager.GetActiveScene ().name)))
						break;
					if (context == null)
					{
						getNewPosition = true;
						context = new CharacterData (this);
						context.Room = SceneManager.GetActiveScene ().name;
						context.SetTarget(context.Target, context.Room);
						context.index = order++;
					}
					owner = (Transform)GameObject.Instantiate (GameManagement.Manager.inspectorPrefab, new Vector3(context.position.x, 2.5f, 0), Quaternion.identity);
					if (!context.isAlive) owner.Rotate (0, 0, 90);
					c = owner.GetComponent<CharacterCore> ();
					if (c.context == null)
					{
						c.context = context;
					}
					context.character = c;
					context.bounds = owner.GetComponent<BoxCollider2D> ().bounds;
					if (getNewPosition)
					{
						context.SetRandomTargetInRoom ();
						context.position.x = context.Target;
						owner.position = context.Position;
					}
					SetCharacterPosition ();
					break;
			}

		}

		if (mType == CharacterType.Player && !owner.GetComponent<PlayerController>().viewExpanded)
		{
			Transform cam = Camera.main.transform;
			cam.position = new Vector3 (owner.transform.position.x, owner.transform.position.y + 1f, -6);
			cam.parent = owner;
		}

        //I added these method calls
        OpenedADoor();

		if (Type != CharacterType.Player && owner != null)
		{
			context.position = new Vector2(owner.position.x, owner.position.y);
		}

		/*if (Type == CharacterType.Inspector)
			Debug.Log (string.Format ("follow: {0}, room: {1}", Context.interestInFollowing, Context.interestInRoom));*/
	}



	public void FixedUpdate()
	{
		if (tree != null && context != null && context.isAlive) {
			context.UpdateDecisionVariables ();
			tree.Process (context);
		}
	}

	public bool BelongsToMe(Transform character)
	{
		return (character == owner);
	}


    public void OpenedADoor()
    {
        if (enteredDoor != null)
        {
			//GameManagement.ClearObjects();
			//SceneManager.L
			SceneManager.LoadScene(enteredDoor, LoadSceneMode.Single);
            //GameManagement.Manager.GameOver();
			//Characters [0].Context.Room = enteredDoor;
			//Characters [0].Context.SetTarget (0, enteredDoor);
            enteredDoor = null;
        }

    }

    public void SetCharacterPosition()
    {
        //GameObject playa = GameObject.FindGameObjectWithTag("Player");//I gave the player prefab the Player tag

        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("InteractableObject"))
        {
			if (obj.GetComponent<Door>() != null && obj.GetComponent<Door>().doorName == roomLeft && owner != null)
            {
                if (obj.GetComponent<Door>().doorNumber == enteredDoorNumber)
                {
					owner.position = new Vector3(obj.transform.position.x, obj.transform.position.y + 1.5f, owner.position.z);
                }
            }
        }
		FixPosition();
    }
    
    
    
	public void MoveToGround()
	{
		int emergencyAbortCounter = 0;
		CharacterCore core = owner.GetComponent<CharacterCore> ();
		if (core != null) 
		{
			//if (!context.isAlive) Debug.Log ("before move to ground " + context.index + ": " + core.transform.position);
			while (!core.IsGrounded && emergencyAbortCounter++ < 10000) 
			{
				//Debug.Log (owner.position);
				core.MoveCharacter (0, -0.5f);

			}
			//if (!context.isAlive) Debug.Log ("move to ground " + context.index + ": " + core.transform.position);
		}

	}

	//  Method for fixing character's position if he's inside an object after coming through a door
	public void FixPosition()
    {
		//if (!context.isAlive) Debug.Log ("fix position " + context.index + ": " + owner.position);
        //GameObject ground=null;
        GameObject[] collidableObjects = GameObject.FindGameObjectsWithTag("Collidable");
        float playerAddToCenter = (owner.GetComponent<Collider2D>().bounds.size.x/2),objAddToCenter,objLeftSide;

		foreach (GameObject obj in collidableObjects)
		{
			if (owner.GetComponent<Collider2D> ().bounds.Intersects (obj.GetComponent<Collider2D> ().bounds))
			{
				objAddToCenter = (obj.GetComponent<Collider2D> ().bounds.size.x / 2);
                
				if ((owner.position.x + playerAddToCenter) > (objLeftSide = obj.transform.position.x - objAddToCenter)
				                &&
				                (owner.position.x < obj.transform.position.x))
				{
					owner.position = new Vector3 (((objLeftSide - playerAddToCenter) - .01f), owner.transform.position.y);
				}
				else
				{
					owner.position = new Vector3 (((obj.transform.position.x + objAddToCenter) + playerAddToCenter + .01f), owner.position.y);
				}
			}
		}
		//if (!context.isAlive) Debug.Log ("after fix position " + context.index + ": " + owner.position);
		MoveToGround();
        owner.GetComponent<CharacterCore>().GetComponent<SpriteRenderer>().flipX = facing;
    }

	public void InitializeTree(Branch<CharacterData> newTree)
	{
		if (tree == null) {
			tree = newTree;
			tree.Reset ();
		}
	}

	public bool DoInitializeTree()
	{
		if (tree == null) {
			return true;
		} else {
			return false;
		}
	}

	public void AddCall(Call call)
	{
		if (context != null)
			context.calls.Add (call);
	}
	
	public static CharacterProfile[] GetCharactersInRoom(string room, bool getDeadPlayers = false)
	{
		List<CharacterProfile> characters = new List<CharacterProfile> ();

		foreach (CharacterProfile character in GameManagement.GetCharacters(getDeadPlayers))
		{
			if (character.Context != null && character.Context.Room == room)
			{
				characters.Add (character);
			}
		}

		return characters.ToArray ();
	}

	public static CharacterProfile[] GetCharactersNotInRoom(string room, bool getDeadPlayers = false)
	{
		List<CharacterProfile> characters = new List<CharacterProfile> ();

		foreach (CharacterProfile character in GameManagement.GetCharacters(getDeadPlayers))
		{
			if (character.Context != null && character.Context.Room != room)
			{
				characters.Add (character);
			}
		}

		return characters.ToArray ();
	}

	public static CharacterProfile[] GetCharactersGoingToRoom(string room, bool getDeadPlayers = false)
	{
		List<CharacterProfile> characters = new List<CharacterProfile> ();

		foreach (CharacterProfile character in Characters)
		{
			if (character.Context != null && character.Context.TargetRoom == room && (getDeadPlayers || character.Context.isAlive))
			{
				characters.Add (character);
			}
		}

		return characters.ToArray ();
	}
}
