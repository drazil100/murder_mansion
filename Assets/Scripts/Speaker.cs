﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (AudioSource))]
public class Speaker : MonoBehaviour {

	//public GameObject speaker;
	public AudioClip MyAudio;
	public bool isOneOff;
	public bool isContinuous;
	private AudioSource source;
	private bool hasPlayedAudio = false;
	public bool modulatePitch = false;
	public bool AutoPlay;
	public float MinDistance = 3f, MaxDistance = 10f;
	public float volumePercentModifier = .80f; // percentage for max volume. Ex: .80f makes max volume 80%

	private float pitchRangeMin = .925f, pitchRangeMax = 1.075f;


	private float GetDistance(float a, float b)
	{
		return Mathf.Abs(a-b);
	}

	void AdjustVolumeByDistance()
	{
		AudioListener listener = (AudioListener) FindObjectOfType(typeof(AudioListener));
		if(listener && source)
		{
			float DistToPlayer = GetDistance(source.transform.position.x, listener.transform.position.x);
			float DistMinMax = GetDistance(MinDistance,MaxDistance);

			//Debug.Log("Distance: "+DistToPlayer);

			if(DistToPlayer <= MinDistance)
			{
				source.volume = 1f * volumePercentModifier;
			}
			else if(DistToPlayer > MinDistance && DistToPlayer <= MaxDistance)
			{
				source.volume = volumePercentModifier * (MaxDistance-DistToPlayer)/DistMinMax;
			}
			else if(DistToPlayer > MaxDistance)
			{
				source.volume = 0f;
			}
			//Debug.Log("Volume: " + source.volume);
		}
	}

	// Use this for initialization
	void Awake () {
		source = GetComponent<AudioSource> ();

		if(AutoPlay && MyAudio != null)
		{
			PlaySoundOnce(MyAudio);
		}
	}

	void Update()
	{
		if (hasPlayedAudio && !source.isPlaying && isOneOff)
		{
			Destroy (gameObject);
			return;
		}

		if(source.isPlaying)
		{
			AdjustVolumeByDistance();
		}
		else if (!source.isPlaying && isContinuous)
		{
			PlaySoundContinuous();
		}

		if(AutoPlay && !source.isPlaying)
		{
			if(isOneOff)
			{
				PlaySoundOnce();
			}
			else
			{
				PlaySoundContinuous();
			}
		}


	}

	public float getRandomPitchValue()
	{
		return Random.Range (pitchRangeMin,pitchRangeMax);
	}

	public bool IsPlaying()
	{
		return source.isPlaying;
	}

	public void PlaySoundOnce ()
	{
		if(MyAudio != null)
		{
		source.PlayOneShot (MyAudio, 1F);
		hasPlayedAudio = true;
		}
	}

	public void PlaySoundOnce (AudioClip playThis) 
	{
		MyAudio = playThis;
		source.PlayOneShot (playThis, 1F);
		hasPlayedAudio = true;
	}

	public void PlaySoundContinuous()
	{
		if(MyAudio != null && source != null)
		{
			isContinuous = true;
			source.clip = MyAudio;
			if(modulatePitch)
			source.pitch = getRandomPitchValue ();
			source.Play();
		}
	}

	public void PlaySoundContinuous(AudioClip playThis)
	{
		isContinuous = true;
		MyAudio = playThis;
		source.clip = playThis;
		if(modulatePitch)
		source.pitch = getRandomPitchValue ();
		source.Play();
	}

	public void StopPlaying()
	{
		if(source.isPlaying)
		{
			isContinuous = false;
			source.Stop();
		}
	}

}
