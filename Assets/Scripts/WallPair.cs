﻿using UnityEngine;
using System.Collections;

public class WallPair
{
    public Bounds rightWall, leftWall;
    
    public WallPair(Bounds leftWall, Bounds rightWall)
    {
        this.rightWall = rightWall;
        this.leftWall = leftWall;
    }	

	public static bool IsValid(string room, float position, Bounds bounds)
	{
		if (!GameManagement.Manager.sceneBounds.ContainsKey (room))
		{
			Debug.Log (string.Format ("\"{0}\" is not a valid room", room));
			return false;
		}

		WallPair pair = GameManagement.Manager.sceneBounds[room];

		bounds.center = new Vector3(position, bounds.center.y, bounds.center.z);

		if (pair.leftWall.Intersects (bounds) || pair.rightWall.Intersects (bounds) || position < pair.leftWall.center.x || position > pair.rightWall.center.x)
			return false;


		return true;
	}

	public static float GetCenter (string room)
	{
		if (!GameManagement.Manager.sceneBounds.ContainsKey (room))
		{
			Debug.Log (string.Format ("\"{0}\" is not a valid room", room));
			return 0;
		}
		WallPair pair = GameManagement.Manager.sceneBounds[room];
		return (pair.leftWall.center.x + pair.rightWall.center.x) / 2.0f;
	}
}
