﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Inventory : MonoBehaviour {

    public CarryWeapon item;
    public bool hasItem;
	public AudioClip KnifeSound;
    public Texture itemImg;
    public Texture noItem;
    public Texture QTEBar;
    public Texture QTEBarTraveller;
    private GameObject guest;
    private PlayerController pc;

    private float percentOfScreen = .33f;
    private float positionX = (Screen.width * .12f);
    private float positionY = 0;

    private float percentOfScreenQTE = .45f;
    private float QTETravellerXPos = 0;
    private float percentOfBar;
    public int speedQTE = 1;//How fast the QTE traveller moves across the bar. The smaller the number, the faster it moves.
    public float percentOfBarTraveller = 15;//Size of the traveller relative to bar size. The larger the number, the smaller the traveller.
    private enum TravellerDirection { Right, Left };
    private TravellerDirection QTETravellerDirection = TravellerDirection.Right;
    private bool attack = false;
	private string murderButton = "UseWeapon";

    [HideInInspector]
    public bool showGUI = false, showQTE = false;

    // Use this for initialization
    void Start ()
    {
    }

    void Update()
    {
        if(pc==null)
        {
            if (GameObject.FindGameObjectWithTag("Player") != null)
            {
                pc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
            }
            else
            {
                return;
            }
        }

        CheckInput();
        QTECalculations();
        if(hasItem==false)
        {
            itemImg = noItem;
        }

    }

    private void CheckInput()
    {
        if (!pc.nearestObjects.ContainsKey(murderButton)||pc.nearestObjects[murderButton] != null)
            return;
        if (hasItem)
        {
            if (Input.GetButtonDown(murderButton))
            {
                if (!showQTE)
                {
                    List<CharacterProfile> chars = GameManagement.GetCharacters(true);

                    if (chars.Count == 0 || chars[0].owner == null)
                        return;

                   
                    if ( (guest=pc.currentInteractableGuest) != null)
                    {
                        //Disable player movement
                        pc.disableMovement = true;

                        //Show QTE bar
                        showQTE = true;
                        attack = true;
                    }
                }
                else
                {
                    if (percentOfBar <= 2.25 && percentOfBar >= 1.95)
                        KillGuest();
                    else if (percentOfBar <= 3.05 && percentOfBar >= 1.65)
                        KillGuest();

                    CleanUpQTE(GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>());
                }
            }
        }

        if(showQTE)
        {
            if (Input.GetButtonUp("CloseQTE")|| pc.currentInteractableGuest == null || guest != pc.currentInteractableGuest)
            {
                CleanUpQTE(pc);
            }
        }
    }

    private void QTECalculations()
    {
        float width = Screen.width * (percentOfScreenQTE / 2);

        if (attack)
        {
            if (QTETravellerDirection == TravellerDirection.Right)
            {
                QTETravellerXPos += width / speedQTE * Time.deltaTime;
                percentOfBar = width / QTETravellerXPos;
                QTETravellerXPos = width / percentOfBar;
            }
            else
            {
                QTETravellerXPos -= width / speedQTE * Time.deltaTime;
                percentOfBar = width / QTETravellerXPos;
                QTETravellerXPos = width / percentOfBar;
            }
        }
    }

    private void KillGuest()
    {
        string scenename = SceneManager.GetActiveScene().name;

        CharacterProfile[] CPs = CharacterProfile.GetCharactersInRoom(scenename);

        List<CharacterProfile> inRange = new List<CharacterProfile>();
        List<CharacterProfile> notInRange = new List<CharacterProfile>();

        foreach (CharacterProfile c in CPs)
        {
            if (guest.GetComponent<CharacterCore>() != null)
            {
                if (c.Context == guest.GetComponent<CharacterCore>().context)
                {
                    inRange.Add(c);
                }
                else
                {
                    if (c.Type != CharacterType.Player)
                        notInRange.Add(c);
                }
            }
            else
            {
                
            }
        }

        bool killedSomeone = false;
        foreach (CharacterProfile prof in inRange)
        {
            prof.AddCall(new KillCharacter());
			pc.GetComponent<Speaker>().PlaySoundOnce(KnifeSound);
            killedSomeone = true;
        }

        if (killedSomeone && notInRange.Count > 0)
        {
            GameManagement.ClearMessages();
            for (int i = 0; i < notInRange.Count; i++)
            {
                CharacterProfile prof = notInRange[i];
                //string r = SceneManager.GetActiveScene ().name;
                //character.AddCall(new FollowMe(GameManagement.GetCharacters()[0].Context));
                prof.AddCall(new StopFollowing(false));
                prof.AddCall(new SetTarget(transform.position.x, scenename));
                prof.AddCall(new WitnessMurder(GameManagement.GetCharacters()[0].Context));
                prof.AddCall(new StopGame());

                if (prof.Type != CharacterType.Player)
                {
                    if (prof.Type == CharacterType.Inspector)
                    {
                        GameManagement.AddMessage(new Message(prof.Context, GameManagement.GetMessage(prof.Type, "witness_murder")));
                    }
                    else if (Random.Range(0, 2) == 0)
                    {
                        GameManagement.AddMessage(new Message(prof.Context, GameManagement.GetMessage(prof.Type, "witness_murder")));
                    }
                }


            }

            foreach (CharacterProfile prof in CharacterProfile.GetCharactersNotInRoom(scenename))
            {
                prof.AddCall(new StopFollowing(false));
                prof.AddCall(new SetTarget(transform.position.x, scenename));
                prof.AddCall(new StopGame());
            }
            GameManagement.Manager.gameOver = true;
        }

        //GameManagement.Manager.SusMeter.AffectMomentumRate (-.1f);

        //GameManagement.Manager.hazardsUsed[item.ObjectName] = true;
        item = null;
        hasItem = false;
        //pc.disableMovement = false;
        CleanUpQTE(pc);
    }

    private void CleanUpQTE(PlayerController pc)
    {
        showQTE = false;
        QTETravellerXPos = 0;
        QTETravellerDirection = TravellerDirection.Right;
        pc.disableMovement = false;
        attack = false;
    }

    void OnGUI()
    {
        if (showGUI)
        {
			float w = noItem.width, h = noItem.height, scaledW = w, scaledH = h, scalefactor = 1; 

			if (h != Screen.height * percentOfScreen)
			{
				scalefactor = (Screen.height * percentOfScreen) / h;
				scaledH = h * scalefactor;
				scaledW = w * scalefactor;
			}

			GUILayout.BeginArea(new Rect(positionX, positionY, scaledH, scaledW));

            GUILayout.BeginVertical();
			GUI.DrawTexture(new Rect(0, 30, scaledW / 2, scaledH / 2), itemImg);
            GUILayout.Label("Current Weapon");

			GUILayout.BeginArea(new Rect(0, 15, scaledH, scaledW));
            if (hasItem)
				GUILayout.Label("Press "+GameManagement.Manager.InputKeys[murderButton]+" To Kill");
            GUILayout.EndArea();

            GUILayout.EndVertical();

            GUILayout.EndArea();
        }
        
        if (showQTE)
        {
            //Silent Kill: 2-2.20 Loud Kill: 1.70-3
            float width= Screen.width * (percentOfScreenQTE/2), height= Screen.height * (percentOfScreenQTE/10);

            GUILayout.BeginArea(new Rect((Screen.width / 2) - (width / 2), Screen.height / 6, width, height));
			GUI.Label(new Rect(0, 0, width / 2, height+50), "\""+GameManagement.Manager.InputKeys[murderButton]+"\": Kill");
            GUI.Label(new Rect(width / 1.5f, 0, width/2.5f, height+50),"Q: Close");
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect((Screen.width / 2) - (width / 2), Screen.height / 4,
            width, height));
            GUI.DrawTexture(new Rect(0, 0, width, height), QTEBar);
            
            GUI.DrawTexture(new Rect(QTETravellerXPos, 0, width / percentOfBarTraveller, height), QTEBarTraveller);
            
            if (QTETravellerXPos + (width / percentOfBarTraveller / 2) >= width)
                QTETravellerDirection = TravellerDirection.Left;
            if (QTETravellerXPos + (width / percentOfBarTraveller / 2) < 0)
                QTETravellerDirection = TravellerDirection.Right;

            GUILayout.EndArea();
        }
    }
}
