﻿using UnityEngine;
using System.Collections;

public class CarryWeapon : CommittalObjectBase
{
	
    public Texture inventoryImg;
	public CarryWeapon()
	{
		SetInteractButton ("UseWeapon");

		if(GameManagement.Manager != null)
			holdText = ("Press " + GameManagement.Manager.InputKeys [Button] + " to Pick Up");
	}
	void Start()
	{
		SAM = GetComponent<SpriteAnimationManager> ();
		SAM.SwitchAnimation (0, true);
		sr = GetComponent<SpriteRenderer>();

		if (GameManagement.Manager != null && !GameManagement.Manager.hazardsUsed.ContainsKey (ObjectName))
		{
			GameManagement.Manager.hazardsUsed.Add (ObjectName, false);
		}
		else if (GameManagement.Manager != null && GameManagement.Manager.hazardsUsed [ObjectName] == true)
		{
			Destroy(gameObject);
		}

	}


    public override InteractionState OnInteractionBegin()
    {
        Inventory playersInventory = GameManagement.Manager.playersInventory;
        

        if (!playersInventory.hasItem)
        {
			GetComponent<Speaker>().PlaySoundOnce(commitSound);
            playersInventory.itemImg = inventoryImg;
            playersInventory.hasItem = true;
            playersInventory.item = this;
            base.Unhighlight();
            GameManagement.Manager.hazardsUsed[ObjectName] = true;
            Destroy(gameObject);
        }

        return InteractionState.Done;
    }

    public override InteractionState OnInteractionContinue()
    {
        return InteractionState.Done;
    }

    public override void OnInteractionEnd()
    {
    }
}
