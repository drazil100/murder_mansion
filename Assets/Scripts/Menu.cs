﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Menu : MonoBehaviour {

	public Color windowColor;
	public Color barBGColor;

	private float trustMeter = 0;

	private Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D> ();

	public int MenuIndex { get { return GameManagement.Manager.menuIndex; } }

	private GUIStyle GetBackgroundStyle(Color color)
	{
		GUIStyle background = new GUIStyle ();
		background.stretchWidth = true;
		background.stretchHeight = true;
		if (!textures.ContainsKey (color.ToString()))
		{
			textures.Add (color.ToString(), new Texture2D (1, 1));
			textures[color.ToString()].SetPixel (0, 0, color);
			textures[color.ToString()].Apply ();
			//Debug.Log (color.ToString ());
		}

		background.normal.background = textures[color.ToString()];
		background.padding = new RectOffset(0, 0, 0, 0);
		background.margin = new RectOffset (0, 5, 0, 5);
		background.overflow = new RectOffset(0, 0, 0, 0);

		return background;
	}

	private GUIStyle GetWindowStyle()
	{
		GUIStyle background = new GUIStyle ();
		background.stretchWidth = true;
		background.stretchHeight = true;
		if (!textures.ContainsKey (windowColor.ToString()))
		{
			textures.Add (windowColor.ToString(), new Texture2D (1, 1));
			textures[windowColor.ToString()].SetPixel (0, 0, windowColor);
			textures[windowColor.ToString()].Apply ();
			//Debug.Log (windowColor.ToString ());
		}

		background.normal.background = textures[windowColor.ToString()];
		background.overflow = new RectOffset(0, 0, 0, 0);
		background.padding = new RectOffset(10, 10, 10, 10);

		return background;
	}

	private GUIStyle GetBarStyle(Color color)
	{
		GUIStyle bar = new GUIStyle ();
		if (!textures.ContainsKey (color.ToString()))
		{
			textures.Add (color.ToString(), new Texture2D (1, 1));
			textures[color.ToString()].SetPixel (0, 0, color);
			textures[color.ToString()].Apply ();
			//Debug.Log (color.ToString ());
		}

		bar.normal.background = textures[color.ToString()];
		bar.margin = new RectOffset (0, 0, 0, 0);
		bar.stretchWidth = true;
		bar.stretchHeight = true;
		bar.overflow = new RectOffset(0, 0, 0, 0);

		return bar;
	}


	public void GetMeter(float width, float height, float value, Color backgroundColor, Color barColor, string text = "")
	{
		GUILayout.BeginVertical (GUILayout.MaxWidth(width));
		if (text != "") GUILayout.Label (text);
		GUILayout.BeginHorizontal (GetBackgroundStyle(backgroundColor), GUILayout.MinWidth(width), GUILayout.MaxHeight(height));
		GUILayout.Box("", GetBarStyle(barColor), GUILayout.Width(Mathf.Clamp01(value) * width), GUILayout.MaxHeight(height));
		GUILayout.EndHorizontal ();
		GUILayout.EndVertical ();
	}

	public void DoGUI()
	{
		switch (MenuIndex)
		{
			case 0:
				ShowTrust (); 
				break;
			case 1: 
				ShowInterests(); 
				break;
		}
	}

	private void ShowTrust()
	{
		CharacterProfile[] profiles = GameManagement.GetCharacters (true).ToArray();
		if (profiles.Length < 2) return;
		CharacterData player = profiles [0].Context;
		CharacterData inspector = profiles [1].Context;

		GUILayout.BeginArea (new Rect ((Screen.width - 325), 10 , 325, Screen.height - 10));
		GUILayout.BeginVertical ();

		try
		{
			trustMeter = CharacterCore.Approach(trustMeter, (inspector.Memory[player].Trust + 100), 0.05f);
			float amount = trustMeter/2.0f;
			if (inspector.isAlive)
				GetMeter (250, 25, amount/100f, barBGColor, Color.HSVToRGB (amount/300f, 0.75f, 1f), "Trust");
		} 
		catch(Exception)
		{
		}
		/*
		foreach(CharacterProfile profile in GameManagement.GetCharacters())
		{
			foreach (KeyValuePair<CharacterData, CharacterMemory> memory in profile.Context.Memory)
			{
				if (memory.Key == player)
					GetMeter (250, 10, (memory.Value.Trust + 100) / 200.0f,barBGColor, profile.lightColor);
			}

		}*/
		GUILayout.EndVertical ();
		GUILayout.EndArea ();
	}

	private void ShowInterests()
	{
		//CharacterData player = GameManagement.GetCharacters () [0].Context;
		GUILayout.BeginArea (new Rect (0, 0, Screen.width, Screen.height), GetWindowStyle());
		GUILayout.BeginHorizontal ();
		foreach (CharacterProfile profile in GameManagement.GetCharacters())
		{
			if (profile.Type == CharacterType.Player)
			{
				GUILayout.BeginVertical (GUILayout.MaxWidth (100));
                GUILayout.Label(profile.Name);
                GUILayout.Label (profile.Context.Room);
				GUILayout.Label ("" + profile.Context.position.x);
				GUILayout.EndVertical ();
				continue;
			}
			
			GUILayout.BeginVertical (GUILayout.MaxWidth(100));
            GUILayout.Label(profile.Name);
            GUILayout.Label (profile.Context.Room);
			GUILayout.Label ("" + profile.Context.position.x);

			GetMeter (95, 10, (profile.Context.frameCounter / (float)CharacterData.frameScale), barBGColor, profile.lightColor, string.Format("[{0} / {1}]",profile.Context.frameCounter, CharacterData.frameScale));
			GetMeter (95, 10, (profile.Context.InterestInFollowing / (float)profile.Context.FOLLOW_INTEREST_MAX), barBGColor, profile.lightColor, "Follow Interest");
			GetMeter (95, 10, profile.Context.InterestInRoom / (float)profile.Context.ROOM_INTEREST_MAX, barBGColor, profile.lightColor, "Room Interest");
			GetMeter (95, 10, profile.Context.InterestInCharacter / (float)profile.Context.ROOM_INTEREST_MAX, barBGColor, profile.lightColor, "Character Interest");



			GUILayout.Space (20);
			GUILayout.Label ("Trust");
			foreach (KeyValuePair<CharacterData, CharacterMemory> memory in profile.Context.Memory)
			{
				GetMeter (95, 5, (memory.Value.Trust + 100) / 200.0f,barBGColor, memory.Key.Profile.lightColor);
			}
			GUILayout.Space (20);
			GUILayout.Label ("Followers " + profile.Context.FollowMeCount);
			/*GUILayout.BeginHorizontal ();
			foreach (CharacterProfile p in GameManagement.GetCharacters())
			{
				if (p.Context.isFollowing && p.Context.targetCharacter == profile.Context)
					GetMeter (10, 10, 1,barBGColor, p.lightColor);
			}
			GUILayout.EndHorizontal ();*/

			if (profile.Context.isFollowing)
			{
				GUILayout.Space (20);
				GUILayout.Label ("Target " + profile.Context.targetCharacter.FollowMeCount);
				if (profile.Context.isFollowing)
				{
					DrawChain (profile.Context.targetCharacter, true);
				}
				//GUILayout.Box("", GetBarStyle(profile.Context.targetCharacter.Profile.lightColor), GUILayout.Width(95), GUILayout.MaxHeight(10));
				if (profile.Context.followState == FollowState.Follow)
				{
					GUILayout.Label ("Following");
				}
				else
				{
					GUILayout.Label ("Wandering");
				}
			}
			if (profile.Context.investigateDeath)
			{
				GUILayout.Space (20);
				GUILayout.Label ("investigating");

			}
			GUILayout.EndVertical ();
		}
		GUILayout.EndHorizontal ();
		GUILayout.EndArea ();
	}

	public void DrawChain (CharacterData c, bool large = false)
	{
		if (c.isFollowing)
		{
			DrawChain (c.targetCharacter);
		}
		if (large)
		{
			GUILayout.Box ("", GetBarStyle (c.Profile.lightColor), GUILayout.Width (95), GUILayout.MaxHeight (10));
		}
		else
		{
			GUILayout.Box ("", GetBarStyle (c.Profile.lightColor), GUILayout.Width (95), GUILayout.MaxHeight (5));
			GUILayout.Space (5);
		}
	}
}
