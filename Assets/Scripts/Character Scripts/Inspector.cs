﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (SpriteAnimationManager))]
public class Inspector : CharacterCore {

    private CharacterProfile profile;
	private SpriteRenderer spRenderer;
	private SpriteAnimationManager aniManager;
	public AudioClip footstepSound;
	private bool update = true;

    void Start()
    {
        profile = GameManagement.Manager.GetMyProfile(transform);
		spRenderer = GetComponent<SpriteRenderer> ();
		aniManager = GetComponent<SpriteAnimationManager> ();

		if (profile.animations == null)
		{
			profile.animations = aniManager.animations;
		}
		else
		{
			aniManager.animations = profile.animations;
		}

        if (profile != null)
        {
            if (profile.DoInitializeTree() == true)
            {

				Branch<CharacterData> tree = Guest.GetNewGuestTree ();

                profile.InitializeTree(tree);
            }

			if (!profile.Context.isAlive)
			{
				//transform.Rotate (0, 0, 90);
				profile.Context.animation = "Idle";
				aniManager.SwitchAnimation ("Idle");
				update = false;
				//Debug.Log (profile.Context);
			}
        }
    }



	void FixedUpdate()
	{
		if (!profile.Context.isAlive)
		{
			profile.Context.animation = "Idle";
			aniManager.SwitchAnimation ("Idle");
			update = false;
			//Debug.Log (profile.Context);
		}
		if (!update)
		{
			//Debug.Log ("gravity");
			aniManager.SwitchAnimation ("Idle");
			ApplyGravity ();
			MoveCharacter(velocity.x * Time.deltaTime, velocity.y * Time.deltaTime);
			return;
		}
		UpdateAnimation ();
	}

	protected void UpdateAnimation()
	{
		if (profile.Context != null)
		{
			CharacterData context = profile.Context;

			//  Switch animation based on state
			bool doFlip = true;

			switch (context.animation)
			{
				//case PlayerState.Interacting:  aniManager.SwitchAnimation ("Idle", true);  doFlip = false;  break;
				case "Idle":
					aniManager.SwitchAnimation ("Idle", true);
					GetComponent<Speaker>().StopPlaying();
					break;
				case "Walking":
					aniManager.SwitchAnimation ("Running", true);
					if(GetComponent<Speaker>().IsPlaying() == false)
					GetComponent<Speaker>().PlaySoundContinuous(footstepSound);
					break;
				default:
					break;
			}

			if (doFlip)
			{
				float xInput;
				//  Flip animation based on horizontal input
				if (context.animation == "Walking")
				{
					xInput = context.velocity;
				}
				else
				{
					xInput = context.dir;
				}
				if (xInput > 0) spRenderer.flipX = false;
				if (xInput < 0) spRenderer.flipX = true;
			}
		}
	}
}
