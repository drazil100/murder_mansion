﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent (typeof(CharacterCore))]
[RequireComponent (typeof(SpriteRenderer))]
public class GuestInteraction : InteractableObject {

	protected string holdText;
	private GameObject HazardCanvas;

	// Use this for initialization
	void Start () {
		holdText = ("Press " + GameManagement.Manager.InputKeys [Button] + " to Talk");
	
	}
	
	// Update is called once per frame
	void Update () {
		if (GetComponent<CharacterCore>().context != null && !GetComponent<CharacterCore> ().context.isAlive)
		{
			Unhighlight ();
		}
		if (GameManagement.Manager.hideHelpBoxes)
		{
			//Debug.Log ("Called");
			if (HazardCanvas != null)
				Destroy (HazardCanvas);
		}
	}

    public override InteractionState OnInteractionBegin()
    {
        //Debug.Log("Interacted with a guest.");
		CharacterData context = GetComponent<CharacterCore>().context;
		if (!context.isInConversation)
		{
			GameManagement.ClearMessages (SceneManager.GetActiveScene ().name);
			GameManagement.AddMessage (context.GetMessage ());
		}
        return InteractionState.Done;
    }

    //  This should be used if the object needs to have code run once a frame. 
    //  It should be treated as an InteractableObject's Update() function.
    public override InteractionState OnInteractionContinue()
    {
        return InteractionState.Done;
    }

    //  This is called when the interaction is over. If any variables need to be reset for the next use
    //  put the cleanup code in here. Alternatively cleanup can be done in OnInteractionBegin().
    public override void OnInteractionEnd()
    {
    }

    public override void Highlight()
    {
		if (!GetComponent<CharacterCore> ().context.isAlive)
			return;
		GetComponent<SpriteRenderer>().sprite = highlighted;

		if(HazardCanvas==null)
		{
			if (GameManagement.Manager.hideHelpBoxes) return;
			HazardCanvas = new GameObject("HazardCanvas");
			HazardCanvas.AddComponent<Canvas>();
			HazardCanvas.AddComponent<CanvasScaler>();
			HazardCanvas.AddComponent<GraphicRaycaster>();
			HazardCanvas.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
			HazardCanvas.transform.position = new Vector3(transform.position.x, transform.position.y  + 1.8f, 0);
            HazardCanvas.GetComponent<Canvas>().sortingOrder = 10; 
			HazardCanvas.GetComponent<RectTransform>().sizeDelta= new Vector2(1.4f, 1);
			HazardCanvas.GetComponent<CanvasScaler>().dynamicPixelsPerUnit = 2200;

			GameObject HazardGUI = new GameObject("HazardGUI");
			HazardGUI.AddComponent<RawImage>();
			HazardGUI.GetComponent<RawImage>().texture = GameManagement.Manager.bubble;
			HazardGUI.GetComponent<RawImage>().color = new Color(0.2f,0.2f,0.2f);
			HazardGUI.transform.SetParent(HazardCanvas.transform);
			HazardGUI.GetComponent<RectTransform>().sizeDelta = new Vector2(1.4f, HazardCanvas.GetComponent<RectTransform>().sizeDelta.y);
			HazardGUI.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);

			GameObject HazardText = new GameObject("HazardText");
			HazardText.transform.SetParent(HazardGUI.transform);
			HazardText.AddComponent<Text>();
			HazardText.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0.01f);
			HazardText.GetComponent<RectTransform>().sizeDelta = HazardCanvas.GetComponent<RectTransform>().sizeDelta;
			HazardText.GetComponent<Text>().text = holdText;
			Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
			HazardText.GetComponent<Text>().font = ArialFont;
			HazardText.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
			HazardText.GetComponent<Text>().color = Color.white;
			HazardText.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
			HazardText.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Truncate;
			HazardText.GetComponent<Text>().resizeTextForBestFit = true;
			HazardText.AddComponent<Outline>();
			HazardText.GetComponent<Outline>().effectDistance = new Vector2 (0.01f, 0.01f);
			HazardCanvas.transform.SetParent (transform);
		}
        //GetComponent<SpriteRenderer>().sprite = highlighted;   //<-- default for interactables
    }

    public override void Unhighlight()
    {
		if(HazardCanvas!=null)
			Destroy(HazardCanvas);
        //GetComponent<SpriteRenderer>().sprite = normal;   //<-- default for interactables
    }
}
