﻿using UnityEngine;
using System.Collections;

public enum GuestState {
	Normal,
	Alert
}

[RequireComponent (typeof (SpriteAnimationManager))]
public class Guest : CharacterCore {

	private CharacterProfile profile;
	private SpriteRenderer spRenderer;
	private SpriteAnimationManager aniManager;
	public bool doRecolor = false;
	public AudioClip footstepSound;
	private bool update = true;

	void Start()
	{
		//if (context == null) context = new GuestData ();

		profile = GameManagement.Manager.GetMyProfile (transform);
		spRenderer = GetComponent<SpriteRenderer> ();
		aniManager = GetComponent<SpriteAnimationManager>();

		//if (!profile.Context.isAlive) Debug.Log ("start " + context.index + ": " + context.position);

		if (doRecolor)
		{
			if (profile.animations == null)
			{
				aniManager.RecolorAll (profile.darkColor, profile.lightColor);
				//spRenderer.color = profile.lightColor;
				profile.animations = aniManager.animations;
			}
			else
			{
				aniManager.animations = profile.animations;
			}
		}
		else
		{
			profile.animations = aniManager.animations;
		}


		if (profile != null) {
			if (profile.DoInitializeTree () == true) {
			
				Branch<CharacterData> tree = GetNewGuestTree ();

				profile.InitializeTree (tree);
			}
				
			if (!profile.Context.isAlive)
			{
				//transform.Rotate (0, 0, 90);
				//if (!profile.Context.isAlive) Debug.Log ("after rotate " + context.index + ": " + context.position);
				profile.Context.animation = "Idle";
				aniManager.SwitchAnimation ("Idle");
				update = false;
				//Debug.Log (profile.Context);
			}
		}
	}

	public static Branch<CharacterData> GetNewGuestTree()
	{
		return new RunOutsideCalls<CharacterData> (
			new InGameMode (
				null,
				new ObserveCharactersInRoom (
					new Selector<CharacterData> (
						new PauseForConversation (),
						InvestigateDeathBranch(),
						MoveToCharacterBranch (),
						new Sequence<CharacterData> (
							new Idle<CharacterData> (60, 300),
							new MoveToTarget<CharacterData> ()
						),
						new Sequence<CharacterData> (
							new EvaluateInterestInRoom (),
							new ChangeRooms ()
						),
						new FollowACharacter (),
						new SelectRandomTargetInRoom ()
					)
				)
			)
		);
	}

	private static Branch<CharacterData> InvestigateDeathBranch()
	{
		return new Sequence<CharacterData> (
			new Selector<CharacterData> (
				new GetHelp (),
				new BodyWasFound ()
			),
			new Succeeder<CharacterData>(new MoveToTarget<CharacterData> ()),
			new InvestigateDeath (),
			new BranchInverter<CharacterData> (new InvestigationComplete ())
		);
	}


	private static Branch<CharacterData> MoveToCharacterBranch()
	{
		return new MoveToCharacter (
			new Selector<CharacterData> (
				//new BranchInverter<CharacterData> (new CompleteAfter<CharacterData> (150)),
				StopFollowingSequence (),
				new SwitchFollowState ()

			),
			new Selector<CharacterData> (
				//new BranchInverter<CharacterData> (new CompleteAfter<CharacterData> (150)),
				StopFollowingSequence (),
				new Sequence<CharacterData> (
					new Idle<CharacterData> (),
					new SelectRandomTargetInRoom (),
					new MoveToTarget<CharacterData> ()
				)

			)
		);
	}


	private static Branch<CharacterData> StopFollowingSequence()
	{
		return new Sequence<CharacterData> (
			new EvaluateInterestInRoom(),
			new ChangeRooms (),
			new StopFollowing(true)

		);
	}




	void FixedUpdate()
	{
		ApplyGravity ();
		MoveCharacter(0, velocity.y * Time.deltaTime);
		if (!profile.Context.isAlive)
		{
			profile.Context.animation = "Idle";
			aniManager.SwitchAnimation ("Idle");
			update = false;
			//Debug.Log (profile.Context);
		}
		if (!update)
		{
			//Debug.Log ("gravity");
			aniManager.SwitchAnimation ("Idle");
			//ApplyGravity ();
			//MoveCharacter(velocity.x * Time.deltaTime, velocity.y * Time.deltaTime);
			return;
		}
		UpdateAnimation ();
	}

	protected void UpdateAnimation()
	{
		if (profile.Context != null)
		{
			CharacterData context = profile.Context;

			//  Switch animation based on state
			bool doFlip = true;

			switch (context.animation)
			{
			//case PlayerState.Interacting:  aniManager.SwitchAnimation ("Idle", true);  doFlip = false;  break;
				case "Idle":
					aniManager.SwitchAnimation ("Idle", true);
					GetComponent<Speaker>().StopPlaying();
					break;
				case "Walking":
				if(GetComponent<Speaker>().IsPlaying() == false)
					GetComponent<Speaker>().PlaySoundContinuous(footstepSound);
					aniManager.SwitchAnimation ("Running", true);
					break;
				default:
					break;
			}

			if (doFlip)
			{
				float xInput;
				//  Flip animation based on horizontal input
				if (context.animation == "Walking")
				{
					xInput = context.velocity;
				}
				else
				{
					xInput = context.dir;
				}
				if (xInput > 0) spRenderer.flipX = false;
				if (xInput < 0) spRenderer.flipX = true;
			}
		}
	}

}
