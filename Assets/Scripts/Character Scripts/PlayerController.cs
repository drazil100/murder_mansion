﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Threading;

public enum PlayerState 
{
	InAir,
	Walking,
	Standing,
	Interacting
}


public class PlayerController : CharacterCore {

	private static PlayerController instance;

	protected PlayerState playerState = PlayerState.InAir;
	protected SpriteAnimationManager aniManager;
	public SpriteRenderer spRenderer;//Made this public 
	public InteractableObject currentInteractable;
	public Dictionary<string, InteractableObject> nearestObjects = new Dictionary<string, InteractableObject> ();
    public GameObject currentInteractableGuest=null;//Holds Guest closest to player
	public Bounds bounds;
	public CharacterProfile profile;
	public int menuIndex = 0;
    public bool viewExpanded = false;
	public AudioClip footstepSound;
    [HideInInspector]
    public bool disableMovement = false;
	public bool disableInteraction = false;
	public bool reEnableInteraction = false;

	//  C# Property. Is basically a variable but gives me more control.
	//  Using to make isGrounded visible but not settable from the outside.
	public PlayerState State
	{
		get  { return playerState; }
	}


	void Start()
	{
		instance = this;
		nearestObjects.Add ("Up", null);
		nearestObjects.Add ("Interact", null);
		nearestObjects.Add ("UseWeapon", null);

		aniManager = GetComponent<SpriteAnimationManager> ();
		spRenderer = GetComponent<SpriteRenderer>();
		aniManager.SwitchAnimation ("Idle", true);
		profile = GameManagement.Manager.GetMyProfile (transform);

		if (profile.animations == null)
		{
			profile.animations = aniManager.animations;
		}
		else
		{
			aniManager.animations = profile.animations;
		}
		/*CharacterData c = GameManagement.GetCharacters () [0].Context;
		foreach (CharacterProfile profile in CharacterProfile.GetCharactersInRoom(c.Room))
		{
			if (profile.Context != c)
			{
				profile.AddCall (new EnterRoom (c));
			}
		}*/
	}



	void Update () 
	{
		
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			if (GameManagement.Manager.paused)
			{
				GameManagement.Manager.paused = false;
				Time.timeScale = 1;
			}
			else
			{
				GameManagement.Manager.paused = true;
				Time.timeScale = 0;
			}
		}

		if (context == null || !context.gameStarted) return;

		MenuManagement ();

		if (GameManagement.Manager.paused || GameManagement.Manager.gameOver)
			return;

		if (playerState != PlayerState.Interacting && !disableInteraction) 
			LocateNearestInteractableObject ();
		if (!disableInteraction)
        	LocateNearestGuest();

        ClickedShift();

		RunStateMachine ();
		UpdateAnimationToCurrentState ();
		UpdatePlayerPosition ();

		bounds = GetComponent<Collider2D> ().bounds;

		if (context != null)
		{
			context.position = new Vector2 (transform.position.x, transform.position.y);
		}

		if (reEnableInteraction)
		{
			reEnableInteraction = false;
			disableInteraction = false;
		}
	}

	private void MenuManagement()
	{
		if (Input.GetKeyDown (KeyCode.F1))
		{
			GameManagement.Manager.hideHelpBoxes = !GameManagement.Manager.hideHelpBoxes;

			PlayerPrefs.SetInt ("help", ((GameManagement.Manager.hideHelpBoxes) ? 1 : 0));
		}
		if (Input.GetKeyDown (KeyCode.F3))
		{
			GameManagement.Manager.menuIndex = (GameManagement.Manager.menuIndex == 0) ? 1 : 0;
		}
	}



	public void LocateNearestInteractableObject()
	{
		string[] keys = new string[nearestObjects.Keys.Count];
		nearestObjects.Keys.CopyTo (keys, 0);

		//string debug = "";
		/*foreach (string key in keys)
		{
			debug += key + ", ";
		}
		Debug.Log (debug);*/
		foreach (string key in keys)
		{
			
			//Debug.Log (key);
			GameObject obj = InteractableObject.FindNearestInteractableObject (transform.position, key);
			if (nearestObjects[key] != null)
			{
				
				float distance = Mathf.Abs (nearestObjects[key].transform.position.x - transform.position.x);
				//Debug.Log(distance);
				if (obj.GetComponent<InteractableObject> () != nearestObjects[key] || (distance > InteractableObject.interactionRange))
				{
					//Debug.Log ("called");
					nearestObjects[key].Unhighlight ();
					nearestObjects[key] = null;
				}
			}
			
			if (obj != null)
			{
				//if (key == "UseWeapon") Debug.Log (obj.transform.position);
				//if (key == "UseWeapon") Debug.Log ("test");
				float distance = Mathf.Abs (transform.position.x - obj.transform.position.x);
				//if (key == "UseWeapon") Debug.Log (distance);
				if (distance > InteractableObject.interactionRange)
					continue;
				nearestObjects[key] = obj.GetComponent<InteractableObject> ();
				nearestObjects[key].Highlight ();
			}
		}
	}

    //New function added
    //Finds the guest closest to the player
    public void LocateNearestGuest()
    {
        GameObject[] gs = GameObject.FindObjectsOfType<GameObject>();

        List<GameObject> guests = new List<GameObject>();

        foreach(GameObject g in gs)
        {
            if(g.GetComponent<Guest>()!=null||g.GetComponent<Inspector>()!=null)
            {
                guests.Add(g);
            }
        }

        if (guests.Count != 0)
        {
            double closest = Vector2.Distance(transform.position, guests[0].transform.position);
            double distance;
            GameObject closestGuest = guests[0];

            foreach (GameObject guest in guests)
            {
                if ((distance = Vector2.Distance(transform.position, guest.transform.position)) < closest)
                {
                    closest = distance;
                    closestGuest = guest;
                }
            }

            if (currentInteractableGuest != null)
            {
                distance = Mathf.Abs(currentInteractableGuest.transform.position.x - transform.position.x);
                //Debug.Log(distance);
                if (closestGuest != currentInteractableGuest || (distance > InteractableObject.interactionRange))
                {
                    currentInteractableGuest = null;
                }
            }

            if (closest <= InteractableObject.interactionRange)
            {
                currentInteractableGuest = closestGuest;
                return;
            }
        }
        else
        {
            currentInteractableGuest = null;
        }
    }

    public void ClickedShift()
    {
        if(Input.GetButtonUp("ExpandView"))
        {
            if(!viewExpanded==true)
            {
                Camera.main.transform.parent = null;
                Bounds leftWall = GameManagement.Manager.sceneBounds[SceneManager.GetActiveScene().name].leftWall;
                Camera.main.transform.position = new Vector3(0, leftWall.center.y,-6);
                Plane[] planes=GeometryUtility.CalculateFrustumPlanes(Camera.main);
                
                while(!GeometryUtility.TestPlanesAABB(planes, leftWall))
                {
                    Camera.main.fieldOfView += 1;
                    //Debug.Log(Camera.main.fieldOfView);
                    planes=GeometryUtility.CalculateFrustumPlanes(Camera.main);
                }
                
                viewExpanded = true;
            }
            else
            {
                viewExpanded = false;
                Camera.main.transform.parent = profile.owner;
                Camera.main.fieldOfView = 60;
            }
        }
    }

    private void IncreasePOV(Plane[] planes, Bounds leftWall)
    {
        while (!GeometryUtility.TestPlanesAABB(planes, leftWall))
        {
            Camera.main.fieldOfView += 5;
            Debug.Log("Added 5.");
            Debug.Log(Camera.main.fieldOfView);
        }
    }

	protected void RunStateMachine()
	{
		//  An example of a state machine which may or may not be useful.
		//  Depending on what state the player is in a different method is called.
		//  each of these methods will return PlayerState that will be used to determine 
		//  which function is called next frame
		//  Player State is output to GUI presently.
		if (playerState == PlayerState.InAir && IsGrounded)
		{
			playerState = PlayerState.Standing;
		}
		else if (!IsGrounded && playerState != PlayerState.InAir)
		{
			//Debug.Log ("test");
			playerState = PlayerState.InAir;
		}
		switch (playerState) 
		{
			case PlayerState.Standing:     playerState = Grounded ();     break;
			case PlayerState.Walking:      playerState = Grounded ();     break;
			case PlayerState.InAir:        playerState = Airborn ();      break;
			case PlayerState.Interacting:  playerState = Interacting ();  break;
			default:
				break;
		}
	}



	protected void UpdateAnimationToCurrentState()
	{
		//  Switch animation based on state
		bool doFlip = true;
		switch (playerState)
		{
			case PlayerState.Interacting:  aniManager.SwitchAnimation ("Idle", true);  doFlip = false;  break;
			case PlayerState.Standing:     aniManager.SwitchAnimation ("Idle", true);                   break;
			case PlayerState.Walking:      aniManager.SwitchAnimation ("Running", true);                break;
			//case PlayerState.InAir:        aniManager.SwitchAnimation ("Jumping", true);                break;
			default:
				break;
		}

		if (doFlip)
		{
			//  Flip animation based on horizontal input
			float xInput = Input.GetAxisRaw ("Horizontal");
			if (xInput > 0) spRenderer.flipX = false;
			if (xInput < 0) spRenderer.flipX = true;
		}
	}



	protected void UpdatePlayerPosition()
	{
        //  Move character handles collision backend. AI characters will use the same backend.
        //  Should make it easy to add new characters and have collision handled.
        //  
        //  To use pass direction * speed (in this example speed is not included) * Time.deltaTime
        //  Depending on how calculations are done this will likely be more complicated than what is shown in this example
        if (!disableMovement)
        {
            Vector2 collisionCheck = MoveCharacter(velocity.x * Time.deltaTime, velocity.y * Time.deltaTime);


            //  MoveCharacter() returns a Vector2 to indicate any collision that may have occured.
            //   if the return value is (1, 1) it means that the character has not run into anything.
            //   if the return value is (1, 0) it means the character hit something along the y axis.
            //   if the return value is (0, 1) it means the character hit something along the x axis.
            //   if the return value is (0, 0) it means the character hit something along both the x and y axis.
            //
            //  Simply multiply the vector to whatever variables you use to keep track of velocity to set it to 0 if it collides with something
            //  In the event you want some sort of rebound type effect then if the value is 0 then you can flip the velocity or something.
            velocity.x *= collisionCheck.x;
            velocity.y *= collisionCheck.y;
        }
	}



	//================================== Begin State Machine Functions ==================================
	protected PlayerState Grounded()
	{
		//  Apply horizontal movement
		//  Approach is basically the same as Mathf.Lerp() but has more approperiatly named parameters
		//  Also Approach automatically applies Time.deltaTime to itself.
        if(!disableMovement)
		    velocity.x = Approach (velocity.x, Input.GetAxisRaw ("Horizontal") * speed, 0.3f);

		//  Gravity is applied despite being grounded in case
		//  the player walks off a ledge.
		ApplyGravity ();

		//  Make the character jump if jump button is pressed
		/*if (Input.GetButtonDown ("Jump"))
		{
			velocity.y = 5;
			return PlayerState.InAir;
		}*/
		if (!disableInteraction)
		{
			string[] keys = new string[nearestObjects.Keys.Count];
			nearestObjects.Keys.CopyTo (keys, 0);
			foreach (string key in keys)
			{
				if (nearestObjects [key] != null)
				{
					if (Input.GetButtonDown (nearestObjects [key].Button))
					{
						if (nearestObjects [key].OnInteractionBegin () == InteractionState.Interacting)
						{
							currentInteractable = nearestObjects [key];
							return PlayerState.Interacting;
						}
						else
						{
							nearestObjects [key].OnInteractionEnd ();
						}
					}
				}
			}
		}

		/*if (Input.GetKeyDown (KeyCode.E))
		{
			if (currentInteractable != null)
			{
				if (currentInteractable.OnInteractionBegin () == InteractionState.Interacting)
				{
					return PlayerState.Interacting;
				}
				else
				{
					currentInteractable.OnInteractionEnd ();
				}
			}
		}*/

		//Debug.Log (velocity.x);
		bool isWalking = (Mathf.Abs(Input.GetAxis("Horizontal")) >= 0.2f && velocity.x != 0);

		if (isWalking)
		{
			if (GetComponent<Speaker>() != null && GetComponent<Speaker>().IsPlaying() == false)
			{
				//GetComponent<Speaker> ().volumePercentModifier = .5f;
				GetComponent<Speaker>().PlaySoundContinuous(footstepSound);
			}
		}
		else if(!isWalking)
		{
			if (GetComponent<Speaker> ().IsPlaying ())
			{
				GetComponent<Speaker> ().StopPlaying ();
			}
				
		}

		//  While standing and walking may use the same code this can be used later
		//  ternary opperator is "(condition) ? (value if true) : (value if false)".
		return (isWalking) ? PlayerState.Walking : PlayerState.Standing;
	}



	protected PlayerState Airborn() 
	{
		//  Apply horizontal movement. The ammount argument is smaller to give less control in air
		velocity.x = Approach (velocity.x, Input.GetAxisRaw ("Horizontal") * speed, 0.05f);
		ApplyGravity ();

		return PlayerState.InAir;
	}

	protected PlayerState Interacting()
	{
		velocity.x = Approach (velocity.x, 0, 0.3f);
		if (Input.GetKeyDown (KeyCode.Escape) || currentInteractable.OnInteractionContinue () == InteractionState.Done)
		{
			currentInteractable.OnInteractionEnd ();
			return PlayerState.Standing;
		}
		return PlayerState.Interacting;
	}
	//================================== End of State Machine Functions ==================================



	public static void DisableInteraction()
	{
		if (instance != null)
		{
			Vector3 temp = instance.transform.position;
			instance.transform.position = new Vector3 (900000, 0, 0);
			instance.LocateNearestGuest ();
			instance.LocateNearestInteractableObject ();
			instance.transform.position = temp;
			instance.playerState = PlayerState.Standing;
			instance.disableInteraction = true;
		}
	}

	public static void EnableInteraction()
	{
		if (instance != null)
		{
			instance.reEnableInteraction = true;
		}
	}

	void OnGUI() 
	{
		//GUI.Label (new Rect (10, 10, 1000, 1000), "" + transform.position.x);

		if (playerState == PlayerState.Interacting)
		{
			currentInteractable.DrawGUI();
		}
	}
}
