﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;

public delegate void MessageDelegate(CharacterData context);

public class Message {

	private CharacterData sender;

	public string Room { 
		get 
		{ 
			if (room != "")
			{
				return room;
			}
			else if (Sender != null)
				return Sender.Room;
			return "";
		} 
	}
	private AnswerList answers;
	private bool responseGiven = false;
	private string room = "";
	private int lifeSpan = 45;
	private int framesPerCharacter = 2;
	public int time = 0;
	private string message;
	public Message response = null;
	private CharacterData player;
	private int dir = 0;
	private bool disabled = false;
	private bool show = false;
	private MessageDelegate action;
	private bool started = false;
	//private bool played = false;

	public int Time 
	{ 
		get 
		{ 
			if (time / framesPerCharacter - message.Length - lifeSpan < 0 || (answers != null &&  !responseGiven))
				return time;
			else if (response != null)
				return response.Time;
			return 0; 
		}
		set 
		{
			if (time / framesPerCharacter - message.Length - lifeSpan < 0 || (answers != null &&  !responseGiven))
				time = value;
			else if (response != null)
				response.Time = value;
			//return 0; 
		}
	}

	public int Dir 
	{ 
		get 
		{ 
			if (time / framesPerCharacter - message.Length - lifeSpan < 0 || (answers != null &&  !responseGiven))
				return dir;
			else if (response != null)
				return response.Dir;
			return 0; 
		}
		set 
		{
			if (time / framesPerCharacter - message.Length - lifeSpan < 0 || (answers != null &&  !responseGiven))
				dir = value;
			else if (response != null)
				response.Dir = value;
			//return 0; 
		}
	}

	public CharacterData Sender
	{ 
		get 
		{
			if (time / framesPerCharacter - message.Length - lifeSpan < 0 || (answers != null &&  !responseGiven))
				return sender;
			else if (response != null)
				return response.Sender;
			return null;
		}
	}

	public int ResponseCount
	{
		get {
			if (response != null)
			{
				return 1 + response.ResponseCount;
			}
			return 0;
		}
	}
	public string TheMessage { 
		get 
		{
			if (time / framesPerCharacter - message.Length - lifeSpan < 0 || (answers != null && !responseGiven))
			{
				if (time / framesPerCharacter - message.Length == 0 && action != null)
					action (sender);
					
				return message.Substring (0, 1 + Mathf.Clamp (time / framesPerCharacter, 0, message.Length - 1));
			}
			else if (response != null)
				return response.TheMessage;
			return "";
		} 
		private set
		{
			message = value;
		}
	}

	public void ShowAnswerList()
	{
		if (time / framesPerCharacter - message.Length - lifeSpan < 0 || (answers != null && !responseGiven))
		{
			if (answers != null && (time / framesPerCharacter - message.Length == -2))
			{
				disabled = true;
				PlayerController.DisableInteraction ();
			}

			if (answers != null && !(time / framesPerCharacter - message.Length < 0))
			{
				responseGiven = (responseGiven || answers.ShowList ());
				show = true;
			}

			if (answers != null && responseGiven)
			{
				if (response == null || response.answers == null)
					PlayerController.EnableInteraction ();
				answers.CloseGUI ();
				show = false;
			}

		}
		else if (response != null)
		{
			response.ShowAnswerList ();
		}
		
	}

	public void DrawAnswerList()
	{
		
		if (time / framesPerCharacter - message.Length - lifeSpan < 0 || (answers != null && !responseGiven))
		{
			if (show && answers != null)
			{
				answers.showGUI ();
			}
		}
		else if (response != null)
			response.DrawAnswerList();
		
	}

	//private List<CallTransmission> calls = new List<CallTransmission>();

	private Message() {}
	public Message(CharacterData sender, string message)
	{
		this.sender = sender;
		this.message = message;
		//sender.isInConversation = true;
		sender.Profile.AddCall (new NullCall ());
		//this.room = sender.Room;
		if (sender.Type == CharacterType.Player)
		{
			player = sender;
			return;
		}

		foreach (CharacterProfile profile in GameManagement.GetCharacters())
		{
			if (profile.Type == CharacterType.Player)
			{
				player = profile.Context;
			}
		}
	}
	public Message(CharacterData sender, string message, AnswerList answers) : this(sender, message)
	{
		this.answers = answers;
		answers.SetOwningMessage (this);
		lifeSpan = 0;
	}

	public Message(CharacterData sender, string message, string room) : this(sender, message)
	{
		this.room = room;
	}

	public void AddResponse(Message message)
	{
		
		if (response == null)
		{
			response = message;
			if (started) message.BeginConversation ();
		}
		else
		{
			response.AddResponse (message);
		}

	}

	public void BeginConversation()
	{
		if (room == "")
			this.room = sender.Room;
		sender.isInConversation = true;
		started = true;
		if (response != null)
		{
			response.BeginConversation();
		}

	}

	public void AddActionToLastMessage(MessageDelegate action)
	{
		if (response == null)
		{
			this.action = action;
		}
		else
		{
			response.AddActionToLastMessage (action);
		}
	}

	public bool IsFinished()
	{
		if (answers != null && !responseGiven && GameManagement.GetCharacters()[0].Context.Room == Room)
		{
		}
		else if (time / framesPerCharacter - message.Length - lifeSpan >= 0)
		{
            if (answers != null)
            {
                answers.CloseGUI();
				show = false;
            }

            if (response != null)
			{
				return response.IsFinished ();
			}
			else
			{
				return true;
			}
		}

		if (!Sender.isAlive)
		{
            if (answers != null)
            {
                answers.CloseGUI();
				show = false;
            }
			return true;
		}

		return false;
	}

	public void ReleaseFromConversation()
	{
		if (disabled)
			PlayerController.EnableInteraction ();
        if(answers!=null)
        {
            answers.CloseGUI();
        }
		sender.isInConversation = false;
		if (response != null)
		{
			response.ReleaseFromConversation ();
		}
	}

	public virtual bool ShowMessage(string room)
	{
		//Debug.Log (time / framesPerCharacter - message.Length - lifeSpan);
		//SendCalls ();
		//BeginConversation();


		if (Dir == 0)
		{
			if (Sender == null)
				return true;
			if (Sender.position.x > player.position.x)
			{
				Dir = -1;
			}
			else
			{
				Dir = 1;
			}
		}


		if (SceneManager.GetActiveScene().name == Room)
			GameManagement.Manager.ShowChatBox (TheMessage, Sender.Profile.bubbleColor ,Sender.index, Dir);  
		if (room == Room)
			Time++;
		return IsFinished();
	}

	public override string ToString ()
	{
		return string.Format ("[Message: Room={0}, Time={1}, Dir={2}, Sender={3}, ResponseCount={4}, message={5}]", Room, Time, Dir, Sender, ResponseCount, message);
	}
}
