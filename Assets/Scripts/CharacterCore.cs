﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (SpriteAnimationManager))]
[RequireComponent (typeof (Collider2D))]
public class CharacterCore : MonoBehaviour {
	
	public static float gravity = 9.81f;
	public float speed = 3.0f;
	public Vector2 velocity = new Vector2 (0, 0);
	private bool isGrounded = false;
	public CharacterData context;

	public bool IsGrounded 
	{
		get            { return isGrounded; }
		protected set  { isGrounded = value; }
	}

	public Vector2 MoveCharacter(float xInput, float yInput) 
	{

		int x = 1;
		int y = 1;

		if (BoundsCast (new Vector2 (xInput, 0.1f)) != null)
		{
			x = 0;
			//Debug.Log ("x collision");
			float i = 0.0f;
			float newXInput = 0.0f;
			while (BoundsCast (new Vector2 (RoundCorrect(Mathf.Lerp (0, xInput, i), 5), 0.1f)) == null)
			{
				newXInput = RoundCorrect(Mathf.Lerp (0, xInput, i), 5);
				i += 0.01f;
				if (i == 1)
					break;
			}
			xInput = newXInput;
		}

		transform.position = new Vector3 (transform.position.x + xInput, transform.position.y, transform.position.z);

		if (yInput != 0) IsGrounded = false;
		if (BoundsCast (new Vector2 (0, yInput-0.001f)) != null)
		{
			if (yInput < 0)
				IsGrounded = true;
			y = 0;
			//Debug.Log ("y collision");
			float i = 0.0f;
			float newYInput = 0.0f;
			while (BoundsCast (new Vector2 (0, RoundCorrect(Mathf.Lerp (0, yInput-0.001f, i), 5))) == null)
			{
				newYInput = RoundCorrect(Mathf.Lerp (0, yInput-0.001f, i), 5);
				i += 0.01f;
				if (i == 1)
					break;
			}
			yInput = newYInput;
		}

		transform.position = new Vector3 (transform.position.x, transform.position.y + yInput, transform.position.z);

		return new Vector2 (x, y);
	}

	public static float RoundCorrect(float value, int decimalPlaces = 5)
	{
		decimalPlaces = Mathf.Clamp (decimalPlaces, 0, 5);
		float multiplyer = Mathf.Pow(10, decimalPlaces);
		value *= multiplyer;

		value = Mathf.Floor (value);

		value /= multiplyer;

		return value;
	}

	public Transform[] BoundsCast(Vector2 boundsTranslation)
	{
		Bounds bounds = GetComponent<Collider2D> ().bounds;
		Vector3 newCenter = new Vector3 (bounds.center.x + boundsTranslation.x, bounds.center.y + boundsTranslation.y, bounds.center.z);
		Bounds newBounds = new Bounds (newCenter, bounds.size);

		return CollidableObject.GetCollidingObjects (newBounds);
	}

	public static float Approach(float current, float target, float rate)
	{
		if (Mathf.Abs (current - target) < 0.1f)
			return target;
		float time = Time.deltaTime;
		float steps = time * 60; //  scale for a base rate of 60fps
		return (current - target) * Mathf.Pow (0.5f, rate * steps) + target;
	}

	//  Just something to make the state machine functions cleaner
	protected void ApplyGravity() {
		if (velocity.y > -10)
			velocity.y -= gravity * Time.deltaTime;
		if (velocity.y < -10)
			velocity.y = -10;
	}

	//private static List<CharacterCore> chars = new List<CharacterCore>();

	public static CharacterCore[] GetCollidingCharacters(Bounds bounds) 
	{
		List<CharacterProfile> profs = GameManagement.GetCharacters();
		List<CharacterCore> chars = new List<CharacterCore>();

		foreach (CharacterProfile prof in profs)
		{
			if (prof.owner != null)
				chars.Add (prof.owner.GetComponent<CharacterCore> ());
		}
		if (chars.Count == 0)
			return null;

		List<CharacterCore> returnList = new List<CharacterCore> ();

		foreach (CharacterCore ch in chars) {
			if (ch != null && ch.GetComponent<Collider2D> ().bounds.Intersects (bounds)) {
				returnList.Add (ch);
			}
		}

		if (returnList.Count == 0)
			return null;

		return returnList.ToArray ();
	}
}
