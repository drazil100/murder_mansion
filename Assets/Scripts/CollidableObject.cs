﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (Collider2D))]
public class CollidableObject : MonoBehaviour {

	private static List<CollidableObject> objects = new List<CollidableObject>();

	public CollidableObject() {
		objects.Add (this);
	}

	~CollidableObject() {
		objects.Remove (this);
	}

	public static CollidableObject[] GetAll()
	{
		return objects.ToArray();
	}

	//  Use to get an array of objects with this script attatched that intersect the bounds passed.
	public static Transform[] GetCollidingObjects(Bounds bounds) {
		if (objects.Count == 0)
			return null;
		
		List<Transform> returnList = new List<Transform> ();

		foreach (CollidableObject obj in objects) {
			if (obj != null && obj.GetComponent<Collider2D> ().bounds.Intersects (bounds)) {
				returnList.Add (obj.transform);
			}
		}

		if (returnList.Count == 0)
			return null;

		return returnList.ToArray ();
	}
}
