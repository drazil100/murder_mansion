﻿using UnityEngine;
using System.Collections;

public class PauseForConversation : Branch<CharacterData> {

	public override BranchStatus Process (CharacterData context)
	{
		
		if (context.isInConversation)
		{
			context.animation = "Idle";
			return BranchStatus.Running;
		}

		return BranchStatus.Failed;
	}
}
