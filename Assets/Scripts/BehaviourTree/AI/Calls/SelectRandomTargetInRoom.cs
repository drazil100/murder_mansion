﻿using UnityEngine;
using System.Collections;

public class SelectRandomTargetInRoom : Call
{
	private string room = "";
	public SelectRandomTargetInRoom() 
	{
		
	}

	public SelectRandomTargetInRoom(string room)
	{
		this.room = room;
	}

	protected override void DoCall (CharacterData context)
	{
		string newRoom = room;
		if (room == "") newRoom = context.Room;
		context.SetRandomTargetInRoom (newRoom);
	}
}
