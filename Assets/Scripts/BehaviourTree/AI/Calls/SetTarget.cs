﻿using UnityEngine;
using System.Collections;

public class SetTarget : Call{

	float position = 0;
	string room = "";

	public SetTarget(float position)
	{
		this.position = position;
	}

	public SetTarget(float position, string room)
	{
		this.room = room;

		this.position = position;
	}

	public SetTarget(Vector2 position)
	{
		this.position = position.x;
	}

	public SetTarget(Vector2 position, string room)
	{
		this.room = room;
		this.position = position.x;
	}

	public SetTarget(Vector3 position)
	{
		this.position = position.x;
	}

	public SetTarget(Vector3 position, string room)
	{
		this.room = room;
		this.position = position.x;
	}

	protected override void DoCall (CharacterData context)
	{
		context.SetTarget (position, room);
	}
}
