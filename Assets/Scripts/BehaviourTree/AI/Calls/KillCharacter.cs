﻿using UnityEngine;
using System.Collections;

public class KillCharacter : Call {

	protected override void DoCall (CharacterData context)
	{
		context.isAlive = false;
		GameManagement.RemoveOnDeath (context.Profile);

		if (context.character != null)
		{
			if(context.Profile.owner != null)
				context.Profile.owner.GetComponent<Speaker>().StopPlaying();
			context.character.transform.Rotate (0, 0, 90);
		}
	}
}
