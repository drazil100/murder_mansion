﻿using UnityEngine;
using System.Collections;

public class RequestToFollow : Call {

	private Message m;
	private CharacterData c;

	public RequestToFollow(CharacterData requester, Message message)
	{
		m = message;
		c = requester;
	}

	protected override void DoCall (CharacterData context)
	{
		if (context.Type == CharacterType.Inspector || context.Memory [c].Trust > 0)
		{
			m.AddResponse (new Message (context, GameManagement.GetMessage (context.Type, "follow_response_yes")));
			c.Profile.AddCall (new FollowMe (context));
			c.Memory [context].Trust += 5;
		}
		else
		{
			m.AddResponse (new Message (context, GameManagement.GetMessage (context.Type, "follow_response_no")));
			c.Memory [context].Trust -= 30;
			c.InterestInFollowing -= 50;
			c.InterestInRoom = Mathf.Clamp(c.InterestInRoom - 60, 0, 100);
		}
	}
}
