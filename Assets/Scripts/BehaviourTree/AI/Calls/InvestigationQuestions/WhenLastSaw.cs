﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WhenLastSaw : Call {
	private List<CharacterProfile> charactersToAsk;
	//private Message message;
	private CharacterData target;

	private WhenLastSaw(){
	}
	public WhenLastSaw(CharacterData context)
	{
		charactersToAsk = new List<CharacterProfile> (CharacterProfile.GetCharactersInRoom (context.Room));
		charactersToAsk.Remove (context.player.Profile);
		target = context.charactersToInvestigate [0];
		//message = new Message (context, "Who was the last person to see " + target.Profile.Name + "?");
	}

	protected override void DoCall (CharacterData context)
	{
		charactersToAsk.Remove (context.Profile);

	}

}
