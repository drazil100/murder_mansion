﻿using UnityEngine;
using System.Collections;

public class DoInvestigation : Call {

	protected override void DoCall (CharacterData context)
	{
		context.investigateDeath = true;
	}
}
