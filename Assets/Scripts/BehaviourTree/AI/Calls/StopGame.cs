﻿using UnityEngine;
using System.Collections;

public class StopGame : Call {

	protected override void DoCall (CharacterData context)
	{
		context.gameStarted = false;
	}
}
