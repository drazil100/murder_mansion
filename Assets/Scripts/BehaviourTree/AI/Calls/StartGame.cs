﻿using UnityEngine;
using System.Collections;

public class StartGame : Call {

	protected override void DoCall (CharacterData context)
	{
		context.gameStarted = true;
	}
}
