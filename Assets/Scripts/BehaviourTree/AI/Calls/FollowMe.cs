﻿using UnityEngine;
using System.Collections;

public class FollowMe : Call {

	private CharacterData target;
	private FollowState state;

	private FollowMe() {}
	public FollowMe(CharacterData target, FollowState state = FollowState.Follow)
	{
		this.target = target;
		this.state = state;
	}

	protected override void DoCall (CharacterData context)
	{
		bool roomConnects = false;

		foreach (string r in GameManagement.Manager.connectedRooms[context.Room])
		{
			if (r == target.Room)
			{
				roomConnects = true;
			}
		}

		if (roomConnects || context.Room == target.Room)
		{
			context.InterestInRoom = context.ROOM_INTEREST_MAX;
			context.InterestInFollowing = context.FOLLOW_INTEREST_MAX;
			context.targetCharacter = target;
			context.isFollowing = true;
			context.hasNewTarget = true;
			context.followState = state;
		}
	}
}
