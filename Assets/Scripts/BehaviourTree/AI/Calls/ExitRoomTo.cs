﻿using UnityEngine;
using System.Collections;

public class ExitRoomTo : Call {

	private CharacterData character;
	private string room;
	private float pos;

	private ExitRoomTo(){}
	public ExitRoomTo(CharacterData context, string room, float oldPosition)
	{
		this.room = room;
		character = context;
		pos = oldPosition;
	}

	protected override void DoCall (CharacterData context)
	{
		if ((pos > context.position.x && context.dir > 0) || (pos < context.position.x && context.dir < 0) || (Random.Range(0, 4) == 0))
		{
			//Debug.Log ("Observed exiting");
			context.Memory [character].lastSeenIn = room;
			context.Memory [character].lastSeen = Time.time;
		}
	}
}
