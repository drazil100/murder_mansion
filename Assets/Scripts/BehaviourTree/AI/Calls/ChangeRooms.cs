﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChangeRooms : Call{

	public ChangeRooms() {}

	protected override void DoCall (CharacterData context)
	{

		List<string> options = context.roomsVisited;
		options.Remove (context.Room);

		if (options.Count == 0)
		{
			Debug.Log ("Could not find any doors");
			return;
		}

		int item = Random.Range(Random.Range(0, options.Count-1), options.Count);

		context.SetRandomTargetInRoom (options [item]);
	}
}