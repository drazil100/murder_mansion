﻿using UnityEngine;
using System.Collections;

public class EnterRoom : Call {

	private CharacterData character;
	private EnterRoom(){}
	public EnterRoom(CharacterData context)
	{
		character = context;
	}

	protected override void DoCall (CharacterData context)
	{
		if ((character.position.x > context.position.x && context.dir > 0) || (character.position.x < context.position.x && context.dir < 0) || (Random.Range(0, 4) == 0))
		{
			context.Memory [character].lastSeenIn = context.Room;
			context.Memory [character].lastSeen = Time.time;
		}
	}
}
