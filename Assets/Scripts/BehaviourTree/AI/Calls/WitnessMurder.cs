﻿using UnityEngine;
using System.Collections;

public class WitnessMurder : Call {

	CharacterData c;

	public WitnessMurder(CharacterData character)
	{
		c = character;
	}

	protected override void DoCall (CharacterData context)
	{
		context.Memory [c].witnessedKill = true;
		context.Memory [c].Trust = -100;
		context.animation = "Idle";
		GameManagement.Manager.gameOver = true;
	}
}
