﻿using UnityEngine;
using System.Collections;

public class EvaluateInterestInRoom : Branch<CharacterData> {

	public override BranchStatus Process (CharacterData context)
	{
		if (context.investigateDeath) Debug.Log (context.ToString());
		

		//context.interestInRoom = CharacterCore.Approach (context.interestInRoom, 0, Random.Range(context.roomInterestChangeRate/10.0f, context.roomInterestChangeRate));
		if (context.InterestInRoom == 0)
		{
			if (context.Type == CharacterType.Inspector) context.InterestInFollowing = 100;
			//Debug.Log (string.Format ("follow: {0}, room{1}", context.interestInFollowing, context.interestInRoom));
			return BranchStatus.Completed;
		}

		return BranchStatus.Failed;
	}
}
