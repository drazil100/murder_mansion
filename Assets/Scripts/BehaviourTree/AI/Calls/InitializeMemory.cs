﻿using UnityEngine;
using System.Collections;

public class InitializeMemory : Call {

	protected override void DoCall (CharacterData context)
	{
		foreach (CharacterProfile profile in GameManagement.GetCharacters())
		{
			if (context != profile.Context)
			{
				context.Memory.Add (profile.Context);
			}
		}
	}
}
