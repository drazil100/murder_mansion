﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WhatHappened : Call {

	private CharacterData sender;
	private WhatHappened(){
	}
	public WhatHappened(CharacterData context)
	{
		sender = context;
	}

	protected override void DoCall (CharacterData context)
	{
		if (sender.Memory [context.inspector].isAlive && !context.Memory[context.inspector].isAlive)
		{
			sender.Memory [context.inspector].isAlive = false;
		}

		if (!sender.investigationStarted)
		{
			sender.investigationStarted = true;
		}
	}
}
