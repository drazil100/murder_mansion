﻿using UnityEngine;
using System.Collections;

public class SwitchFollowState : Call {

	protected override void DoCall (CharacterData context)
	{
		//context.interestInFollowing = CharacterCore.Approach (context.interestInFollowing, 0, Random.Range(context.FOLLOW_INTEREST_CHANGE_RATE/10.0f, context.FOLLOW_INTEREST_CHANGE_RATE));
		
		if (context.followState == FollowState.Follow &&  context.InterestInFollowing == 0)
		{
			//context.interestInRoom = context.ROOM_INTEREST_MAX;
			context.followState = FollowState.Wander;
			context.SetRandomTargetInRoom (context.Room);

			Message theMessage = new Message (context, GameManagement.GetMessage (context.Type, "wander"));
			GameManagement.AddMessage (theMessage);
			context.targetCharacter.Profile.AddCall (new ImGoingToWander (theMessage));

			//Debug.Log (string.Format ("follow: {0}, room{1}", context.interestInFollowing, context.interestInRoom));
		}
		else
		{
			context.followState = FollowState.Follow;
		}
		//Debug.Log (string.Format ("{0} switched to {1}", context.index, context.followState));


	}
}
