﻿using UnityEngine;
using System.Collections;

public class StopFollowing : Call {

	private bool doMessage = false;

	public StopFollowing()
	{
	}

	public StopFollowing(bool showMessage)
	{
		doMessage = showMessage;
	}

	protected override void DoCall (CharacterData context)
	{
		if (context.targetCharacter == null || !context.isFollowing)
			return; //Debug.Log (string.Format ("{0} is no longer following {1}", context.index, context.targetCharacter.index));


		if (doMessage)
		{
			//Debug.Log ("Called");
			GameManagement.AddMessage (new Message (context, GameManagement.GetMessage (context.Type, "unfollow")));
			context.dir = context.GetDirectionOf (context.targetCharacter);
		}
		context.isFollowing = false;
		//context.hasNewTarget = false;
	}
}
