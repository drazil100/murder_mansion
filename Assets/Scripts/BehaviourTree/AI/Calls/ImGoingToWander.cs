﻿using UnityEngine;
using System.Collections;

public class ImGoingToWander : Call {

	Message m;

	public ImGoingToWander(Message message)
	{
		m = message;
	}

	protected override void DoCall (CharacterData context)
	{
		context.InterestInRoom = Random.Range (Mathf.Clamp (context.InterestInRoom, 25, context.ROOM_INTEREST_MAX), context.ROOM_INTEREST_MAX + 1);
		m.AddResponse (new Message (context, GameManagement.GetMessage(context.Type,"wander_response")));
	}
}
