﻿using UnityEngine;
using System.Collections;

public class InvestigationComplete : Call {

	protected override void DoCall (CharacterData context)
	{
		Debug.Log ("Reached..." + context.index);
		context.investigateDeath = false;
		context.foundBody = false;
		context.investigationStarted = false;
		context.numberOfAnswers = 0;
		context.reporter = null;
	}
}
