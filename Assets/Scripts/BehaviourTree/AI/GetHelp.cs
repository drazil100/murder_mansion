﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GetHelp : SearchForCharacter {

	private bool started = false;
	private List<CharacterData> characters = new List<CharacterData> ();
	//private List<string> rooms = new List<string> ();
	public GetHelp() : base()
	{
	}

	public override BranchStatus Process (CharacterData context)
	{
		if (context.foundBody == false)
			return BranchStatus.Failed;

		if (!started)
		{
			if (!InitializeInvestigation (context))
				return BranchStatus.Failed;
		}

		if (base.Process (context) != BranchStatus.Completed)
		{
			return BranchStatus.Running;
		}

		//SendMessage (context);
		//if (context.inspector != null && context.inspector.TargetRoom != context.bodyRoom) Debug.Log ("Error");

		/*context.targetCharacter.Profile.AddCall (new SetTarget (context.bodyPosition, context.bodyRoom));
		context.targetCharacter.bodyRoom = context.bodyRoom;
		context.targetCharacter.bodyPosition = context.bodyPosition;
		context.targetCharacter.isFollowing = false;
		context.targetCharacter.investigateDeath = true;*/


		context.SetTarget (context.bodyPosition, context.bodyRoom);
		characters.Clear ();
		started = false;
		context.foundBody = false;
		context.investigationStarted = true;
		return BranchStatus.Completed;
	}

	protected override void OnRoomChange (CharacterData context)
	{
		CharacterProfile[] characters = CharacterProfile.GetCharactersInRoom (context.Room);
		if (characters.Length > 1)
		{
			bool check = false;
			foreach (CharacterProfile character in characters)
			{
				if (!this.characters.Contains (character.Context))
				{
					check = true;
				}
			}

			if (check)
				SendMessage (context);
		}
	}

	private bool InitializeInvestigation(CharacterData context)
	{
		Debug.Log ("GettingHelp");
		started = true;

		if (context.inspector != context && context.Memory [context.inspector].isAlive)
		{
			context.targetCharacter = context.inspector;
		}
		else
		{
			CharacterData t = context.Memory.GetMostTrustedCharacterNotInRoom ();
			if (t != null)
			{
				context.targetCharacter = t;
			}
			else
			{
				return false;
			}
		}

		SendMessage (context);
		return true;
	}

	private void SendMessage(CharacterData context)
	{
		context.isInConversation = true;
		GameManagement.ClearMessages (context.Room);
		string m = GameManagement.GetMessage (context.Type, "found_victim");
		GameManagement.AddMessage (new Message (context, m, context.Room));
		foreach (CharacterProfile p in CharacterProfile.GetCharactersInRoom(context.Room))
		{
			if (!this.characters.Contains (p.Context))
			{
				if (context != p.Context)
				{
					p.Context.Profile.AddCall (new SetTarget (context.bodyPosition, context.bodyRoom));
					p.Context.bodyRoom = context.bodyRoom;
					p.Context.bodyPosition = context.bodyPosition;
					p.Context.isFollowing = false;
					p.Context.investigateDeath = true;
					p.AddCall (new NullCall ());
				}
				this.characters.Add (p.Context);
			}

		}
		/*foreach (string room in GameManagement.Manager.connectedRooms[context.bodyRoom])
		{
			GameManagement.ClearMessages (room);
			GameManagement.AddMessage (new Message (context, m, room));
			foreach (CharacterProfile p in CharacterProfile.GetCharactersInRoom(room))
			{
				p.Context.isFollowing = false;
				p.AddCall (new SetTarget (context.bodyPosition, context.bodyRoom));
				p.Context.investigateDeath = true;
			}
		}*/
	}
}
