﻿using UnityEngine;
using System.Collections;

public class BodyWasFound : Branch<CharacterData> {

	public override BranchStatus Process (CharacterData context)
	{
		if (context.investigateDeath)
		{
			//Debug.Log ("Body was found: " + context.index);
			context.isFollowing = false;
			return BranchStatus.Completed;
		}

		return BranchStatus.Failed;
	}
}
