﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class MoveToTarget<T> : Branch<T>  where T : CharacterData {

	public MoveToTarget()
	{
		
	}

	public MoveToTarget(bool moveToRoomOnly)
	{
		this.moveToRoomOnly = moveToRoomOnly;
	}

	protected int doorWaitCounter = 40;
	protected int doorMaxWaitCounter = 40;
	protected bool moveToRoomOnly = false;

	protected List<DoorProfile> currentTargets = new List<DoorProfile> ();

	protected float FindShortestPath (DoorProfile[] doors,
										string[] roomsVisited,
									    string currentRoom, 
									    string targetRoom, 
									    float position, 
									    float target, 
									    float currentDistance, 
									    DoorProfile comingFrom,
										ref List<DoorProfile> l,
										float shortestDistance = -1)
	{
		//Debug.Log (string.Format ("currentRoom: {0}, targetRoom: {1}, currentDistance: {2}", currentRoom, targetRoom, currentDistance));
		if (shortestDistance != -1 && shortestDistance < currentDistance)
			return -1;
		int targetDoor = -1;
		float targetDistance = Mathf.Infinity;
		List<DoorProfile> shortestPathList = new List<DoorProfile>();
		List<string> newRooms = new List<string> (roomsVisited);

		for (int i = 0; i < doors.Length; i++)
		{
			List<DoorProfile> newList = new List<DoorProfile> ();
			List<DoorProfile> newDoors = new List<DoorProfile> ();

			newRooms.Add (currentRoom);
			for (int j = 0; j < doors.Length; j++)
			{
				if (j != i && !newRooms.Contains(doors[j].OtherRoom))
				{
					newDoors.Add (doors [j]);
				}
			}

			if (newDoors.Count == 0) return -1;

			if (currentRoom == targetRoom)
			{
				
				float dist = currentDistance + Mathf.Abs (position - target);
				//Debug.Log (dist);
				//Debug.Log ("" + shortestDistance + ", " + dist);
				if (shortestDistance == -1 || dist < shortestDistance)
					return dist;
				return -1;
			}

			if (doors [i].Room == currentRoom && doors [i] != comingFrom)
			{
				if (targetDoor == -1)
				{

					float newDistance = FindShortestPath (
						                    newDoors.ToArray (),
											newRooms.ToArray(),
						                    doors [i].OtherRoom, 
						                    targetRoom, doors [i].ExitPosition.x, 
						                    target, 
						                    currentDistance + Mathf.Abs (position - doors [i].Position.x), 
						                    doors [i].other, 
						                    ref newList,
											shortestDistance);
					
					if (newDistance != -1)
					{
						targetDoor = i;
						targetDistance = newDistance;
						shortestPathList = newList;
					}
				}
				else
				{
					float newDistance = FindShortestPath (
											newDoors.ToArray (),
											newRooms.ToArray(), 
						                    doors [i].OtherRoom, 
						                    targetRoom, 
						                    doors [i].ExitPosition.x, 
						                    target, 
						                    currentDistance + Mathf.Abs (position - doors [i].Position.x), 
						                    doors [i].other, 
						                    ref newList,
											targetDistance);
					
					if (newDistance != -1 && newDistance < targetDistance)
					{
						targetDistance = newDistance;
						targetDoor = i;
						shortestPathList = newList;
					}
				}
			}


			//FindShortestPath (newDoors, doors [i].otherRoom, targetRoom, doors [i].otherPosition.x, target, Mathf.Abs (position - doors [i].currentPosition.x));
		}
		if (targetDistance == Mathf.Infinity) return -1;
		shortestPathList.Insert (0, doors [targetDoor]);
		l = shortestPathList;
		return targetDistance;
	}

	public override void Reset()
	{
		base.Reset ();
		currentTargets.Clear ();
		doorWaitCounter = doorMaxWaitCounter;
	}

	public override BranchStatus Process(T context) {
		//context.called = false;

		if (!context.hasNewTarget) return BranchStatus.Failed;
		context.animation = "Walking";

		float currentTarget;
		string room = context.Room;


		if (room == context.TargetRoom)
		{
			currentTarget = context.Target;
		}
		else if (currentTargets != null && currentTargets.Count > 0)
		{
			currentTarget = currentTargets[0].Position.x;
			if (Mathf.Abs (context.position.x - currentTarget) < 0.2f)
			{
				context.animation = "Idle";
				if (doorWaitCounter == doorMaxWaitCounter)
				{
					currentTargets [0].OpenDoor ();
				}
				if (doorWaitCounter-- < 1)
				{
					currentTargets [0].CloseDoor ();
					doorWaitCounter = doorMaxWaitCounter;
					foreach (CharacterProfile profile in CharacterProfile.GetCharactersInRoom(context.Room))
					{
						if (profile.Context != context)
						{
							profile.AddCall (new ExitRoomTo (context, currentTargets [0].OtherRoom, context.position.x));
						}
					}
					context.position = new Vector2 (currentTargets [0].ExitPosition.x, currentTargets [0].ExitPosition.y + 0.5f);
					context.Room = currentTargets [0].OtherRoom;
					/*foreach (CharacterProfile profile in CharacterProfile.GetCharactersInRoom(context.Room))
				{
					if (profile.Context != context)
					{
						profile.AddCall (new EnterRoom (context));
					}
				}*/

					currentTargets.RemoveAt (0);
					//Debug.Log ("" + context.position + ", " + doors[targetDoor].otherPosition);
					if (context.character != null)
					{
						GameObject.Destroy (context.character.gameObject);
					}
				}
				return BranchStatus.Running;
			}
		}
		else
		{
			//Debug.Log (context.target);
			DoorProfile[] doors = GameManagement.Manager.doorProfiles.ToArray();

			int targetDoor = -1;
			float targetDistance = Mathf.Infinity;

			for (int i = 0; i < doors.Length; i++)
			{
				List<DoorProfile> newList = new List<DoorProfile> ();
				if (doors [i].Room == context.Room)
				{
					if (targetDoor == -1)
					{
						
						float newDistance = FindShortestPath (
							                    doors, 
												new string[] { context.Room },
							                    doors [i].OtherRoom, 
							                    context.TargetRoom, 
							                    doors [i].ExitPosition.x, 
							                    context.Target, 
							                    Mathf.Abs (context.position.x - doors [i].Position.x), 
							                    doors [i].other,
							                    ref newList);
						//Debug.Log (newDistance);
						if (newDistance != -1)
						{
							targetDoor = i;
							targetDistance = newDistance;
							currentTargets = newList;
						}
					}
					else
					{
						float newDistance = FindShortestPath (
												doors,  
												new string[] { context.Room },
							                    doors [i].OtherRoom, 
							                    context.TargetRoom, 
							                    doors [i].ExitPosition.x, 
							                    context.Target, 
							                    Mathf.Abs (context.position.x - doors [i].Position.x), 
							                    doors [i].other,
							                    ref newList,
												targetDistance);
						//Debug.Log (newDistance);
						if (newDistance != -1 && newDistance < targetDistance)
						{
							targetDistance = newDistance;
							targetDoor = i;
							currentTargets = newList;
						}
					}
				}
			}
			//Debug.Log (string.Format("--> {0}", targetDistance));

			if (targetDoor == -1)
			{
				//Debug.Log ("Called");
				currentTargets.Clear ();
				return BranchStatus.Failed;
			}
				
			currentTargets.Insert (0, doors [targetDoor]);

			currentTarget = doors [targetDoor].Position.x;
			if (Mathf.Abs (context.position.x - currentTarget) < 0.2f)
			{
				context.animation = "Idle";
				if (doorWaitCounter == doorMaxWaitCounter)
				{
					doors [0].OpenDoor ();
				}
				if (doorWaitCounter-- < 1)
				{
					doors [0].CloseDoor ();
					doorWaitCounter = doorMaxWaitCounter;
					foreach (CharacterProfile profile in CharacterProfile.GetCharactersInRoom(context.Room))
					{
						if (profile.Context != context)
						{
							profile.AddCall (new ExitRoomTo (context, doors [targetDoor].OtherRoom, context.position.x));
						}
					}
					context.position = new Vector2 (doors [targetDoor].ExitPosition.x, doors [targetDoor].ExitPosition.y + 0.5f);
					context.Room = doors [targetDoor].OtherRoom;
					/*foreach (CharacterProfile profile in CharacterProfile.GetCharactersInRoom(context.Room))
				{
					if (profile.Context != context)
					{
						profile.AddCall (new EnterRoom (context));
					}
				}*/
					//Debug.Log ("" + context.position + ", " + doors[targetDoor].otherPosition);
					currentTargets.RemoveAt (0);
					if (context.character != null)
					{
						GameObject.Destroy (context.character.gameObject);
					}
				}
				return BranchStatus.Running;
			}
		}



		context.dir = Mathf.Sign (currentTarget - context.position.x);
		context.velocity = CharacterCore.Approach (context.velocity, context.dir * context.speed, 0.3f);

		//Debug.Log (currentTarget);

		Move (context.velocity * Time.deltaTime, context);

		if (context.character != null) {
			//Debug.Log ("Called");
			context.character.transform.position = new Vector3(context.position.x, context.character.transform.position.y, context.character.transform.position.z);
		}

		if (Mathf.Abs (context.position.x - context.Target) > 0.2f || context.Room != context.TargetRoom) {
			if (context.Room == context.TargetRoom && moveToRoomOnly) return BranchStatus.Completed;
			return BranchStatus.Running;
		}

		context.hasNewTarget = false;
		return BranchStatus.Completed;
	}

	public void Move(float xInput, T context) 
	{

		//int x = 1;
		//int y = 1;

		float newXInput = CharacterCore.RoundCorrect (Mathf.Lerp (0, xInput, 1), 5);

		/*
		if (yInput != 0) IsGrounded = false;
		if (BoundsCast (new Vector2 (0, yInput)) != null)
		{
			if (yInput < 0)
				//IsGrounded = true;
			y = 0;
			//Debug.Log ("y collision");
			float i = 0.0f;
			float newYInput = 0.0f;
			while (BoundsCast (new Vector2 (0, CharacterCore.RoundCorrect(Mathf.Lerp (0, yInput, i), 5))) == null)
			{
				newYInput = CharacterCore.RoundCorrect(Mathf.Lerp (0, yInput, i), 5);
				i += 0.01f;
				if (i == 1)
					break;
			}
			yInput = newYInput;
		}

		//transform.Translate (0, yInput, 0);
		*/

		context.position.x = context.position.x + newXInput;

		//return new Vector2 (x, y);
	}
}
