﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SearchForCharacter : MoveToTarget<CharacterData> {
	private bool started = false;
	private string currentRoom;
	private List<string> roomsToVisit = new List<string> ();
	//private List<string> roomsVisited = new List<string> ();
	// Use this for initialization
	public SearchForCharacter() : base(true)
	{
	}

	public override BranchStatus Process (CharacterData context)
	{
		if (context.targetCharacter == null)
			return BranchStatus.Failed;

		if (!started)
			InitializeSearch (context);



		if (base.Process (context) == BranchStatus.Completed)
		{
			context.SetRandomTargetInRoom (roomsToVisit[roomsToVisit.Count-1]);
			roomsToVisit.Remove (roomsToVisit[roomsToVisit.Count-1]);
		}

		if (currentRoom != context.Room)
		{
			if (roomsToVisit.Contains (context.Room))
				roomsToVisit.Remove (context.Room);
			currentRoom = context.Room;
			if (roomsToVisit.Count == 0)
				InitializeSearch (context);
			OnRoomChange (context);
		}

		if (context.targetCharacter.Room != context.Room)
		{
			return BranchStatus.Running;
		}

		started = false;
		return BranchStatus.Completed;
	}

	protected virtual void OnRoomChange(CharacterData context)
	{
	}

	private void InitializeSearch(CharacterData context)
	{
		//Debug.Log ("Called");
		context.isFollowing = false;
		started = true;
		roomsToVisit.Clear ();
		//List<string> tmp = new List<string> ();
		currentRoom = context.Room;
		foreach (string room in context.roomsVisited)
		{
			roomsToVisit.Add (room);
		}


		if (context.Type != CharacterType.Inspector && context.Memory [context.targetCharacter].isAlive)
		{
			string lastSeen = context.Memory [context.targetCharacter].lastSeenIn;
			if (lastSeen != "")
			{
				context.SetRandomTargetInRoom (lastSeen);
				roomsToVisit.Remove (lastSeen);
			}
			else
			{
				context.SetRandomTargetInRoom (roomsToVisit[roomsToVisit.Count-1]);
				roomsToVisit.Remove (roomsToVisit[roomsToVisit.Count-1]);
			}
		}
		else
		{
			context.SetRandomTargetInRoom (roomsToVisit[roomsToVisit.Count-1]);
			roomsToVisit.Remove (roomsToVisit[roomsToVisit.Count-1]);
		}
	}
}
