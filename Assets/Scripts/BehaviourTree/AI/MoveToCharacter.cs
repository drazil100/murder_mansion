﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveToCharacter : MoveToTarget<CharacterData> {

	private string lastSeen = "";
	private float target;
	private float lastPosition;
	private bool isCompleted = false;
	private int goingTooSlowCounter = 0;

	public MoveToCharacter() {}
	public MoveToCharacter(Branch<CharacterData> followBranch, Branch<CharacterData> wanderBranch)
	{
		children = new Branch<CharacterData>[] { followBranch, wanderBranch };
	}

	public override BranchStatus Process (CharacterData context)
	{
		doorMaxWaitCounter = 0;
		doorWaitCounter = 0;

		if (!context.isFollowing)
		{
			lastSeen = context.Room;
			return BranchStatus.Failed;
		}

		BranchStatus status;
		if (!isCompleted)
		{
			status = base.Process (context);
		}
		else
		{
			status = BranchStatus.Running;
		}


		if (status == BranchStatus.Completed || isCompleted)
		{
			context.animation = "Idle";
		}


		if (context.targetCharacter.Room != context.TargetRoom && context.TargetRoom == context.Room)
		{
			if (lastSeen == context.Room)
			{
				base.Reset ();
				context.followState = FollowState.Follow;
				isCompleted = false;
				context.SetTarget (context.targetCharacter.position.x, context.targetCharacter.Room, false);

			}

		}
		else if (context.targetCharacter.Room == context.Room)
		{
			lastSeen = context.Room;
			int dir = 0;
			float targetPos = context.targetCharacter.position.x;
			float pos = context.position.x;

			if (targetPos < pos) dir = -1;
			if (targetPos >= pos) dir = 1;
			if (context.followState == FollowState.Follow) context.dir = dir;

			CharacterProfile[] characters = CharacterProfile.GetCharactersInRoom (context.Room);


			for (int i = 0; i < characters.Length; i++)
			{
				for (int j = characters.Length - 1; j > i; j--)
				{
					if (characters [i].Context.position.x > characters [j].Context.position.x)
					{
						CharacterProfile temp = characters [i];
						characters [i] = characters [j];
						characters [j] = temp;
					}
				}
			}

			float newTarg = targetPos;
			int scatterMultiplier = 1;

			int index = -1;
			for (int i = 0; i < characters.Length; i++)
			{
				if (index != -1 && characters [i].Context.targetCharacter == context.targetCharacter && characters [i].Context.isFollowing)
				{
					characters [i].Context.followOrder = scatterMultiplier++;
				}
				if (characters [i].Context == context.targetCharacter)
				{
					index = i;
				}
			}
			scatterMultiplier = 1;
			for (int i = index; i >= 0; i--)
			{
				if (i == index) continue;
				if (characters [i].Context.targetCharacter == context.targetCharacter && characters [i].Context.isFollowing)
				if (index != -1 && characters [i].Context.targetCharacter == context.targetCharacter && characters [i].Context.isFollowing)
				{
					characters [i].Context.followOrder = scatterMultiplier++;
				}

			}
			bool outOfLongRange = (Mathf.Abs (context.targetCharacter.position.x - context.position.x) > 6f);
			bool targetReturnedToRoom = (context.TargetRoom != context.Room && context.targetCharacter.Room == context.Room);
			bool isInFollowMode = context.followState == FollowState.Follow;
			bool targetHasMoved = Mathf.Abs (context.targetCharacter.position.x - lastPosition) > 0.5f;

			//Debug.Log (string.Format("outOfLongRange: {0}, targetReturnedToRoom: {1}, isInFollowMode: {2}, targetHasMoved {3}", outOfLongRange, targetReturnedToRoom, isInFollowMode, targetHasMoved));

			if (((targetHasMoved || outOfLongRange) && isInFollowMode && goingTooSlowCounter-- < 1) || targetReturnedToRoom)
			{
				
				isCompleted = false;

				/*foreach (CharacterProfile character in characters)
				{
					if (character.Context != context.targetCharacter && character.Context != context && character.Context.isFollowing && character.Context.targetCharacter == context.targetCharacter)
					{
						float p = character.Context.position.x;
						if (dir == -1)
						{
							if (p >= targetPos && character.Context.speed > context.speed)
							{
								scatterMultiplier++;
							}
						}
						else
						{
							if (p <= targetPos && character.Context.speed > context.speed)
							{
								scatterMultiplier++;
							}
						}
					}
				}*/

				newTarg = newTarg + (((CharacterData.scatterDistance * context.followOrder * 1.5f) + 0.01f) * (-1 * dir));

				if ((dir == -1 && newTarg < pos) || (dir == 1 && newTarg > pos))
				{
					if (Mathf.Abs (newTarg - context.position.x) > 1)
					{
						target = newTarg;
						context.SetTarget (target, context.Room, false);
					}
					else
					{
						goingTooSlowCounter = 20;
					}
				}
				lastPosition = context.targetCharacter.position.x;
			}
			else
			{
				if ((isCompleted || status == BranchStatus.Completed) && children != null)
				{
					if (!isCompleted)
					{
						//Debug.Log ("reset");
						children [0].Reset ();
					}
					isCompleted = true;
					if (context.followState == FollowState.Follow)
					{
						if (children.Length > 0)
						{
							children [0].Process (context);
						}
					}
					else
					{
						if (children.Length > 1)
						{
							children [1].Process (context);
						}
					}


				}
				/*
				if (status == BranchStatus.Completed && children != null)
				{
					Debug.Log ("called");
					children [0].Process (context);
				}
				*/
			}
		}


		if (status == BranchStatus.Completed && lastSeen != context.Room)
		{
			//Debug.Log (string.Format("{0} lost sight of {1}", context.index, context.targetCharacter.index));
			context.Memory [context.targetCharacter].Trust = context.Memory [context.targetCharacter].Trust - 10;
			GameManagement.AddMessage(new Message(context, GameManagement.GetMessage(context.Type, "lost_sight")));

			context.isFollowing = false;
			return BranchStatus.Failed;
		}

		if (context.isFollowing)
			return BranchStatus.Running;




		//Debug.Log (status);



		return status;

	}
}
