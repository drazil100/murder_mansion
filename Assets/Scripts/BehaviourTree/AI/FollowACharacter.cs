﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowACharacter : Branch<CharacterData> {

	public override BranchStatus Process (CharacterData context)
	{
		if (GameManagement.CoversationHappening (context.Room))
			return BranchStatus.Failed;

		//context.interestInFollowing = CharacterCore.Approach (context.interestInFollowing, context.FOLLOW_INTEREST_MAX, Random.Range(context.FOLLOW_INTEREST_CHANGE_RATE / 10.0f, context.FOLLOW_INTEREST_CHANGE_RATE));

		if (context.InterestInFollowing != context.FOLLOW_INTEREST_MAX)
		{
			return BranchStatus.Failed;
		}

		List<CharacterData> characters1 = context.Memory.GetMostTrustedCharactersInRoom ();
		List<CharacterData> characters = new List<CharacterData> ();

		foreach (CharacterData character in characters1)
		{
			//if (character.Type != CharacterType.Player) continue;
			if (character.isAlive && context.Memory[character].Trust > 20 && (character.targetCharacter == null || NotFollowingMe(context, character)))
			{
				characters.Add (character);
			}
		}

		if (characters.Count == 0)
		{
			//Debug.Log ("Failed");
			return BranchStatus.Failed;
		}

		//context.targetCharacter = characters [Random.Range (0, characters.Count)];
		//context.isFollowing = true;
		//context.followState = FollowState.Follow;

		int random = 0;//Random.Range (0, 2);
		if (random == 0)
		{
			string m = GameManagement.GetMessage (context.Type, "follow_request"); //string.Format ("{0} is now following {1}", context.index, context.targetCharacter.index);
			Message theMessage;
			CharacterData c = characters [Random.Range (0, characters.Count)];
			if (c.Type == CharacterType.Player)
			{
				AnswerList answers = new AnswerList ();
				answers.Add ("Yes", new Message(context, "Thanks"), delegate(AnswerList a)
					{
						//Debug.Log("Called");
						context.Memory[c].Trust += 5;
						context.Profile.AddCall (new FollowMe (c));
					});
				answers.Add ("No", new Message (context, "Ok..."), delegate (AnswerList a) 
					{
						context.Memory[c].Trust -= 30;
						context.InterestInFollowing -= 50;
						context.InterestInRoom = Mathf.Clamp(context.InterestInRoom - 60, 0, 100);
					});
				/*
				answers.Add ("test", new Message (context, "Ok..."));
				answers.Add ("test", new Message (context, "Ok..."));
				answers.Add ("test", new Message (context, "Ok..."));
				answers.Add ("test", new Message (context, "Ok..."));
				answers.Add ("test", new Message (context, "Ok..."));
				answers.Add ("test", new Message (context, "Ok..."));
				answers.Add ("test", new Message (context, "Ok..."));
				answers.Add ("test", new Message (context, "Ok..."));
				answers.Add ("test", new Message (context, "Ok..."));
				answers.Add ("test", new Message (context, "Ok..."));
				answers.Add ("test", new Message (context, "Ok..."));
				answers.Add ("test", new Message (context, "Ok..."));
				*/

				theMessage = new Message (context, m, answers);
				GameManagement.AddMessage (theMessage);
			}
			else
			{
				theMessage = new Message (context, m);
				GameManagement.AddMessage (theMessage);
				c.Profile.AddCall (new RequestToFollow (context, theMessage));
			}
			GameManagement.AddMessage (theMessage);
			context.dir = context.GetDirectionOf (c);
			context.targetCharacterWasRequested = true;
		}
		else
		{
			context.targetCharacter = characters [Random.Range (0, characters.Count)];
			context.followState = (Random.Range(0,2) == 0) ? FollowState.Wander : FollowState.Follow;
			context.isFollowing = true;
			context.targetCharacterWasRequested = false;
			context.InterestInRoom = context.ROOM_INTEREST_MAX;
		}

		//Debug.Log (string.Format ("follow: {0}, room{1}", context.interestInFollowing, context.interestInRoom));

		return BranchStatus.Completed;
	}

	private bool NotFollowingMe(CharacterData self, CharacterData other)
	{
		if (!(other.isFollowing && other.targetCharacter == self))
		{
			if (other.isFollowing)
			{
				return NotFollowingMe (self, other.targetCharacter);
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}
}
