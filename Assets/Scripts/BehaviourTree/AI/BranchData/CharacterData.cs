﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum FollowState
{
	Follow,
	Wander
}

public class CharacterData {

	public static float scatterDistance = 0.6f;
	public static int frameScale = 1000;

	public int frameCounter = 0;

	public CharacterType Type { get; private set; }
	public CharacterProfile Profile { get; private set; }
	public bool gameStarted = false;
	public bool isAlive = true;
	public float speed = 2.0f;
	public Vector2 position = new Vector2(0, 2.5f);
	public CharacterCore character;
	private float target = 0;
	private string targetRoom = "";
	public float velocity = 2;
	public float dir = 1;
	private string room = "";
	public Bounds bounds;
	public string animation = "Idle";
	public bool hasNewTarget = false;
	public bool isFollowing = false;
	public CharacterData targetCharacter;
	private bool doScatter = true;
	public int index = 0;
	public int followOrder = 0;
	public int chatOrder = 0;
	public int numberOfAnswers = 0;
	public FollowState followState = FollowState.Wander;
	public MemoryBank Memory { get; private set; }
	public List<string> roomsVisited = new List<string>();
	public bool isInConversation = false;
	public bool targetCharacterWasRequested = false;
	public bool investigateDeath = false;
	public bool investigationStarted = false;
	public bool foundBody = false;
	public float bodyPosition = 0;
	public string bodyRoom = "";
	public CharacterData player;
	public CharacterData inspector;
	public CharacterData reporter;
	private List<Message> messageList = new List<Message>();
	public List<CharacterData> charactersToInvestigate = new List<CharacterData> ();

	//========================= Interests ===================================
	private int interestInRoom = 0;
	public int InterestInRoom { get { return interestInRoom; } set { interestInRoom = Mathf.Clamp (value, 0, 100); } }

	private int interestInFollowing = 0;
	public int InterestInFollowing { get { return interestInFollowing; } set { interestInFollowing = Mathf.Clamp (value, 0, 100); } }

	private int interestInCharacter = 0;
	public int InterestInCharacter { get { return interestInCharacter; } set { interestInCharacter = Mathf.Clamp (value, 0, 100); } }

	public bool characterQuestion = false;

	//private int roomInterestDecreaseRate = 0;
	//private int followInterestDecreaseRate = 0;
	//private int followInterestIncreaseRate = 0;

	public int ROOM_INTEREST_MAX = 100;
	public int FOLLOW_INTEREST_MAX = 100;
    public int QUESTION_INTEREST_MAX = 100;

	//public List<CharacterData> followingMe = new List<CharacterData>();

	public CharacterData(CharacterProfile profile)
	{
		this.Type = profile.Type;
		this.Profile = profile;
		Memory = new MemoryBank(this);
		speed = Random.Range (2.0f, 3.0f);

		roomsVisited = new List<string> (GameManagement.Manager.rooms);

		for (int i = 0; i < roomsVisited.Count; i++)
		{
			int j = Random.Range (0, roomsVisited.Count);

			string tmp = roomsVisited [i];
			roomsVisited [i] = roomsVisited [j];
			roomsVisited [j] = tmp;
		}

		//roomInterestDecreaseRate = Random.Range (10, 50);
		//followInterestIncreaseRate = Random.Range (5, 15);
		//followInterestDecreaseRate = Random.Range (10, 30);

		interestInRoom = Random.Range (0, 25);
		interestInFollowing = Random.Range (40, FOLLOW_INTEREST_MAX);
		interestInCharacter = Random.Range (30, 70);
		frameCounter = Random.Range (0, frameScale);
		//followingMe.Add (this);

		if (Type == CharacterType.Player)
		{
			//string m = "(I can't let the inspector figure out I did it.)";
			//GameManagement.AddMessage (new Message (this, m, "Foyer"));
		}
	}

	public void UpdateDecisionVariables()
	{
		if (isInConversation) return;
		if (isFollowing)
			frameCounter += targetCharacter.FollowMeCount - 1;
		if (Mathf.Clamp(++frameCounter, 0, frameScale) % frameScale == 0)
		{
			frameCounter = 0;

			if (isFollowing)
			{
				interestInFollowing -= Random.Range (0, 70);
				interestInRoom -= Random.Range (0, 35);
			}
			else
			{
				interestInFollowing += Random.Range (0, 40);
				interestInRoom -= Random.Range (0, 50);
			}
			interestInFollowing = Mathf.Clamp (interestInFollowing, 0, FOLLOW_INTEREST_MAX);

			interestInCharacter = Mathf.Clamp (interestInCharacter + Random.Range (0, 40), 0, QUESTION_INTEREST_MAX);
			if (interestInCharacter == QUESTION_INTEREST_MAX && !characterQuestion)
			{
				List<CharacterProfile> chars = new List<CharacterProfile>(GameManagement.GetCharacters (true));
				for (int i = chars.Count-1; i >= 0; i--)
				{
					if (!Memory.ContainsKey(chars [i].Context) && chars[i].Context != this)
					{
						chars.Clear ();
						break;
					}
					if (!Memory.ContainsKey(chars [i].Context) || (!Memory [chars [i].Context].isAlive || chars [i].Type != CharacterType.Guest || chars [i].Context == this))
					{
						chars.RemoveAt (i);
					}
				}
				if (chars.Count > 0)
				{
					messageList.Add (WhatDoYouThinkOf (chars [Random.Range (0, chars.Count)].Context));
					characterQuestion = true;
				}
			}

			interestInRoom = Mathf.Clamp (interestInRoom, 0, ROOM_INTEREST_MAX);
			if (Type == CharacterType.Inspector)
			{
				Memory [GameManagement.GetCharacters () [0].Context].Trust -= 5;
				if (Memory [GameManagement.GetCharacters () [0].Context].Trust == -100)
				{
					Debug.Log ("GameOver");
					GameManagement.Manager.gameOver = true;
				}
				if (CharacterProfile.GetCharactersInRoom (Room).Length < 3)
				{
					interestInRoom = 0;
				}
			}
		}
			
	}

	public float Target
	{
		get { return target; }
	}
	public string TargetRoom
	{
		get { return targetRoom; }
	}

	public string Room 
	{
		get { return room; }
		set 
		{ 
			if (room != "")
			{
				if (!isFollowing)
				{
					interestInRoom = ROOM_INTEREST_MAX;
				}
				else
				{
					interestInFollowing = Random.Range (interestInFollowing, FOLLOW_INTEREST_MAX + 1);
					interestInRoom = Random.Range (interestInRoom, ROOM_INTEREST_MAX + 1);
				}
				room = value;
			}
			else
			{
				room = value;
				bodyRoom = room;
			}
			OnRoomChange ();
		}
	}

	public int FollowMeCount
	{
		get {
			int c = 0;
			//if (isFollowing)
				//c++;
			foreach (CharacterProfile p in GameManagement.GetCharacters ())
			{
				if (p.Context.targetCharacter == this && p.Context.isFollowing)
				{
					c += 1 + p.Context.FollowMeCount;
				}
			}
			return c;
		}
	}

	public Texture2D BubbleTexture { get { return Profile.bubble; } set { Profile.bubble = value; } }

	public Vector3 Position { get { return new Vector3 (position.x, position.y); } }

	public List<Call> calls = new List<Call> ();

	public Message GetMessage()
	{
        CharacterProfile[] p = GameManagement.GetCharacters(true).ToArray();
        if (inspector == null)
		{
			
			player = p [0].Context;
			inspector = p [1].Context;
		}

		string m = GameManagement.GetMessage (Type, "interact"); //string.Format ("{0} is now following {1}", context.index, context.targetCharacter.index);
		Message message;

		AnswerList answers = new AnswerList ();

        
        
		//Ask to meet somewhere
		AnswerList al = new AnswerList ();
		al = new AnswerList();
		AnswerList ansl = new AnswerList();
		for(int i=0;i<CharacterProfile.Characters.Length;i++)
		{
            if (i != 1)
            {
                if (Profile.Name != CharacterProfile.Characters[i].Name && CharacterProfile.Characters[i].Context.isAlive)
                {
					if (Memory[p[i].Context].Trust > 60)
					{
						if(i==0)
						{
							ansl.Add("Me", new Message(this, "Of course!"), delegate (AnswerList a)
								{
									//What happens when you ask about what a character thinks about another
								});
						}
						else
						{
							ansl.Add(CharacterProfile.Characters[i].Name, new Message(this, "I trust them."), delegate (AnswerList a)
								{
									//What happens when you ask about what a character thinks about another
								}, CharacterProfile.Characters[i].lightColor);
						}
					}
                    else if (Memory[p[i].Context].Trust > 20)
                    {
                        if(i==0)
                        {
                            ansl.Add("Me", new Message(this, "I trust you."), delegate (AnswerList a)
                            {
                                //What happens when you ask about what a character thinks about another
                            });
                        }
                        else
                        {
                            ansl.Add(CharacterProfile.Characters[i].Name, new Message(this, "I trust them."), delegate (AnswerList a)
                            {
                                //What happens when you ask about what a character thinks about another
                            }, CharacterProfile.Characters[i].lightColor);
                        }
                    }
                    else if (Memory[p[i].Context].Trust < -60)
                    {
                        if (i == 0)
                        {
                            ansl.Add("Me", new Message(this, "I don't trust you."), delegate (AnswerList a)
                            {
                                //What happens when you ask about what a character thinks about another
                            });
                        }
                        else
                        {
                            ansl.Add(CharacterProfile.Characters[i].Name, new Message(this, "I don't trust them."), delegate (AnswerList a)
                            {
                            //What happens when you ask about what a character thinks about another
                        	}, CharacterProfile.Characters[i].lightColor);
                        }
                    }
                    else
                    {
						if (i == 0)
						{
							ansl.Add ("Me", new Message (this, "I don't know..."), delegate (AnswerList a)
								{
									//What happens when you ask about what a character thinks about another
								});
						}
						else
						{
							if (!Memory [player].knownOpinions.Contains (CharacterProfile.Characters [i].Context))
							{
								ansl.Add (CharacterProfile.Characters [i].Name, WhatDoYouThinkOf(CharacterProfile.Characters [i].Context, "I don't know. What do you think?"), delegate (AnswerList a)
									{
									}, CharacterProfile.Characters [i].lightColor);
							}
							else
							{
								ansl.Add (CharacterProfile.Characters [i].Name, new Message (this, "I'm not sure."), delegate (AnswerList a)
									{
										//What happens when you ask about what a character thinks about another
									}, CharacterProfile.Characters [i].lightColor);
							}
						}
                    }
                }
            }    
		}
		ansl.Add("Nevermind.", new Message(this, "Ok."));
		//ansl.Add("Nevermind", new Message(this, "Sure thing."));
		CharacterProfile greatest = Memory.GetLeastTrustedCharacter ().Profile;

        al.Add("Who do you think is the killer?", new Message(this,"I bet it's "+greatest.Name+"."));
		greatest = Memory.GetMostTrustedCharacter ().Profile;

        al.Add("Who do you think we can trust?", new Message(this,"I think "+greatest.Name+" is trustworthy."));
		al.Add("Do you trust...?", new Message(this, "Who?",ansl));
		al.Add("Nevermind.", new Message(this, "Ok."));
		if (messageList.Count > 0)
		{
			message = messageList [0];
			messageList.RemoveAt (0);
		}
		else
		{
			message = new Message (this, "What would you like to talk about?", al);
		}
		answers.Add("Lets talk.", message);
		al = new AnswerList ();
		for (int i = 0; i < GameManagement.Manager.rooms.Length; i++)
		{
			al.Add (GameManagement.Manager.rooms [i], new Message (this, "OK. See you there."), MeetUpRequest);
			//Debug.Log(GameManagement.Manager.rooms[i]);
		}
        al.Add("Nevermind", new Message(this, "Ok."));
		message = new Message (this, "Where?", al);
		answers.Add ("Can we talk in another room?", message);
        //Increase suspicion
        al =new AnswerList();
        for (int i = 2; i < CharacterProfile.Characters.Length; i++)
        {
			CharacterData c = CharacterProfile.Characters [i].Context;
            if (Profile.Name != CharacterProfile.Characters[i].Name)
            {
                al.Add(CharacterProfile.Characters[i].Name, new Message(this, "Hmmm."), delegate (AnswerList a)
                {
					if (Memory[c].Trust > 20)
					{
						Memory[player].Trust -= 15;
					}
					else if (Memory[c].Trust < 20)
					{
						Memory[player].Trust += 15;
						Memory[c].Trust -= 10;
					}
					}, CharacterProfile.Characters[i].lightColor);
            }
        }
        al.Add("No, nevermind.", new Message(this, "If you're sure..."));
        answers.Add("I don't really trust...", new Message(this, "Who?", al));

        //answers.Add ("Yes", new Message(this, "Thanks"), FollowRequest);
        answers.Add("It's nothing.", new Message(this, "Ok."), delegate (AnswerList a)
          {
              //Memory[player].Trust -= 30;
          });
        /*
			answers.Add ("test", new Message (context, "Ok..."));
			answers.Add ("test", new Message (context, "Ok..."));
			answers.Add ("test", new Message (context, "Ok..."));
			answers.Add ("test", new Message (context, "Ok..."));
			answers.Add ("test", new Message (context, "Ok..."));
			answers.Add ("test", new Message (context, "Ok..."));
			answers.Add ("test", new Message (context, "Ok..."));
			answers.Add ("test", new Message (context, "Ok..."));
			answers.Add ("test", new Message (context, "Ok..."));
			answers.Add ("test", new Message (context, "Ok..."));
			answers.Add ("test", new Message (context, "Ok..."));
			answers.Add ("test", new Message (context, "Ok..."));
			*/

        message = new Message (this, m, answers);

		return message;
	}

	public Message WhatDoYouThinkOf(CharacterData character, string msg = "")
	{
		AnswerList al = new AnswerList ();
		al.Add("I trust them.", new Message(this, "Sure thing."), delegate {
			if (!Memory[player].knownOpinions.Contains(character))
				Memory [player].knownOpinions.Add(character);
			
			if (Memory[character].Trust > 20)
			{
				Memory[player].Trust += 15;
				Memory[character].Trust += 10;
			}
			else if (Memory[character].Trust < 20)
			{
				Memory[player].Trust -= 20;
			}
		});
		al.Add("I don't trust them.", new Message(this, "Sure thing."), delegate {
			if (!Memory[player].knownOpinions.Contains(character))
				Memory [player].knownOpinions.Add(character);
			
			if (Memory[character].Trust > 20)
			{
				Memory[player].Trust -= 15;
			}
			else if (Memory[character].Trust < 20)
			{
				Memory[player].Trust += 15;
				Memory[character].Trust -= 10;
			}
		});
		al.Add("I don't really know.", new Message(this, "Hmmm..."));

		if (msg == "")
			msg = string.Format ("What do you think about {0}?", character.Profile.Name);
		Message tmp = new Message (this, msg, al);
		tmp.AddActionToLastMessage (delegate(CharacterData context) {
			interestInCharacter = 0;
		});
		return tmp;
	}

	public void FollowRequest(AnswerList a)
	{
		if (Type == CharacterType.Inspector || Memory [player].Trust > 50)
		{
			new Message (this, GameManagement.GetMessage (Type, "follow_response_yes"));
			Profile.AddCall (new FollowMe (player));
			Memory [player].Trust += 5;
		}
		Profile.AddCall (new FollowMe (player));
	}

    public void MeetUpRequest(AnswerList a)
    {
		interestInFollowing = 0;
		isFollowing = false;
		investigateDeath = false;
		investigationStarted = false;
		foundBody = false;
        Profile.AddCall(new SelectRandomTargetInRoom(a.answers[a.index].Text));
        //Debug.Log(a.answers[a.index].Text);
    }

	public void SetTarget(float newTarget, bool doScatter = true)
	{
		//Debug.Log (index);
		hasNewTarget = true;
		target = newTarget;
		this.doScatter = doScatter;
		if (room == targetRoom && doScatter)
		{
			target = ScatterTarget (target, targetRoom);
		}
	}

	public void SetTarget(float newTarget, string newRoom, bool doScatter = true)
	{
		if (newRoom != "" && newRoom != targetRoom) 
			targetRoom = newRoom;
		SetTarget (newTarget, doScatter);
	}

	private void OnRoomChange()
	{
		for (int i = 0; i < roomsVisited.Count; i++)
		{
			if (roomsVisited[i] == room)
			{
				roomsVisited.RemoveAt(i);
			}
		}
		roomsVisited.Insert (0, room);


		if (room == targetRoom && doScatter)
		{
			target = ScatterTarget (target, targetRoom);
		}
	}

	public float ScatterTarget(float newTarget, string targetRoom)
	{
		CharacterProfile[] characters = CharacterProfile.GetCharactersGoingToRoom (targetRoom);
		/*if (characters.Length > 1)
		{
			for (int i = 0; i < characters.Length; i++)
			{
				for (int j = characters.Length; j > i; j--)
				{
					if (Mathf.Abs (characters [j].Context.target - position.x) < Mathf.Abs (characters [i].Context.target - position.x))
					{
						CharacterProfile temp = characters [i];
						characters [i] = characters [j];
						characters [j] = temp;
					}
				}
			}
		}*/
		float sDist = scatterDistance;
		float mRange = 3f;
		if (WallPair.IsValid (targetRoom, newTarget, bounds))
		{
			while (true)
			{
				float randomDistance = Random.Range (0, 0.4f);


				bool pComplete = false;
				bool mComplete = false;
				float pTarget = 0;
				float mTarget = 0;
				while (WallPair.IsValid (targetRoom, newTarget + randomDistance, bounds) || WallPair.IsValid (targetRoom, newTarget - randomDistance, bounds))
				{
					bool mSafe = true;
					bool pSafe = true;

					foreach (CharacterProfile c in characters)
					{
						if (c.Type == CharacterType.Player || c.Context == this)
							continue;

						if ((Mathf.Abs (c.Context.target - (newTarget + randomDistance)) < sDist && c.Context.Room == targetRoom) || !WallPair.IsValid (targetRoom, newTarget + randomDistance, bounds))
						{
							pSafe = false;
						}
						if ((Mathf.Abs (c.Context.target - (newTarget - randomDistance)) < sDist && c.Context.Room == targetRoom) || !WallPair.IsValid (targetRoom, newTarget - randomDistance, bounds))
						{
							mSafe = false;
						}
					}

					if (pSafe && !pComplete)
					{
						pTarget = (newTarget + randomDistance);
						pComplete = true;
					}
					if (mSafe && !mComplete)
					{
						mTarget = (newTarget - randomDistance);
						mComplete = true;
					}
					if (pComplete && mComplete)
					{
						//Debug.Log(string.Format("{0}=({2}, {3}), {1}=({4}, {3})", Mathf.Abs (pTarget - position.x), Mathf.Abs (mTarget - position.x), pTarget, position.x, mTarget));
						return (Mathf.Abs (position.x - pTarget) < Mathf.Abs (position.x - mTarget)) ? pTarget : mTarget;
					}

					randomDistance += Random.Range (0.0f, mRange);
				}
				if (pComplete) return pTarget;
				if (mComplete) return mTarget;

				mRange /= 2.0f;
				sDist /= 2.0f;
			}
		}
		return WallPair.GetCenter (targetRoom);

		//return newTarget;
	}

	public void SetRandomTargetInRoom (string room = "")
	{
		if (room == "") room = this.room;
		//if (context.called) Debug.Log ("whyyyyyy?");
		WallPair pair = GameManagement.Manager.sceneBounds[room];

		float point = UnityEngine.Random.Range (pair.leftWall.center.x, pair.rightWall.center.x);

		while (!WallPair.IsValid(room, point, bounds))
		{
			point = UnityEngine.Random.Range (pair.leftWall.center.x, pair.rightWall.center.x);
		}
			
		SetTarget (point, room);
	}

	public int GetDirectionOf(CharacterData other)
	{
		int d = 0;

		if (other.position.x < position.x)
		{
			d = -1;
		}
		else if (other.position.x > position.x)
		{
			d = 1;
		}
		return d;
	}

	public override string ToString ()
	{
		return string.Format ("[CharacterData: Index={0}, IsFollowing={1}", index, isFollowing);
	}

}
