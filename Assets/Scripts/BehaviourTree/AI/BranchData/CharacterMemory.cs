﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterMemory {

	public CharacterData Character { get; private set; }
	public CharacterData Owner { get { return this.bank.Owner; } }
	private MemoryBank bank;
	public List<CharacterData> knownOpinions = new List<CharacterData> ();

	public string lastSeenIn = "";
	public float lastSeen;
	private float trust = 0;
	public bool witnessedKill = false;
	public bool isAlive = true;
	public CharacterData reporter;
	private bool debug = false;

	public bool qLastSeen = false;
	public bool qLastSeenWith = false;

	public float Trust { get { return trust; } set {  if (debug) Debug.Log (string.Format ("{0}: {1}'s trust was changed {2}", Owner.Profile.Name, Character.Profile.Name, value - trust)); trust = Mathf.Clamp (value, -100, 100); } }


	private CharacterMemory(){}
	public CharacterMemory(CharacterData character, MemoryBank bank)
	{
		if (character.Memory.ContainsKey (bank.Owner))
		{
			trust = character.Memory [bank.Owner].trust;
			if (character.Type == CharacterType.Player && bank.Owner.Type == CharacterType.Inspector)
			{
				trust = 0;
			}
			else if (character.Type == CharacterType.Player)
			{
				trust = Random.Range (-50, 25);
			}
			else if (character.Type == CharacterType.Inspector)
			{
				trust = Random.Range (25, 50);
			}
			else
			{
				trust = Random.Range (trust - 25, trust + 25);
			}
		}
		else
		{
			if (character.Type == CharacterType.Player && bank.Owner.Type == CharacterType.Inspector)
			{
				trust = 0;
			}
			else if (character.Type == CharacterType.Player)
			{
				trust = Random.Range (-50, 10);
			}
			else if (character.Type == CharacterType.Inspector)
			{
				trust = Random.Range (25, 50);
			}
			else
			{
				trust = Random.Range (-50, 50);
			}
		}
		Character = character;
		lastSeenIn = Character.Room;
		lastSeen = Time.time;
		this.bank = bank;
	}



	public List<CharacterData> LastSeenWith()
	{
		List<CharacterData> toReturn = new List<CharacterData> ();
		foreach (KeyValuePair<CharacterData, CharacterMemory> memory in bank)
		{
			if (memory.Key != Character && lastSeenIn == memory.Value.lastSeenIn)
			{
				toReturn.Add (memory.Key);
			}
		}

		return toReturn;
	}


}



public class MemoryBank : Dictionary<CharacterData, CharacterMemory>
{
	public CharacterData Owner { get; private set; }
	private static Dictionary<CharacterData, MemoryBank> banks = new Dictionary<CharacterData, MemoryBank> (); 

	private MemoryBank():base(){}
	private MemoryBank(int capacity):base(capacity){}
	public MemoryBank(CharacterData character) : this()
	{
		Owner = character;
		banks.Add (Owner, this);
	}

	~MemoryBank()
	{
		banks.Remove(Owner);
	}



	public void Add(CharacterData character)
	{
		Add (character, new CharacterMemory (character, this));
	}

	public List<CharacterData> GetMostTrustedCharactersInRoom()
	{
		List<CharacterData> toReturn = new List<CharacterData> ();
		float trust = Mathf.NegativeInfinity;
		foreach (CharacterMemory memory in Values)
		{
			if (Owner.Room == memory.Character.Room && memory.Character.isAlive)
			{
				// remove later...
				//toReturn.Add (memory.Character);
				//continue;
				if (memory.Trust > trust)
				{
					toReturn.Clear ();
					toReturn.Add (memory.Character);
					trust = memory.Trust;
				}
				else if (memory.Trust == trust)
				{
					toReturn.Add (memory.Character);
				}
			}
		}
		return toReturn;
	}

	public CharacterData GetMostTrustedCharacter()
	{
		CharacterData toReturn = null;
		float trust = Mathf.NegativeInfinity;
		foreach (CharacterMemory memory in Values)
		{
			if (memory.isAlive)
			{
				// remove later...
				//toReturn.Add (memory.Character);
				//continue;
				if (memory.Trust > trust)
				{
					toReturn = memory.Character;
					trust = memory.Trust;
				}
			}
		}
		return toReturn;
	}

	public CharacterData GetLeastTrustedCharacter()
	{
		CharacterData toReturn = null;
		float trust = Mathf.Infinity;
		foreach (CharacterMemory memory in Values)
		{
			if (memory.isAlive)
			{
				// remove later...
				//toReturn.Add (memory.Character);
				//continue;
				if (memory.Trust < trust)
				{
					toReturn = memory.Character;
					trust = memory.Trust;
				}
			}
		}
		return toReturn;
	}

	public CharacterData GetMostTrustedCharacterInRoom()
	{
		CharacterData toReturn = null;
		float trust = Mathf.NegativeInfinity;
		foreach (CharacterMemory memory in Values)
		{
			if (Owner.Room == memory.Character.Room && memory.Character.isAlive)
			{
				// remove later...
				//toReturn.Add (memory.Character);
				//continue;
				if (memory.Trust > trust)
				{
					toReturn = memory.Character;
					trust = memory.Trust;
				}
			}
		}
		return toReturn;
	}

	public CharacterData GetLeastTrustedCharacterInRoom()
	{
		CharacterData toReturn = null;
		float trust = Mathf.Infinity;
		foreach (CharacterMemory memory in Values)
		{
			if (Owner.Room == memory.Character.Room && memory.Character.isAlive)
			{
				// remove later...
				//toReturn.Add (memory.Character);
				//continue;
				if (memory.Trust < trust)
				{
					toReturn = memory.Character;
					trust = memory.Trust;
				}
			}
		}
		return toReturn;
	}

	public CharacterData GetMostTrustedCharacterNotInRoom()
	{
		CharacterData toReturn = null;
		float trust = Mathf.NegativeInfinity;
		foreach (CharacterMemory memory in Values)
		{
			if (Owner.Room != memory.Character.Room && memory.isAlive)
			{
				// remove later...
				//toReturn.Add (memory.Character);
				//continue;
				if (memory.Trust > trust)
				{
					toReturn = memory.Character;
					trust = memory.Trust;
				}
			}
		}
		return toReturn;
	}

	public int CharactersInRoom(string room)
	{
		int count = 0;
		foreach (CharacterMemory memory in Values)
		{
			if (memory.lastSeenIn == room)
			{
				count++;
			}
		}
		return count;
	}

	public static CharacterData FindLastWitnessOf(CharacterData character)
	{
		CharacterData o = null;
		foreach (MemoryBank b in banks.Values)
		{
			if (b.Owner != character && b.Owner.isAlive)
			{
				if (o == null)
				{
					o = b.Owner;;
					continue;
				}

				if (b [character].lastSeen < o.Memory [character].lastSeen)
				{
					o = b.Owner;
				}
			}
		}

		if (o != null)
		{
			return o;
		}

		return null;
	}

	//public static CharacterData[] FindLastCharactersSeenWith(


}
