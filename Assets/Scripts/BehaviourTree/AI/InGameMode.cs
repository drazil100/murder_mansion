﻿using UnityEngine;
using System.Collections;

public class InGameMode : Branch<CharacterData> {

	public InGameMode(Branch<CharacterData> notInGameMode, Branch<CharacterData> inGameMode)
	{
		children = new Branch<CharacterData>[2] { notInGameMode, inGameMode };
	}

	public override BranchStatus Process (CharacterData context)
	{
		if (!context.gameStarted)
		{
			if (children [0] != null)
			{
				return children [0].Process (context);
			}
		}
		else
		{
			if (children [1] != null)
			{
				return children [1].Process (context);
			}
		}

		return BranchStatus.Failed;
	}
}
