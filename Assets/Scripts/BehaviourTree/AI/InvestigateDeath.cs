﻿using UnityEngine;
using System.Collections;

public class InvestigateDeath : Branch<CharacterData>{
	private bool started = false;


	//private WhatHappened call;
	private CharacterProfile target;
	public override BranchStatus Process (CharacterData context)
	{
		context.animation = "Idle";
		if (!started)
			Initialize (context);

		if (!GameManagement.CoversationHappening (context.Room))
		{
			if (target == null)
			{
				CharacterData c = context.Memory.GetMostTrustedCharacterInRoom ();
				if (c!= null)
					target = c.Profile;
			}
			else
			{
				//target.AddCall (call);
			}
		}


		if (context.InterestInRoom > 0)
		{
			
			return BranchStatus.Running;
		}

		context.charactersToInvestigate.Clear ();
		started = false;
		return BranchStatus.Completed;
	}

	private void Initialize(CharacterData context)
	{
		started = true;
		if (!context.investigationStarted)
		{
			context.InterestInRoom = Random.Range(10, 25);
			//call = new WhatHappened (context);
			context.investigationStarted = true;
		}
	}
}
