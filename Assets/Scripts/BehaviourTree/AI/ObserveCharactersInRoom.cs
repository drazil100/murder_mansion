﻿using UnityEngine;
using System.Collections;

public class ObserveCharactersInRoom : Branch<CharacterData> {

	private ObserveCharactersInRoom(){}
	public ObserveCharactersInRoom(Branch<CharacterData> branch)
	{
		children = new Branch<CharacterData>[1] { branch };
	}

	public override BranchStatus Process (CharacterData context)
	{
		 bool found = false;
		foreach (CharacterProfile profile in CharacterProfile.GetCharactersInRoom(context.Room, true))
		{
				
			if (context.isAlive && !profile.Context.isAlive && Mathf.Sign(profile.Context.position.x - context.position.x) == context.dir)
			{
				
				if (context.Memory [profile.Context].isAlive)
				{
					context.Memory [profile.Context].isAlive = false;

					if (found) continue;
					found = true;
					if (!GameManagement.Manager.deadCharacters.Contains (profile))
					{
						GameManagement.Manager.deadCharacters.Add (profile);
						context.foundBody = true;
						context.Memory [profile.Context].reporter = context;
						context.reporter = context;
					}
					context.bodyPosition = profile.Context.position.x;
					context.bodyRoom = context.Room;
					context.investigateDeath = true;
					if (context.reporter != null)
						context.Memory [profile.Context].reporter = context.reporter;
					context.charactersToInvestigate.Add (profile.Context);
					//context.Profile.AddCall (new NullCall ());
					//Debug.Log ("called");
				}
			}

		}
		foreach (CharacterProfile profile in CharacterProfile.GetCharactersInRoom(context.Room))
		{
			CharacterData character = profile.Context;
			if (((character.position.x > context.position.x && context.dir > 0) || (character.position.x < context.position.x && context.dir < 0)) && !context.investigateDeath)
			{
				//Debug.Log ("Observed");
				context.Memory [character].lastSeenIn = context.Room;
				context.Memory [character].lastSeen = Time.time;
			}
		}
		foreach (CharacterProfile profile in CharacterProfile.GetCharactersNotInRoom(context.Room))
		{
			CharacterData character = profile.Context;
			if (context.Memory [character].lastSeenIn == context.Room && !context.investigateDeath)
			{
				//Debug.Log ("Observed");
				context.Memory [character].lastSeenIn = "";
			}
		}

		if (children != null && children [0] != null)
		{
			if (found)
				children [0].Reset ();
			children [0].Process (context);
		}
			

		return BranchStatus.Completed;
	}
}
