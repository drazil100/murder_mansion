﻿using UnityEngine;
using System.Collections;

public class Idle<T> : Branch<T>  where T : CharacterData {

    private int count;
	private int dir;
	private int changeDir;
	private int min = 60;
	private int max = 1200;

	public Idle()
	{

	}

	public Idle(int min, int max)
	{
		this.min = min;
		this.max = max;
		Reset ();
	}

	public override void Reset ()
	{
		base.Reset ();
		count = Random.Range(min, max);
		changeDir = Random.Range(120, 240);
		dir = Random.Range (-1, 1);
	}



	public override BranchStatus Process(T context)
	{
		context.animation = "Idle";

		if (changeDir-- < 1)
		{
			changeDir = Random.Range(120, 240);
			dir = Random.Range (-1, 2);
		}

		context.dir = dir;

		if (context.Room == context.TargetRoom && Mathf.Abs (context.position.x - context.Target) < 0.2f)
		{
            if (count != 0)
            {
                --count;
                return BranchStatus.Running;
            }
            else
            {
                return BranchStatus.Completed;
            }
		}

		return BranchStatus.Completed;
	}
}
