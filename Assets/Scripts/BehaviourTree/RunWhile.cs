﻿using UnityEngine;
using System.Collections;

public class RunWhile<T> : Branch<T> {

	private int index = 0;

	public override void Reset ()
	{
		base.Reset ();
		index = 0;
	}

	public RunWhile(Branch<T> condition, Branch<T> whileTrueBranch, Branch<T> whileFalseBranch = null)
	{
		children = new Branch<T>[3] { condition, whileTrueBranch, whileFalseBranch };
	}

	public override BranchStatus Process(T context)
	{
		BranchStatus status = children [0].Process (context);

		if (status == BranchStatus.Running)
		{
			if (index != 0) children [0].Reset ();
			index = 0;
			return BranchStatus.Running;
		}
		else if (status == BranchStatus.Completed)
		{
			if (index != 1) children [1].Reset ();
			index = 1;
			return children [1].Process (context);
		}
		else
		{
			if (index != 2) children [2].Reset ();
			index = 2;
			return children [2].Process (context);
		}
		//return BranchStatus.Running;
	}
}
