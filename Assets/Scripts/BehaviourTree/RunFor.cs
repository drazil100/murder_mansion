﻿using UnityEngine;
using System.Collections;

public class RunFor<T> : Branch<T> {

	private int counter;
	private int total;

	private RunFor() {}
	public RunFor (int frames, Branch<T> branch)
	{
		counter = frames;
		total = frames;
		children = new Branch<T>[1] { branch };
	}

	public override void Reset ()
	{
		base.Reset ();
		counter = total;
	}

	public override BranchStatus Process (T context)
	{
		counter--;
		BranchStatus status = children [0].Process (context);

		if (counter < 0)
		{
			Reset ();
			return BranchStatus.Completed;
		}
		else if (status == BranchStatus.Failed)
		{
			return status;
		}

		return BranchStatus.Running;
	}

}
