﻿using UnityEngine;
using System.Collections;

public class Call : Branch<CharacterData> {

	protected virtual void DoCall(CharacterData context)
	{
	}

	public override BranchStatus Process (CharacterData context)
	{
		DoCall (context);
		return BranchStatus.Completed;
	}
}
