﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum BranchStatus
{
	Completed,
	Running,
	Failed

}
	
abstract public class Branch<T>
{
	protected Branch<T>[] children;

	public void SetChildren(params Branch<T>[] branches)
	{
		children = branches;
	}

	// Use this for initialization
	public virtual void Reset () 
	{
		if (children != null)
		{
			foreach (Branch<T> b in children)
			{
				if (b != null)
				{
					b.Reset ();
				}
			}
		}
	}
	
	// Update is called once per frame
	public virtual BranchStatus Process (T context) 
	{
		if (children != null && children.Length > 0)
		{
			return children [0].Process (context);
		}
		else
		{
			return BranchStatus.Failed;
		}
	}
}
