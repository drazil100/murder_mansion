﻿using UnityEngine;
using System.Collections;

public class Succeeder<T> : Branch<T> {

	public Succeeder(Branch<T> branch)
	{
		children = new Branch<T>[1] { branch };
	}

	public override BranchStatus Process (T context)
	{
		BranchStatus status = children [0].Process (context);

		if (status == BranchStatus.Running)
		{
			return status;
		}

		children [0].Reset ();
		return BranchStatus.Completed;
	}
}
