﻿using UnityEngine;
using System.Collections;

abstract public class Interrupter<T> : Branch<T> {

	protected int index = 0;

	public override void Reset ()
	{
		base.Reset ();
		index = 0;
	}

	protected abstract bool Check (T context);

	public Interrupter(Branch<T> main, Branch<T> second)
	{
		children = new Branch<T>[2] { main, second };
	}

	public override BranchStatus Process (T context)
	{
		if (Check (context) && index == 0)
		{
			children[0].Reset();
			index = 1;
		}

		BranchStatus status = children [index].Process (context);

		if (status == BranchStatus.Completed)
		{
			Reset ();
			return BranchStatus.Completed;
		}

		return status;

	}
}
