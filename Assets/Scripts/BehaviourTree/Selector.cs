﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Selector<T> : Branch<T> 
{
	private int index = 0;

	public override void Reset ()
	{
		base.Reset ();
		index = 0;
	}

	public Selector(params Branch<T>[] branches)
	{
		children = branches;
	}

	public override BranchStatus Process(T context)
	{
		while (index < children.Length) {
			BranchStatus status = children [index].Process (context);
			if (status == BranchStatus.Running) {
				return BranchStatus.Running;
			} else if (status == BranchStatus.Completed) {
				Reset ();
				return BranchStatus.Completed;
			} else {
				index++;
			}
		}
		Reset ();
		return BranchStatus.Failed;
	}

}
