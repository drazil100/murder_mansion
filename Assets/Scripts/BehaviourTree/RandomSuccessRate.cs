﻿using UnityEngine;
using System.Collections;

public class RandomSuccessRate<T> : Branch<T> {

	private int success;
	private int total;

	public RandomSuccessRate(int successes, int outOf)
	{
		success = successes;
		total = outOf;
	}

	public override BranchStatus Process (T context)
	{
		int rand = Random.Range (0, total);

		if (rand < success)
			return BranchStatus.Completed;

		return BranchStatus.Failed;
	}
}
