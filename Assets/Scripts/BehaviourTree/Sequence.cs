﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sequence<T> : Branch<T> 
{
	private int index = 0;

	public override void Reset ()
	{
		base.Reset ();
		index = 0;
	}

	public Sequence(params Branch<T>[] branches)
	{
		children = branches;
	}

	public override BranchStatus Process(T context)
	{
		while (index < children.Length) {
			BranchStatus status = children [index].Process (context);
			if (status == BranchStatus.Running) {
				return BranchStatus.Running;
			} else if (status == BranchStatus.Failed) {
				Reset ();
				return BranchStatus.Failed;
			} else {
				index++;
			}
		}
		Reset ();
		return BranchStatus.Completed;
	}

}