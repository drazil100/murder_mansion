﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConditionalBranch<T> : Branch<T> {

	private int index = 0;

	public override void Reset ()
	{
		base.Reset ();
		index = 0;
	}

	public ConditionalBranch(Branch<T> test, Branch<T> completedBranch, Branch<T> failedBranch = null)
	{
		children = new Branch<T>[3] { test, completedBranch, failedBranch };
	}

	public override BranchStatus Process(T context)
	{
		BranchStatus status;
		if (children [index] != null) {
			status = children [index].Process (context);
		} else {
			return BranchStatus.Completed;
		}

		if (index == 0) {
			if (status == BranchStatus.Completed) {
				index = 1;
				children [index].Reset ();
			} else if (status == BranchStatus.Failed) {
				index = 2;
				children [index].Reset ();
			} else if (status == BranchStatus.Running) {
				return BranchStatus.Running;
			}
			return Process (context);
		} else {
			if (status == BranchStatus.Running) {
				return BranchStatus.Running;
			} else {
				Reset ();
				return status;
			}
		}
		//return BranchStatus.Running;
	}
}
