﻿using UnityEngine;
using System.Collections;

public class BranchInverter<T> : Branch<T> {

	public BranchInverter(Branch<T> child)
	{
		children = new Branch<T>[1] { child };
	}

	public override BranchStatus Process(T context)
	{
		BranchStatus status = children [0].Process (context);

		if (status == BranchStatus.Completed)
		{
			return BranchStatus.Failed;
		}
		else if (status == BranchStatus.Failed)
		{
			return BranchStatus.Completed;
		}

		return BranchStatus.Running;
	}
}
