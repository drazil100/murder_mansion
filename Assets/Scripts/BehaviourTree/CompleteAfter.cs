﻿using UnityEngine;
using System.Collections;

public class CompleteAfter<T> : Branch<T> {

	private int countStart;
	private int count;

	private CompleteAfter(){}
	public CompleteAfter(int frames)
	{
		count = frames;
		countStart = frames;
	}

	public override void Reset ()
	{
		base.Reset ();
		count = countStart;
	}

	public override BranchStatus Process (T context)
	{
		if (count-- <= 0)
		{
			return BranchStatus.Completed;
		}
		else
		{
			return BranchStatus.Running;
		}
	}
}
