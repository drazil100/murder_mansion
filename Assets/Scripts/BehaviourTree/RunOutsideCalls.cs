﻿using UnityEngine;
using System.Collections;

public class RunOutsideCalls<T> : Branch<T> where T : CharacterData {

	private bool completed = false;

	public RunOutsideCalls(Branch<T> branch)
	{
		children = new Branch<T>[1] { branch };
	}
	

	public override BranchStatus Process (T context)
	{
		
		BranchStatus status;

		while (context.calls.Count > 0)
		{
			completed = true;
			status = context.calls [0].Process (context);

			if (status == BranchStatus.Running) return status;

			context.calls.RemoveAt (0);
		}

		if (completed)
		{
			//Debug.Log ("Reset: " + context.index);
			children [0].Reset ();
			completed = false;
		}

		status = children [0].Process (context);

		return status;

	}

}
