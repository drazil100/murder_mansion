﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public delegate void AnswerFunction(AnswerList list);
public class Answer
{
	private AnswerFunction action;
	private string text = "Error: No text.";
	private Message message;
	public Color c = Color.white;
	private Answer(){}
	public string Text { get { return text; } }
	public AnswerFunction Action { get { return action; } } 
	public Message ResponseMessage { get { return message; } }
	public Answer(string text, Message responseMessage)
	{
		this.text = text;
		this.message = responseMessage;
	}
	public Answer(string text, Message responseMessage, AnswerFunction action) : this(text, responseMessage)
	{
		this.action = action;
	}
	public Answer(string text, Message responseMessage, AnswerFunction action, Color color) : this(text, responseMessage, action)
	{
		c = color;
	}
}

public class AnswerList {
	public List<Answer> answers = new List<Answer> ();
	public int index = 0;
	private Message owningMessage;

	private Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D> ();

	public void Add(string text, Message response, AnswerFunction action)
	{
		answers.Add (new Answer (text, response, action));
	}

	public void Add(string text, Message response, AnswerFunction action, Color color)
	{
		answers.Add (new Answer (text, response, action, color));
	}
	/*public void Add(string text, AnswerFunction action)
	{
		answers.Add (new Answer (text, action));
	}*/
	public void Add(string text, Message response)
	{
		answers.Add (new Answer (text, response));
	}

	public void SetOwningMessage(Message theMessage)
	{
		owningMessage = theMessage;
	}

	public int Count { get { return answers.Count; } }

	public void RemoveCurrent()
	{
		answers.RemoveAt (index);
	}

	public bool ShowList()
	{
		//TODO
		if (Input.GetButtonDown ("Up"))
		{
			index--;// = Mathf.Clamp (index - 1, 0, Count - 1);
			if (index < 0) index = Count - 1;
		}
		if (Input.GetButtonDown ("Down"))
		{
			index++;// = Mathf.Clamp (index + 1, 0, Count- 1);
			if (index >= Count) index = 0;
		}

		if (Input.GetButtonDown ("Interact"))
		{
			if (answers [index].Action != null)
			{
				answers [index].Action (this);
			}
			if (answers [index].ResponseMessage != null)
			{
				owningMessage.AddResponse (answers [index].ResponseMessage);
            }
            CloseGUI();
			return true;
		}

		return false;
	}

	private GUIStyle GetBackgroundStyle(Color color)
	{
		GUIStyle background = new GUIStyle ();
		background.stretchWidth = true;
		background.stretchHeight = true;
		if (!textures.ContainsKey (color.ToString()))
		{
			textures.Add (color.ToString(), new Texture2D (1, 1));
			textures[color.ToString()].SetPixel (0, 0, color);
			textures[color.ToString()].Apply ();
			//Debug.Log (color.ToString ());
		}

		background.normal.background = textures[color.ToString()];
		background.padding = new RectOffset(0, 0, 0, 0);
		background.margin = new RectOffset (0, 5, 0, 5);
		background.overflow = new RectOffset(0, 0, 0, 0);

		return background;
	}

    public void showGUI()
    {
		try
		{
			int maxW = 250;
			float w = 160, h = 200, scaledW = w, scalefactor = 1;

			if (h != Screen.height / 6)
			{
				scalefactor = (Screen.height / 4) / h;
				scaledW = w * scalefactor;
			}
			GUILayout.BeginArea (new Rect ((Screen.width - maxW - scaledW), 100 , maxW + scaledW, Screen.height - (Screen.height / 6) - 100));
			GUILayout.BeginVertical ();
			GUILayout.Space(Mathf.Clamp(Screen.height - (Screen.height / 6) - 100 - (35 * answers.Count), 0, Screen.height));
			//GUI.contentColor = Color.yellow;
			for (int i = 0; i < answers.Count; i++)
			{
				Color c = owningMessage.Sender.Profile.answerColor;//new Color(0.2f, 0.2f, 0.2f);
				//c = new Color(c.r, c.g, c.b, 0.95f);
				Color c2 = new Color(c.r, c.g, c.b, 0.75f);

				c = Color.Lerp(c2, c, Mathf.Lerp(0.25f,i/(float)(answers.Count-1),i/(float)(answers.Count-1)) );

				if (i == index)
					c = owningMessage.Sender.Profile.bubbleColor;//new Color(0.6f, 0.6f, 0.2f);
				GUILayout.BeginVertical (GetBackgroundStyle(c), GUILayout.MaxHeight(30));

				//GUIStyle style = new GUIStyle ();

				if (index != i)
					GUI.contentColor = answers[i].c;

				GUILayout.Label (answers [i].Text, GUILayout.MaxWidth(maxW - 5));

				GUI.contentColor = Color.white;

				GUILayout.EndVertical ();
			}
			GUILayout.EndVertical ();
			GUILayout.EndArea ();
		}
		catch (Exception) {
		}
    }

    public void CloseGUI()
    {

    }
}
