﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class SuspicionMeter : MonoBehaviour {

	public const float SUS_METER_LIMIT = 10000;
	public const float SUS_RATE_DEFAULT = 0.001f;
	public static float SusRate { get; private set;}
	public static float SusCurrent { get; private set;}
	public static float SusMomentum { get; private set;}
	public static float SusMeterPercent { get; private set;}

	public Sprite grey60;
	public Sprite yellow100;

	public GameObject MeterBar;
	public GameObject theBar;
	public float MeterPosX, MeterPosY;
	public float MeterW, MeterH;

	private bool inGame;
	private float theBarPosX;
	private bool SusFrozen;

	// Use this for initialization
	void Start ()
	{

		SusCurrent = 0;
		SusMomentum = 1;
		SusRate = -.2f;

		SusFrozen = false;

		MeterW = Screen.width / 3;
		MeterH = Screen.height / 20;

		MeterPosX = (Screen.width / 2) - MeterW - 15;
		MeterPosY = (Screen.height /2) - MeterH - 15;

		theBarPosX = 0;

		SusMeterPercent = 0;
	}

	void FixedUpdate () 
	{
		MeterW = Screen.width / 3;
		MeterH = Screen.height / 20;

        MeterPosX = (Screen.width / 2) - MeterW - 15;
        //MeterPosX = (Screen.width / 2)-40;
        MeterPosY = (Screen.height /2) - MeterH - 15;
        //MeterPosY = (Screen.height / 2)+40;
        DoMomentum ();

		if (SusCurrent >= SUS_METER_LIMIT) //suspicion level has maxed out
		{
			SusCurrent = SUS_METER_LIMIT;
			SusMomentum = 0;
			SusRate = 0;
		}
			
		SusMeterPercent = (SusCurrent / SUS_METER_LIMIT) * 100;

		if (theBar != null && MeterBar != null && inGame)	
		{
			theBarPosX = ((MeterW * (SusMeterPercent/100))/2)-(MeterW/2);	//position bar to looked fixed on right side
			if (SusMeterPercent / 100 > 0) 				//only display positive levels of suspicion, negative is hidden to prevent bar from moving off base/screen 
			{
				theBar.transform.localScale = new Vector3 ((SusMeterPercent / 100), 1, 1);
				theBar.transform.localPosition = new Vector3 (-(theBarPosX), 0);
			}
		}

		if (SusMeterPercent == 100)	// do this is suspicion is maxed (game over?)
		{
			inGame = false;
		}

	}

	void DoMomentum()			// organized momentum mechanics here. called with update, 60x per second
	{
		 
		if (!SusFrozen)
		{
			SusMomentum += SusRate;
			SusCurrent += SusMomentum;
		}

		if (SusRate < SUS_RATE_DEFAULT) // if rate is less than normal, slowly restore it to default rate
		{
			if (SusRate + 0.0015f > SUS_RATE_DEFAULT)
				SusRate = SUS_RATE_DEFAULT;
			else
				SusRate += .0015f;
		}

		//Debug.Log ("SM: "+SusMomentum);
		//Debug.Log ("SR: "+SusRate);
	}


	public void AffectMomentumRate(float newRate)
	{
		SusRate = newRate;
	}

	public void ResetMomentum()
	{
		SusMomentum = 1;
	}
		
	void SetSusCurrent(float newCurrent) //currently unused
	{
		SusCurrent = newCurrent;
	}

	public void NewGame()
	{
		SusCurrent = -500;				// give player a few seconds before suspicion starts to show on the bar
		SusMomentum = 1;
		SusRate = SUS_RATE_DEFAULT;

		theBar.transform.localScale = new Vector3 (0, 1, 1);
		theBar.transform.localPosition = new Vector3 ((150 - theBarPosX), 0);
	}
	public void FreezeMomentum()
	{
		SusFrozen = true;
	}
	public void UnfreezeMomentum()
	{
		SusFrozen = false;
	}
	public void DrawMeter()
	{
		//GameManagement.Manager.canvas;
		inGame = true;

		MeterBar = new GameObject("MeterBase");
		MeterBar.transform.SetParent(GameManagement.Manager.canvas.transform);
		MeterBar.transform.localPosition = new Vector3 (MeterPosX, MeterPosY);
		MeterBar.AddComponent<Image> ();
		MeterBar.GetComponent<Image> ().sprite = grey60;
		MeterBar.GetComponent<Image> ().rectTransform.sizeDelta = new Vector2 (MeterW,MeterH);
        MeterBar.GetComponent<Image>().rectTransform.localPosition= new Vector3(MeterPosX, MeterPosY);

        theBar = new GameObject("Bar");
		theBar.transform.SetParent(MeterBar.transform);
		theBar.transform.localPosition = new Vector3 (0, 0);
		theBar.AddComponent<Image> ();
		theBar.GetComponent<Image> ().sprite = yellow100;
		theBar.GetComponent<Image> ().rectTransform.sizeDelta = new Vector2 (MeterW-4, MeterH-4);

	}

}
