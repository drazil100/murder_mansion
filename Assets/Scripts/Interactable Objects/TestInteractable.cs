﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TestInteractable : InteractableObject {

	void Start() { Destroy (this.gameObject); }

	public override InteractionState OnInteractionBegin()
	{
		
		foreach (CharacterProfile character in GameManagement.GetCharacters())
		{
			if (character.Context != null && character.Type == CharacterType.Guest)
			{
				//string r = SceneManager.GetActiveScene ().name;

				character.AddCall(new FollowMe(GameManagement.GetCharacters()[0].Context));
				//character.AddCall (new SetTarget (transform.position, r));
			}
		}
		return InteractionState.Done;
	}

	public override InteractionState OnInteractionContinue()
	{
		return InteractionState.Done;
	}

}