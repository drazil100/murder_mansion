﻿using UnityEngine;
using System.Collections;

public class DoorSound : MonoBehaviour {

	public GameObject Speaker;
	public AudioClip doorOpeningSound;

	private AudioSource source;

	// Use this for initialization
	void Awake () {
		source = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	public void PlaySound () {
	
		source.PlayOneShot (doorOpeningSound, 1F);

	}
}
