﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DoorProfile
{
	private string currentRoom;       // Name of the room this door is found in
    private int doorNumber;           // Number of this door
	private Vector2 currentPosition;  // Position this door is located at
	public string otherRoom;
	public DoorProfile other;
	public bool isOpen = false;

	public string Room { get { return currentRoom; } }
	public int Number { get { return doorNumber; } }
	public Vector2 Position { get { return currentPosition; } }

	public string OtherRoom
	{
		get {
			return otherRoom;
		}
	}

	public Vector2 ExitPosition
	{
		get {
			if (other != null) 
				return other.Position;

			return Vector2.zero;
		}
	}
		
	public void OpenDoor()
	{
		if (!isOpen)
		{
			if (SceneManager.GetActiveScene ().name == currentRoom)
			{
				GameObject.Instantiate (GameManagement.Manager.openedDoorSound, currentPosition, Quaternion.identity);
			}

			if (SceneManager.GetActiveScene ().name == OtherRoom)
			{
				GameObject.Instantiate (GameManagement.Manager.openedDoorSound, ExitPosition, Quaternion.identity);
			}
			isOpen = true;
			other.isOpen = true;
		}
	}

	public void CloseDoor()
	{
		if (isOpen)
		{
			if (SceneManager.GetActiveScene ().name == currentRoom)
			{
				GameObject.Instantiate (GameManagement.Manager.closedDoorSound, currentPosition, Quaternion.identity);
			}

			if (SceneManager.GetActiveScene ().name == OtherRoom)
			{
				GameObject.Instantiate (GameManagement.Manager.closedDoorSound, ExitPosition, Quaternion.identity);
			}
			isOpen = false;
			other.isOpen = false;
		}
	}

	public DoorProfile(string currentRoom, string otherRoom, int doorNumber, Vector2 currentPosition)
    {
        this.currentRoom = currentRoom;
		this.otherRoom = otherRoom;
        this.doorNumber = doorNumber;
        this.currentPosition = currentPosition;
    }

	public override string ToString ()
	{
		return string.Format ("[DoorProfile: Room={0}, Number={1}, Position={2}, OtherRoom={3}, ExitPosition={4}]", Room, Number, Position, OtherRoom, ExitPosition);
	}
}
