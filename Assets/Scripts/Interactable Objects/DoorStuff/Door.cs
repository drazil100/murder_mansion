﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Door : InteractableObject {

    //Foyer 1 door in CollisionExample scene leads to CollisionExample 1 door in Foyer scene
    //^How my door system works                       ^doorName        ^doorNumber 
    public string doorName;
	private string text;
    public int doorNumber;
	private bool help = true;
    //private GameManagement manager;
    
	public bool includedInDoorProfiles = true;
	private GameObject doorCanvas;

	// Use this for initialization
	void Start ()
    {
		help = GameManagement.Manager.hideHelpBoxes;
        //manager = GameManagement.Manager;
        SetInteractButton("Up");
        GetComponent<Texture2D>();

		for (int i = 0; i < doorName.Length; i++)
		{
			if (i != 0 && char.IsUpper (doorName [i]))
				text += " ";
			text += doorName [i];
		}
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (GameManagement.Manager.hideHelpBoxes != help)
		{
			help = GameManagement.Manager.hideHelpBoxes;
			if (doorCanvas != null)
			{
				Destroy (doorCanvas);
			}
		}
	}

	public override void Highlight ()
	{
		base.Highlight ();
		DoGUI ();
	}

	public override void Unhighlight()
	{
		base.Unhighlight ();
		if (doorCanvas != null)
		{
			Destroy (doorCanvas);
		}
	}


    /*
    When player interacts with the door save the door's name, number and and the room the player is leaving 
    to the player's character profile
    */
    public override InteractionState OnInteractionBegin()
    {
		List<CharacterProfile> chars = GameManagement.GetCharacters();
        chars[0].enteredDoor = doorName;
        chars[0].enteredDoorNumber = doorNumber;
        chars[0].roomLeft = SceneManager.GetActiveScene().name;
        chars[0].facing = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().spRenderer.flipX;

		CharacterData context = chars [0].Context;

		foreach (CharacterProfile profile in CharacterProfile.GetCharactersInRoom(context.Room))
		{
			if (profile.Context != chars[0].Context)
			{
				profile.AddCall (new ExitRoomTo (context, doorName, context.position.x));
			}
		}

        return InteractionState.Done;
    }

    public void DoGUI()
    {
		
		if (doorCanvas == null)
		{
			doorCanvas = new GameObject ("DoorCanvas");
			doorCanvas.AddComponent<Canvas> ();
			doorCanvas.AddComponent<CanvasScaler> ();
			doorCanvas.AddComponent<GraphicRaycaster> ();
			doorCanvas.GetComponent<Canvas> ().renderMode = RenderMode.WorldSpace;
			doorCanvas.GetComponent<Canvas>().sortingOrder = 9;
			doorCanvas.transform.position = new Vector3 (transform.position.x, transform.position.y + GetComponent<SpriteRenderer> ().bounds.size.y / 2 + .5f, 0);
			doorCanvas.GetComponent<RectTransform> ().sizeDelta = new Vector2 (1.4f, GetComponent<SpriteRenderer> ().bounds.size.y / 5);
			doorCanvas.GetComponent<CanvasScaler> ().dynamicPixelsPerUnit = 3000;
			doorCanvas.transform.SetParent (this.transform);

			GameObject doorGUI = new GameObject ("DoorGUI");
			doorGUI.AddComponent<RawImage> ();
			doorGUI.GetComponent<RawImage> ().texture = bubble;
			doorGUI.GetComponent<RawImage> ().color = new Color (0.2f, 0.2f, 0.2f);
			doorGUI.transform.SetParent (doorCanvas.transform);
			doorGUI.GetComponent<RectTransform> ().sizeDelta = new Vector2 (1.4f, doorCanvas.GetComponent<RectTransform> ().sizeDelta.y);
			doorGUI.GetComponent<RectTransform> ().localPosition = new Vector3 (0, 0, 0);

			GameObject doorText = new GameObject ("DoorText");
			doorText.transform.SetParent (doorGUI.transform);
			doorText.AddComponent<Text> ();
			doorText.GetComponent<RectTransform> ().localPosition = new Vector3 (0, .01f, .01f);
			doorText.GetComponent<RectTransform> ().sizeDelta = doorCanvas.GetComponent<RectTransform> ().sizeDelta;
			doorText.GetComponent<Text> ().text = Text;
			Font ArialFont = (Font)Resources.GetBuiltinResource (typeof(Font), "Arial.ttf");
			doorText.GetComponent<Text> ().font = ArialFont;
			doorText.GetComponent<Text> ().alignment = TextAnchor.MiddleCenter;
			doorText.GetComponent<Text> ().color = Color.white;
			doorText.GetComponent<Text> ().horizontalOverflow = HorizontalWrapMode.Wrap;
			doorText.GetComponent<Text> ().verticalOverflow = VerticalWrapMode.Truncate;
			doorText.GetComponent<Text> ().resizeTextForBestFit = true;

			doorGUI.transform.SetParent (doorCanvas.transform);
		}
	}

	public string Text
	{
		get {
			if (GameManagement.Manager.hideHelpBoxes) return text;
			return "Press W: " + text;
		}
	}
}
