﻿using UnityEngine;
using System.Collections;

public class HazardProfile
{
	public string ObjectName { get; private set;}
	public Vector2 Position { get; private set;}
	public string RoomName { get; private set;}

	public bool IsOneTimeUse { get; private set;}
	public bool HasBeenUsed { get; private set;}

	public HazardProfile (string newName, Vector2 newPosition, string newRoomName, bool isOneTime, bool used)
	{
		ObjectName = newName;
		Position = newPosition;
		RoomName = newRoomName;

		IsOneTimeUse = isOneTime;
		HasBeenUsed = used;
	}

	public override string ToString ()
	{
		return string.Format ("[InteractProfile: Name={0}, Position={1}, RoomName={2}, isOneTimeUse={3}, hasBeenUsed={4}]", ObjectName, Position, RoomName, IsOneTimeUse, HasBeenUsed);
	}
}
