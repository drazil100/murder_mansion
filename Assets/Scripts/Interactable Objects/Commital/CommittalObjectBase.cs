using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
//using UnityEditor;

[RequireComponent (typeof (SpriteAnimationManager))]
public class CommittalObjectBase : InteractableObject {

	public string ObjectName = "";
	protected bool InteractAvailable = true;

	protected int screenCenterX;
	protected int screenCenterY;
	protected int rectW = 160;
	protected int rectH = 20;
	protected Rect centerRect;
	protected Rect CommitBar;

	public Texture yellow100;
	public Texture grey60;
	protected SpriteAnimationManager SAM;
	public int finalLayer = 15;

	protected bool commit = false;

	protected float commitBarNum = 0;
	protected const int COMMIT_BAR_MAX = 45;
	protected string holdText;
	protected GameObject HazardCanvas;
	protected SpriteRenderer sr;
	public AudioClip commitSound;

	public CommittalObjectBase()
	{
		SetInteractButton ("UseWeapon");

		if(GameManagement.Manager != null)
			holdText = ("Hold " + GameManagement.Manager.InputKeys [Button] + " to Use");
	}

	void Start()
	{
		SAM = GetComponent<SpriteAnimationManager> ();
		SAM.SwitchAnimation (0, true);
		sr = GetComponent<SpriteRenderer>();

		if (GameManagement.Manager != null && !GameManagement.Manager.hazardsUsed.ContainsKey (ObjectName))
		{
			GameManagement.Manager.hazardsUsed.Add (ObjectName, false);
		}
		else if (GameManagement.Manager != null && GameManagement.Manager.hazardsUsed [ObjectName] == true)
		{
			//Destroy(gameObject);
			sr.sortingOrder = finalLayer;
			commit = true;
			InteractAvailable=false;
			base.Unhighlight ();
			SAM.SwitchAnimation (2, true);
		}

		//yellow100 = (Texture)AssetDatabase.LoadAssetAtPath ("Assets/TESTures/yellow.png", typeof(Texture));
		//grey60 = (Texture)AssetDatabase.LoadAssetAtPath ("Assets/TESTures/grey60.png", typeof(Texture));
		screenCenterX = Screen.width/2;
		screenCenterY = Screen.height/2;
		centerRect = new Rect((screenCenterX-(rectW/2)),(screenCenterY-(rectH/2)),rectW,rectH);
		CommitBar = new Rect((screenCenterX-(rectW/2)),(screenCenterY-(rectH/2)),rectW,rectH);
	}

	void Update()
	{
		if (GameManagement.Manager.hideHelpBoxes)
		{
			//Debug.Log ("Called");
			if(HazardCanvas!=null)
				Destroy(HazardCanvas);
		}
	}

	public override void Highlight ()
	{
		if (commit == true)
			return;




		if(HazardCanvas==null)
		{
			SAM.Stop ();
			GetComponent<SpriteRenderer>().sprite = highlighted;
			if (GameManagement.Manager.hideHelpBoxes) return;
			HazardCanvas = new GameObject ("HazardCanvas");
			HazardCanvas.AddComponent<Canvas>();
			HazardCanvas.AddComponent<CanvasScaler>();
			HazardCanvas.AddComponent<GraphicRaycaster>();
			HazardCanvas.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
            HazardCanvas.GetComponent<Canvas>().sortingOrder = 10;
			HazardCanvas.transform.position = new Vector3(transform.position.x, transform.position.y + GetComponent<SpriteRenderer>().bounds.size.y / 2 + .5f, 0);
            if(Camera.main.WorldToViewportPoint(HazardCanvas.transform.position).y>1)
            {
                HazardCanvas.transform.position = new Vector3(transform.position.x/*+GetComponent<SpriteRenderer>().bounds.size.x/2+.5f*/, transform.position.y+transform.position.y*.50f, 0);
            }
			HazardCanvas.GetComponent<RectTransform>().sizeDelta= new Vector2(1.4f, 1);
			HazardCanvas.GetComponent<CanvasScaler>().dynamicPixelsPerUnit = 2600;

			GameObject HazardGUI = new GameObject("HazardGUI");
			HazardGUI.AddComponent<RawImage>();
			HazardGUI.GetComponent<RawImage>().texture = bubble;
			HazardGUI.GetComponent<RawImage>().color = new Color(0.2f,0.2f,0.2f);
			HazardGUI.transform.SetParent(HazardCanvas.transform);
			HazardGUI.GetComponent<RectTransform>().sizeDelta = new Vector2(1.4f, HazardCanvas.GetComponent<RectTransform>().sizeDelta.y);
			HazardGUI.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);

			GameObject HazardText = new GameObject("HazardText");
			HazardText.transform.SetParent(HazardGUI.transform);
			HazardText.AddComponent<Text>();
			HazardText.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0.01f);
			HazardText.GetComponent<RectTransform>().sizeDelta = HazardCanvas.GetComponent<RectTransform>().sizeDelta;
			HazardText.GetComponent<Text>().text = holdText;
			Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
			HazardText.GetComponent<Text>().font = ArialFont;
			HazardText.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
			HazardText.GetComponent<Text>().color = Color.white;
			HazardText.GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
			HazardText.GetComponent<Text>().verticalOverflow = VerticalWrapMode.Truncate;
			HazardText.GetComponent<Text>().resizeTextForBestFit = true;
			HazardText.AddComponent<Outline>();
			HazardText.GetComponent<Outline>().effectDistance = new Vector2 (0.01f, 0.01f);
		}
	}
	public override void Unhighlight ()
	{
		SAM.Play ();
		if(HazardCanvas!=null)
			Destroy(HazardCanvas);

		if (commit == true)
			return;
		SAM.SwitchAnimation (0, true);

	}

	public override InteractionState OnInteractionBegin()
	{

		if(!InteractAvailable)
		{
			return InteractionState.Done;
		}
			
		return InteractionState.Interacting;
	}

	public override InteractionState OnInteractionContinue()
	{

		if (Input.GetButton (Button))
		{
			commitBarNum += (40 * Time.deltaTime);
		}
		else
		{
			commit = false;
			return InteractionState.Done;
		}
			
		if (commitBarNum >= COMMIT_BAR_MAX)
		{
			commit = true;
			return InteractionState.Done;
		}

		return InteractionState.Interacting;
	}

	public override void OnInteractionEnd()
	{
		commitBarNum = 0;

		if(!InteractAvailable) // this option for interacting with the object after its been used already
		{
			//Debug.Log ("already used");
			return; 
		}
		else if(commit) // this option for committing to using this object 
		{
			InteractAvailable = false;
			PerformCommit();

		}
		else // this option for not committing to use object (usually does nothing)
			return; 
	}

	public override void DrawGUI()
	{
		//GUI.Box(centerRect, "");
		//GUI.DrawTexture(centerRect, grey60);

		//GUI.Box(CommitBar, "");
		//GUI.DrawTexture(CommitBar, yellow100);
		Menu m = GameManagement.Manager.GetComponent<Menu> ();
		GUILayout.BeginArea(centerRect);
		m.GetMeter (centerRect.width, centerRect.height, commitBarNum / 45.0f, m.barBGColor, Color.yellow);
		GUILayout.EndArea ();

		CommitBar.width = (commitBarNum*3.54f);

	}

	public virtual void PerformCommit() // upon committing, play animation, set final 
	{
		Debug.Log ("I did the thing");
	}

	public virtual void ShowHoldText()
	{
		
	}

}