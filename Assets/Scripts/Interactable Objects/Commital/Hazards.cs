﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using UnityEditor;
using UnityEngine.SceneManagement;

[RequireComponent (typeof (SpriteAnimationManager))]
public class Hazards : CommittalObjectBase {

	public string RoomName { get; private set;}

	/*
	public override void Highlight ()
	{
		// do nothing so far
	}
	public override void Unhighlight ()
	{
		// do nothing so far
	}
	*/

	void Update()
	{
		if (GameManagement.Manager.hideHelpBoxes)
		{
			//Debug.Log ("Called");
			if(HazardCanvas!=null)
				Destroy(HazardCanvas);
		}
	}
		
	public override void PerformCommit() // upon committing, play animation, set final 
	{
		string scenename = SceneManager.GetActiveScene ().name;

		CharacterProfile[] CPs = CharacterProfile.GetCharactersInRoom (scenename);

		CharacterCore[] chars =	CharacterCore.GetCollidingCharacters (GetComponent<BoxCollider2D> ().bounds);

		List<CharacterProfile> inRange = new List<CharacterProfile> ();
		List<CharacterProfile> notInRange = new List<CharacterProfile> ();

		foreach (CharacterProfile c in CPs)
		{
			bool foundMatch = false;
			foreach (CharacterCore core in chars)
			{
				if (c.owner != null && c.owner.GetComponent<CharacterCore> () == core && c.Type != CharacterType.Player)
				{
					foundMatch = true;
				}
			}

			if (foundMatch)
			{   
				inRange.Add (c);
			}
			else
			{
				if (c.Type != CharacterType.Player)
					notInRange.Add (c);
			}
		}

		bool killedSomeone = false;
		foreach (CharacterProfile prof in inRange)
		{
			prof.AddCall (new KillCharacter ());
			killedSomeone = true;
		}

		if (killedSomeone && notInRange.Count > 0)
		{
			GameManagement.ClearMessages ();
			for (int i = 0; i < notInRange.Count; i++)
			{
				CharacterProfile prof = notInRange [i];
				//string r = SceneManager.GetActiveScene ().name;

				//character.AddCall(new FollowMe(GameManagement.GetCharacters()[0].Context));
				prof.AddCall (new StopFollowing (false));
				prof.AddCall (new SetTarget (transform.position.x, scenename));
				prof.AddCall (new WitnessMurder (GameManagement.GetCharacters () [0].Context));
				prof.AddCall (new StopGame ());

				if (prof.Type != CharacterType.Player)
				{
					if (prof.Type == CharacterType.Inspector)
					{
						GameManagement.AddMessage (new Message (prof.Context, GameManagement.GetMessage (prof.Type, "witness_murder")));
					} 
					else if (Random.Range (0, 2) == 0)
					{
						GameManagement.AddMessage (new Message (prof.Context, GameManagement.GetMessage (prof.Type, "witness_murder")));
					}
				}


			}

			foreach (CharacterProfile prof in CharacterProfile.GetCharactersNotInRoom(scenename))
			{
				prof.AddCall (new StopFollowing (false));
				prof.AddCall (new SetTarget (transform.position.x, scenename));
				prof.AddCall(new StopGame());
			}
			GameManagement.Manager.gameOver = true;
		}

        //GameManagement.Manager.SusMeter.AffectMomentumRate (-.1f);

		GameManagement.Manager.hazardsUsed [ObjectName] = true;

		base.Unhighlight ();

		SAM.PlayOnce (1);
		GetComponent<Speaker>().PlaySoundOnce(commitSound);
		SAM.SwitchAnimation (2, true);
		sr.sortingOrder = finalLayer;

		//Destroy(gameObject);
	}
}
