﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class HeavyHazard : CommittalObjectBase {

    public int rateOfIncrease = 300; //How much the bar increases for each key press
    public const int rateOfDecreaseBase = 10; //How fast the bar moves toward 0. Gets called each frame.
    private float rateOfDecreaseCurrent;
    void Start()
    {
		SetInteractButton("UseWeapon");

        SAM = GetComponent<SpriteAnimationManager>();
		sr = GetComponent<SpriteRenderer>();

        if (GameManagement.Manager != null && !GameManagement.Manager.hazardsUsed.ContainsKey(ObjectName))
        {
            GameManagement.Manager.hazardsUsed.Add(ObjectName, false);
        }
        else if (GameManagement.Manager != null && GameManagement.Manager.hazardsUsed[ObjectName] == true)
        {
            //Destroy(gameObject);
			commit = true;
			InteractAvailable=false;
			base.Unhighlight ();
			SAM.SwitchAnimation (2, true);
			sr.sortingOrder = 15;
        }

        //yellow100 = (Texture)AssetDatabase.LoadAssetAtPath ("Assets/TESTures/yellow.png", typeof(Texture));
        //grey60 = (Texture)AssetDatabase.LoadAssetAtPath ("Assets/TESTures/grey60.png", typeof(Texture));
        screenCenterX = Screen.width / 2;
        screenCenterY = Screen.height / 2;
        centerRect = new Rect((screenCenterX - (rectW / 2)), (screenCenterY - (rectH / 2)), rectW, rectH);
        CommitBar = new Rect((screenCenterX - (rectW / 2)), (screenCenterY - (rectH / 2)), rectW, rectH);

		holdText = "Rapidly Press \""+ GameManagement.Manager.InputKeys[Button] +"\" to Knock Over";

        rateOfDecreaseCurrent = rateOfDecreaseBase;
    }

    public override InteractionState OnInteractionBegin()
    {
        if(!InteractAvailable)
        {
            return InteractionState.Done;
        }
        if(commitBarNum==0)
        {
            commitBarNum = 20;
        }

        return InteractionState.Interacting;
    }

    public override InteractionState OnInteractionContinue()
    {
        
        
		if(Input.GetButtonDown(Button))
        {
            commitBarNum += rateOfIncrease * Time.deltaTime;
            rateOfDecreaseCurrent = rateOfDecreaseBase;
			//Debug.Log (commitBarNum);
        }
        else if(Input.GetButtonDown("CloseQTE"))
        {
            commitBarNum = 0;
            return InteractionState.Done;
        }
        else
        {
			rateOfDecreaseCurrent = CharacterCore.Approach (rateOfDecreaseCurrent, 50, 0.08f);
			commitBarNum -= (int)rateOfDecreaseCurrent * Time.deltaTime;
			if (commitBarNum < 0) commitBarNum = 0;
        }

        if(commitBarNum<=0)
        {
            commitBarNum = 0;
            rateOfDecreaseCurrent = rateOfDecreaseBase;
            return InteractionState.Done;
        }
        if(CommitBar.width>=centerRect.width)
        {
            commit = true;
            return InteractionState.Done;
        }

        return InteractionState.Interacting;
    }

    public override void OnInteractionEnd()
    {
		if(!InteractAvailable) // this option for interacting with the object after its been used already
		{
			//Debug.Log ("already used");
			return; 
		}
		else if(commit) // this option for committing to using this object 
		{
			InteractAvailable = false;
			PerformCommit();

		}
		else // this option for not committing to use object (usually does nothing)
			return; 
    }

    public override void PerformCommit() // upon committing, play animation, set final 
    {

        string scenename = SceneManager.GetActiveScene().name;

        CharacterProfile[] CPs = CharacterProfile.GetCharactersInRoom(scenename);

        CharacterCore[] chars = CharacterCore.GetCollidingCharacters(GetComponent<BoxCollider2D>().bounds);

        List<CharacterProfile> inRange = new List<CharacterProfile>();
        List<CharacterProfile> notInRange = new List<CharacterProfile>();

        foreach (CharacterProfile c in CPs)
        {
            bool foundMatch = false;
            foreach (CharacterCore core in chars)
            {
                if (c.owner != null && c.owner.GetComponent<CharacterCore>() == core && c.Type != CharacterType.Player)
                {
                    foundMatch = true;
                }
            }

            if (foundMatch)
            {
                inRange.Add(c);
            }
            else
            {
                if (c.Type != CharacterType.Player)
                    notInRange.Add(c);
            }
        }

        bool killedSomeone = false;
        foreach (CharacterProfile prof in inRange)
        {
            prof.AddCall(new KillCharacter());
            killedSomeone = true;
        }

        if (killedSomeone && notInRange.Count > 0)
        {
            GameManagement.ClearMessages();
            for (int i = 0; i < notInRange.Count; i++)
            {
                CharacterProfile prof = notInRange[i];
                //string r = SceneManager.GetActiveScene ().name;

                //character.AddCall(new FollowMe(GameManagement.GetCharacters()[0].Context));
                prof.AddCall(new StopFollowing(false));
                prof.AddCall(new SetTarget(transform.position.x, scenename));
                prof.AddCall(new WitnessMurder(GameManagement.GetCharacters()[0].Context));
                prof.AddCall(new StopGame());

                if (prof.Type != CharacterType.Player)
                {
                    if (prof.Type == CharacterType.Inspector)
                    {
                        GameManagement.AddMessage(new Message(prof.Context, GameManagement.GetMessage(prof.Type, "witness_murder")));
                    }
                    else if (Random.Range(0, 2) == 0)
                    {
                        GameManagement.AddMessage(new Message(prof.Context, GameManagement.GetMessage(prof.Type, "witness_murder")));
                    }
                }


            }

            foreach (CharacterProfile prof in CharacterProfile.GetCharactersNotInRoom(scenename))
            {
                prof.AddCall(new StopFollowing(false));
                prof.AddCall(new SetTarget(transform.position.x, scenename));
                prof.AddCall(new StopGame());
            }
            GameManagement.Manager.gameOver = true;
        }

        //GameManagement.Manager.SusMeter.AffectMomentumRate (-.1f);

        GameManagement.Manager.hazardsUsed[ObjectName] = true;

        base.Unhighlight();

		SAM.PlayOnce (1);
		GetComponent<Speaker>().PlaySoundOnce(commitSound);
		SAM.SwitchAnimation (2, true);
		sr.sortingOrder = finalLayer;      
		//Destroy(gameObject);
    }

    public override void DrawGUI()
    {
        //GUI.Box(centerRect, "");
        //GUI.DrawTexture(centerRect, grey60);

        //GUI.Box(CommitBar, "");
        //GUI.DrawTexture(CommitBar, yellow100);

		Menu m = GameManagement.Manager.GetComponent<Menu> ();
		GUILayout.BeginArea(centerRect);
		m.GetMeter (centerRect.width, centerRect.height, commitBarNum / 45.0f, m.barBGColor, Color.yellow);
		GUILayout.EndArea ();

		GUI.Label(new Rect(new Vector2(centerRect.x,centerRect.y - centerRect.height), new Vector2(centerRect.width/2,centerRect.height)), "Tap "+GameManagement.Manager.InputKeys[Button]+"!");
        GUI.Label(new Rect(new Vector2(centerRect.x+centerRect.x/2,centerRect.y - centerRect.height), new Vector2(centerRect.width/2,centerRect.height)), "Q: Close");

        CommitBar.width = (commitBarNum * 3.54f);

    }
}
