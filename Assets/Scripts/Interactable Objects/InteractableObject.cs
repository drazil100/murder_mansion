﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum InteractionState
{
	Interacting,
	Done
}

//[RequireComponent (typeof (Collider2D))]
[RequireComponent (typeof (SpriteRenderer))]
abstract public class InteractableObject : MonoBehaviour {
	public static float interactionRange = 1;
	public Sprite normal;
	public Sprite highlighted;
	public Texture2D bubble;
	private string baseButton="Interact";

	//  Put any GUI that the player should draw in here. 
	//  The player will display it as long as they remain in the PlayerState.Interacting state
	public virtual void DrawGUI()
	{
	}

	//  This function is called when the player first presses the key used to start an interaction.
	//  This function only runs once per keypress and should be used to either set up an object
	//  or on an objects that should only have one frame of interaction.
	public virtual InteractionState OnInteractionBegin()
	{
		return InteractionState.Done;  
	}

	//  This should be used if the object needs to have code run once a frame. 
	//  It should be treated as an InteractableObject's Update() function.
	public virtual InteractionState OnInteractionContinue()
	{
		return InteractionState.Done; 
	}

	//  This is called when the interaction is over. If any variables need to be reset for the next use
	//  put the cleanup code in here. Alternatively cleanup can be done in OnInteractionBegin().
	public virtual void OnInteractionEnd()
	{
	}

	public virtual void Highlight()
	{
		GetComponent<SpriteRenderer>().sprite = highlighted;   //<-- default for interactables
	}

	public virtual void Unhighlight()
	{
		GetComponent<SpriteRenderer>().sprite = normal;   //<-- default for interactables
	}

	public static GameObject FindNearestInteractableObject(Vector3 position)
	{
		return FindNearestInteractableObject (new Vector2 (position.x, position.y));
	}

	public static GameObject FindNearestInteractableObject(Vector2 position)
	{
		try
		{
			GameObject[] objects = GameObject.FindGameObjectsWithTag ("InteractableObject");
			int nearest = 0;
			if (objects [0] == null) return null;
			Vector2 nearestPos = new Vector2(objects[0].transform.position.x, objects[0].transform.position.y);
			for (int i = 1; i < objects.Length; i++)
			{
				Vector2 v1 = new Vector2(objects[i].transform.position.x, objects[i].transform.position.y);

				if (Vector2.Distance (v1, position) < Vector2.Distance (nearestPos, position))
				{
					nearest = i;
					nearestPos = v1;
				}
			}

			return objects[nearest];
		}
		catch (Exception)
		{
			return null;
		}
	}

	public static GameObject FindNearestInteractableObject(Vector3 position, string button)
	{
		return FindNearestInteractableObject (new Vector2 (position.x, position.y), button);
	}

	public static GameObject FindNearestInteractableObject(Vector2 position, string button)
	{
		try
		{
			GameObject[] objects = GameObject.FindGameObjectsWithTag ("InteractableObject");
			int nearest = -1;
			//if (objects [0] == null) return null;
			Vector2 nearestPos = Vector2.zero;// = new Vector2(objects[0].transform.position.x, objects[0].transform.position.y);
			//int count = 0;
			for (int i = 0; i < objects.Length; i++)
			{
				//if (button == objects[i].GetComponent<InteractableObject>().Button) count++;
				if (nearest != -1)
				{
					if (objects[i].GetComponent<InteractableObject>() != null && objects[i].GetComponent<InteractableObject>().Button == button)
					{
						//if (button == "UseWeapon") Debug.Log (objects[i].transform.position);
						Vector2 v1 = new Vector2(objects[i].transform.position.x, objects[i].transform.position.y);

						if (Mathf.Abs(v1.x - position.x) < Mathf.Abs(nearestPos.x - position.x))
						{
							nearest = i;
							nearestPos = v1;
						}
					}
				}
				else
				{
					if (objects[i].GetComponent<InteractableObject>() != null && objects[i].GetComponent<InteractableObject>().Button == button)
					{
						//if (button == "UseWeapon") Debug.Log ("first: " + objects[i].transform.position);
						nearest = i;
						nearestPos = new Vector2(objects[i].transform.position.x, objects[i].transform.position.y);
					}
				}
			}

			//Debug.Log(button + ": " + count);

			if (nearest == -1)
				return null;

			return objects[nearest];
		}
		catch (Exception e)
		{
			if (button == "UseWeapon") Debug.Log (e);
			return null;
		}
	}

    protected void SetInteractButton(string buttonName)
    {
        baseButton = buttonName;
    }

    public string Button { get { return baseButton; } }
}
